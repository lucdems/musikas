<?php

namespace musikas\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class musikasUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
