<?php
namespace musikas\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Test\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType;

class RegistrationType extends AbstractType{

  public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options){
    $builder->add('nom')
            ->add('prenom')
            ->remove('username');
  }

  public function getParent(){
    return 'fos_user_registration';
  }

  public function getName(){
    return 'musikas_user_registration';
  }
}
 ?>