<?php

namespace musikas\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('musikasUserBundle:Default:index.html.twig');
    }
}
