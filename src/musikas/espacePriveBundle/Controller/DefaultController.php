<?php

namespace musikas\espacePriveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('musikasespacePriveBundle:Default:index.html.twig');
    }
    
    public function espace_priveAction()
    {
        // Si l'utilisateur est un admin on le redirige vers la page d'administration
        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            $url = $this->generateUrl('easyadmin');
            return $this->redirect($url);
        }
        else {
        return $this->render('musikasespacePriveBundle:Default:espace_prive.html.twig'); 
        }
    }
    
    public function utilisateursAction()
    {
        return $this->render('musikasespacePriveBundle:Default:espace_prive_utilisateurs.html.twig'); // Vue de la page de contact à faire
    }
    
    public function adminAction()
    {
        return $this->render('musikasespacePriveBundle:Default:admin.html.twig');
    }
    
    public function documentsAction()
    {
        return $this->render('musikasespacePriveBundle:Default:espace_prive_documents.html.twig');
    }
    
    public function nouvelArticleAction()
    {
        return $this->render('musikasespacePriveBundle:Default:espace_prive_nouvel_article.html.twig');
    }
}
