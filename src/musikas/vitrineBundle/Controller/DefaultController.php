<?php

namespace musikas\vitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('musikasvitrineBundle:Default:accueil.html.twig');
    }
    
    public function evenementsAction()
    {
        $gestionnaireEntite=$this->getDoctrine()->getManager();
        $repositoryArticle=$gestionnaireEntite->getRepository('musikasvitrineBundle:Article');
        $tabArticle=$repositoryArticle->findAll();
        return $this->render('musikasvitrineBundle:Default:evenements.html.twig', 
        array('tabArticle' => $tabArticle));
    }
    
    public function evenementAction($id)
    {
        $gestionnaireEntite=$this->getDoctrine()->getManager();
        $repositoryArticle=$gestionnaireEntite->getRepository('musikasvitrineBundle:Article');
        $article=$repositoryArticle->find($id);
        return $this->render('musikasvitrineBundle:Default:evenement.html.twig', 
        array('article' => $article));
    }
    
    public function enseignementsAction()
    {
        $gestionnaireEntite=$this->getDoctrine()->getManager();
        $repositoryenseignement=$gestionnaireEntite->getRepository('musikasvitrineBundle:enseignement');
        $tabEnseignement=$repositoryenseignement->findAll();
        return $this->render('musikasvitrineBundle:Default:enseignements.html.twig', 
        array('tabEnseignement' => $tabEnseignement));
    }
    public function enseignementAction($id)
    {
        $gestionnaireEntite=$this->getDoctrine()->getManager();
        $repositoryenseignement=$gestionnaireEntite->getRepository('musikasvitrineBundle:enseignement');
        $repositoryEcole=$gestionnaireEntite->getRepository('musikasvitrineBundle:Ecole');
        $enseignement=$repositoryenseignement->find($id);
        $tabEcole=$repositoryEcole->getEcolesByEnseignement($enseignement);
        return $this->render('musikasvitrineBundle:Default:enseignement.html.twig', 
        array('enseignement' => $enseignement, 'tabEcole' => $tabEcole));
    }
    
    public function connexionAction()
    {
        return $this->render('musikasvitrineBundle:Default:connexion.html.twig');
    }
    
    public function ecolesAction()
    {
        $gestionnaireEntite=$this->getDoctrine()->getManager();
        $repositoryEcole=$gestionnaireEntite->getRepository('musikasvitrineBundle:Ecole');
        $tabEcole=$repositoryEcole->findAll();
        return $this->render('musikasvitrineBundle:Default:ecoles.html.twig', 
        array('tabEcole' => $tabEcole));
    }
    public function ecoleAction($id)
    {
        $gestionnaireEntite=$this->getDoctrine()->getManager();
        $repositoryEcole=$gestionnaireEntite->getRepository('musikasvitrineBundle:Ecole');
        $repositoryenseignement=$gestionnaireEntite->getRepository('musikasvitrineBundle:enseignement');
        $ecole=$repositoryEcole->find($id);
        $tabEnseignement=$ecole->getEnseignements();
        return $this->render('musikasvitrineBundle:Default:ecole.html.twig', 
        array('ecole' => $ecole, 'tabEnseignement' => $tabEnseignement));  
    }
    
    public function equipeAction()
    {
        $gestionnaireEntite=$this->getDoctrine()->getManager();
        $repositoryProf=$gestionnaireEntite->getRepository('musikasUserBundle:User');
        $tabProf=$repositoryProf->findAll();
        return $this->render('musikasvitrineBundle:Default:equipe.html.twig', 
        array('tabProf' => $tabProf));
    }
    public function equipe_profAction($id)
    {
        $gestionnaireEntite=$this->getDoctrine()->getManager();
        $repositoryProf=$gestionnaireEntite->getRepository('musikasUserBundle:User');
        $repositoryEnseignement=$gestionnaireEntite->getRepository('musikasvitrineBundle:enseignement');
        $prof=$repositoryProf->find($id);
        $tabEnseignement=$repositoryEnseignement->getEnseignementsByUser($prof);
        return $this->render('musikasvitrineBundle:Default:equipe_prof.html.twig', 
        array('prof' => $prof,'tabEnseignement' => $tabEnseignement));
    }
    
    public function FormulaireAction()
    {
        return $this->render('musikasvitrineBundle:Default:formulaire.html.twig');
    }
    public function ressourcesAction()
    {
        return $this->render('musikasvitrineBundle:Default:ressources.html.twig');
    }
    
    public function contactAction()
    {
        return $this->render('musikasvitrineBundle:Default:contact.html.twig'); // Vue de la page de contact à faire
    }
    
    public function changeLocaleAction($langue)
    {
      
      $this->get('session')->set('_locale', $langue);
      // 
      $request = $this->getRequest();
      return $this->redirect($request->server->get('HTTP_REFERER'));;
    }
    
}
