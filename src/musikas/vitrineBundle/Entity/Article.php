<?php

namespace musikas\vitrineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="musikas\vitrineBundle\Repository\ArticleRepository")
 * @Vich\Uploadable
 */
class Article
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
    * @var File
    * @Vich\UploadableField(mapping="article_images", fileNameProperty="image")
    *
    */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu_fr", type="text")
     */
    private $contenuFr;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu_eh", type="text")
     */
    private $contenuEh;

    /**
     * @var int
     *
     * @ORM\Column(name="rang", type="integer")
     */
    private $rang;

    /**
     * @var bool
     *
     * @ORM\Column(name="est_accueil", type="boolean")
     */
    private $estAccueil;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Article
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Article
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set contenuFr
     *
     * @param string $contenuFr
     *
     * @return Article
     */
    public function setContenuFr($contenuFr)
    {
        $this->contenuFr = $contenuFr;

        return $this;
    }

    /**
     * Get contenuFr
     *
     * @return string
     */
    public function getContenuFr()
    {
        return $this->contenuFr;
    }

    /**
     * Set contenuEh
     *
     * @param string $contenuEh
     *
     * @return Article
     */
    public function setContenuEh($contenuEh)
    {
        $this->contenuEh = $contenuEh;

        return $this;
    }

    /**
     * Get contenuEh
     *
     * @return string
     */
    public function getContenuEh()
    {
        return $this->contenuEh;
    }

    /**
     * Set rang
     *
     * @param integer $rang
     *
     * @return Article
     */
    public function setRang($rang)
    {
        $this->rang = $rang;

        return $this;
    }

    /**
     * Get rang
     *
     * @return int
     */
    public function getRang()
    {
        return $this->rang;
    }

    /**
     * Set estAccueil
     *
     * @param boolean $estAccueil
     *
     * @return Article
     */
    public function setEstAccueil($estAccueil)
    {
        $this->estAccueil = $estAccueil;

        return $this;
    }

    /**
     * Get estAccueil
     *
     * @return bool
     */
    public function getEstAccueil()
    {
        return $this->estAccueil;
    }
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }
}
