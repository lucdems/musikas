<?php

namespace musikas\vitrineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * enseignement
 *
 * @ORM\Table(name="enseignement")
 * @ORM\Entity(repositoryClass="musikas\vitrineBundle\Repository\enseignementRepository")
 */
class enseignement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=100)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    
    /**
     * @ORM\ManyToMany(targetEntity="\musikas\UserBundle\Entity\User", cascade={"persist"})
     */
    private $professeurs;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return enseignement
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return enseignement
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return enseignement
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->professeurs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add professeur
     *
     * @param \musikas\UserBundle\Entity\User $professeur
     *
     * @return enseignement
     */
    public function addProfesseur(\musikas\UserBundle\Entity\User $professeur)
    {
        $this->professeurs[] = $professeur;

        return $this;
    }

    /**
     * Remove professeur
     *
     * @param \musikas\UserBundle\Entity\User $professeur
     */
    public function removeProfesseur(\musikas\UserBundle\Entity\User $professeur)
    {
        $this->professeurs->removeElement($professeur);
    }

    /**
     * Get professeurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfesseurs()
    {
        return $this->professeurs;
    }
    
    public function __toString() 
    //permet de debuger le forumulaire ("impossible de convertir Enseignement en String" )
    //de école qui ne marchait pas 
    {
        return $this->nom;
    }
}
