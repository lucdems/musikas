<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_485a066c32ca9ae1e1d67b296b3e021f888d141c60fa27e18b7a27a022eefb09 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d64eb5779469c2cf8738c66a3229d588fa7d384387e2e808903f74881c690921 = $this->env->getExtension("native_profiler");
        $__internal_d64eb5779469c2cf8738c66a3229d588fa7d384387e2e808903f74881c690921->enter($__internal_d64eb5779469c2cf8738c66a3229d588fa7d384387e2e808903f74881c690921_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_d64eb5779469c2cf8738c66a3229d588fa7d384387e2e808903f74881c690921->leave($__internal_d64eb5779469c2cf8738c66a3229d588fa7d384387e2e808903f74881c690921_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo str_replace('{{ widget }}', $view['form']->block($form, 'form_widget_simple'), $money_pattern) ?>*/
/* */
