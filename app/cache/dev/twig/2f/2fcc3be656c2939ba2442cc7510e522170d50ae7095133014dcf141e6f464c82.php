<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_f2315243796ef21c6d74adbed82b3c73b48a8fe3d4201f4462291a3a8e91a14a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_456c2c9a16e0a733b139afceb2666187053d92cc549ce53101ad41897064f53c = $this->env->getExtension("native_profiler");
        $__internal_456c2c9a16e0a733b139afceb2666187053d92cc549ce53101ad41897064f53c->enter($__internal_456c2c9a16e0a733b139afceb2666187053d92cc549ce53101ad41897064f53c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_456c2c9a16e0a733b139afceb2666187053d92cc549ce53101ad41897064f53c->leave($__internal_456c2c9a16e0a733b139afceb2666187053d92cc549ce53101ad41897064f53c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td></td>*/
/*     <td>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
