<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_3cc245f4f99d1b7c1789339f0d20e014e557c55ab46206d64cd12431143762d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_766e3d6521e12757eb62a8e0b42bd5c8b59f573919e76e3d13148b36c9c39eff = $this->env->getExtension("native_profiler");
        $__internal_766e3d6521e12757eb62a8e0b42bd5c8b59f573919e76e3d13148b36c9c39eff->enter($__internal_766e3d6521e12757eb62a8e0b42bd5c8b59f573919e76e3d13148b36c9c39eff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_766e3d6521e12757eb62a8e0b42bd5c8b59f573919e76e3d13148b36c9c39eff->leave($__internal_766e3d6521e12757eb62a8e0b42bd5c8b59f573919e76e3d13148b36c9c39eff_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_container_attributes') ?>*/
/* */
