<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_bb2aa5d12773979abec5cf192400d194cbd68af06499aa93f2df6428a1771597 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_db2f148b4c0b6b9b32a63146a8e72f86c2e650c5be0d2f6f338a0dedbc1fb4f6 = $this->env->getExtension("native_profiler");
        $__internal_db2f148b4c0b6b9b32a63146a8e72f86c2e650c5be0d2f6f338a0dedbc1fb4f6->enter($__internal_db2f148b4c0b6b9b32a63146a8e72f86c2e650c5be0d2f6f338a0dedbc1fb4f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_db2f148b4c0b6b9b32a63146a8e72f86c2e650c5be0d2f6f338a0dedbc1fb4f6->leave($__internal_db2f148b4c0b6b9b32a63146a8e72f86c2e650c5be0d2f6f338a0dedbc1fb4f6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'reset')) ?>*/
/* */
