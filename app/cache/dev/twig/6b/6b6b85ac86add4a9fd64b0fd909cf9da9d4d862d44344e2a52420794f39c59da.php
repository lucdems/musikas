<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_7e682ef4fbc69c2ed55e384899bd1369a1705354dca2ee98b3d57e708f930ebd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2707c7b5d7a36930df2072336895a63b3afae44eba5ab2adf90af51d062ca21 = $this->env->getExtension("native_profiler");
        $__internal_a2707c7b5d7a36930df2072336895a63b3afae44eba5ab2adf90af51d062ca21->enter($__internal_a2707c7b5d7a36930df2072336895a63b3afae44eba5ab2adf90af51d062ca21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a2707c7b5d7a36930df2072336895a63b3afae44eba5ab2adf90af51d062ca21->leave($__internal_a2707c7b5d7a36930df2072336895a63b3afae44eba5ab2adf90af51d062ca21_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3e5578e95866f03b168b81a5337f8e628014f25b222bd789e3add5380833f484 = $this->env->getExtension("native_profiler");
        $__internal_3e5578e95866f03b168b81a5337f8e628014f25b222bd789e3add5380833f484->enter($__internal_3e5578e95866f03b168b81a5337f8e628014f25b222bd789e3add5380833f484_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.check_email", array("%email%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_3e5578e95866f03b168b81a5337f8e628014f25b222bd789e3add5380833f484->leave($__internal_3e5578e95866f03b168b81a5337f8e628014f25b222bd789e3add5380833f484_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "@FOSUser/layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>*/
/* {% endblock fos_user_content %}*/
/* */
