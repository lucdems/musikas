<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_afa45345a2c1bc94437a86684729620ec30f00410d68e4fbb5055fb3a2575e4c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7dfce89c56fcfaea83d9da852a1cc75d2eac17aec7f7e2f7dc599a412a395d66 = $this->env->getExtension("native_profiler");
        $__internal_7dfce89c56fcfaea83d9da852a1cc75d2eac17aec7f7e2f7dc599a412a395d66->enter($__internal_7dfce89c56fcfaea83d9da852a1cc75d2eac17aec7f7e2f7dc599a412a395d66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_7dfce89c56fcfaea83d9da852a1cc75d2eac17aec7f7e2f7dc599a412a395d66->leave($__internal_7dfce89c56fcfaea83d9da852a1cc75d2eac17aec7f7e2f7dc599a412a395d66_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <table <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <tr>*/
/*         <td colspan="2">*/
/*             <?php echo $view['form']->errors($form) ?>*/
/*         </td>*/
/*     </tr>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </table>*/
/* */
