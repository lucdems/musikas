<?php

/* @EasyAdmin/default/field_string.html.twig */
class __TwigTemplate_12bf2d66a3499d4b39284903faba1472bf33501db4e6977d548352bb8048b79b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83029ef648abcbcfbd65b969d1ee61bd90dee6cdbaf69220eec1b16962ce855a = $this->env->getExtension("native_profiler");
        $__internal_83029ef648abcbcfbd65b969d1ee61bd90dee6cdbaf69220eec1b16962ce855a->enter($__internal_83029ef648abcbcfbd65b969d1ee61bd90dee6cdbaf69220eec1b16962ce855a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/field_string.html.twig"));

        // line 1
        if (((isset($context["view"]) ? $context["view"] : $this->getContext($context, "view")) == "show")) {
            // line 2
            echo "    ";
            echo nl2br(twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true));
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('easyadmin_extension')->truncateText($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        }
        
        $__internal_83029ef648abcbcfbd65b969d1ee61bd90dee6cdbaf69220eec1b16962ce855a->leave($__internal_83029ef648abcbcfbd65b969d1ee61bd90dee6cdbaf69220eec1b16962ce855a_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/field_string.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  24 => 2,  22 => 1,);
    }
}
/* {% if view == 'show' %}*/
/*     {{ value|nl2br }}*/
/* {% else %}*/
/*     {{ value|easyadmin_truncate }}*/
/* {% endif %}*/
/* */
