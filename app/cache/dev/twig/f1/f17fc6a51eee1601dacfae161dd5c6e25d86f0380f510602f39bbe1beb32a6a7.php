<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_bba45715b94739459a5ca4ef1f008465ce03e35aef6ae037ce0928bc05a75661 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_918fb75b0871c9b70f5eefa794809f886e3744741bc89443ff6a676399fb5d27 = $this->env->getExtension("native_profiler");
        $__internal_918fb75b0871c9b70f5eefa794809f886e3744741bc89443ff6a676399fb5d27->enter($__internal_918fb75b0871c9b70f5eefa794809f886e3744741bc89443ff6a676399fb5d27_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_918fb75b0871c9b70f5eefa794809f886e3744741bc89443ff6a676399fb5d27->leave($__internal_918fb75b0871c9b70f5eefa794809f886e3744741bc89443ff6a676399fb5d27_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="checkbox"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     <?php if (strlen($value) > 0): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?>*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
