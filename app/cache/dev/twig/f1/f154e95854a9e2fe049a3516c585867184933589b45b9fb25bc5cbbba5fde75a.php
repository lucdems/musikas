<?php

/* musikasvitrineBundle:Default:ecoles.html.twig */
class __TwigTemplate_d779911b87b76bf9db104fe573e0c1acdaa1255216113a8c086a0d8bbc910663 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMere.html.twig", "musikasvitrineBundle:Default:ecoles.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMere.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_92969f696c4de1170c1349e2b3b4d5c13ad901abbd52bef0ac0fe872ea2cf257 = $this->env->getExtension("native_profiler");
        $__internal_92969f696c4de1170c1349e2b3b4d5c13ad901abbd52bef0ac0fe872ea2cf257->enter($__internal_92969f696c4de1170c1349e2b3b4d5c13ad901abbd52bef0ac0fe872ea2cf257_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasvitrineBundle:Default:ecoles.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_92969f696c4de1170c1349e2b3b4d5c13ad901abbd52bef0ac0fe872ea2cf257->leave($__internal_92969f696c4de1170c1349e2b3b4d5c13ad901abbd52bef0ac0fe872ea2cf257_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_d2dbe022b509d71115cf26239f6f8f3f97128a93e871f9712b895e3254ef5221 = $this->env->getExtension("native_profiler");
        $__internal_d2dbe022b509d71115cf26239f6f8f3f97128a93e871f9712b895e3254ef5221->enter($__internal_d2dbe022b509d71115cf26239f6f8f3f97128a93e871f9712b895e3254ef5221_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <section>
        <div class=\"article-list\">
        <div class=\"container\" id=\"contenuenseignement\">
            <div class=\"intro\">
                <h1 class=\"text-center\">";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nos écoles"), "html", null, true);
        echo "</h1>
                <p class=\"text-justify\">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p>
            </div>
            
            <div class=\"row articles\" id=\"imagesenseignements\">
            ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tabEcole"]) ? $context["tabEcole"] : $this->getContext($context, "tabEcole")));
        foreach ($context['_seq'] as $context["_key"] => $context["ecole"]) {
            // line 14
            echo "                <div class=\"col-md-4 col-sm-6 item\">
                    <a href=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("musikasvitrine_ecole", array("id" => $this->getAttribute($context["ecole"], "id", array()))), "html", null, true);
            echo "\"><img class=\"img-responsive\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["ecole"], "image", array())), "html", null, true);
            echo "\"></a>
                    <h3 class=\"name\">";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["ecole"], "nom", array()), "html", null, true);
            echo "</h3>
                    <p class=\"text-justify description\">";
            // line 17
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["ecole"], "description", array()), 0, 200), "html", null, true);
            echo "...</p><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("musikasvitrine_ecole", array("id" => $this->getAttribute($context["ecole"], "id", array()))), "html", null, true);
            echo "\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ecole'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "            </div>
            
        </div>
        </div>
    </section>
";
        
        $__internal_d2dbe022b509d71115cf26239f6f8f3f97128a93e871f9712b895e3254ef5221->leave($__internal_d2dbe022b509d71115cf26239f6f8f3f97128a93e871f9712b895e3254ef5221_prof);

    }

    public function getTemplateName()
    {
        return "musikasvitrineBundle:Default:ecoles.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 20,  71 => 17,  67 => 16,  61 => 15,  58 => 14,  54 => 13,  46 => 8,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMere.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <section>*/
/*         <div class="article-list">*/
/*         <div class="container" id="contenuenseignement">*/
/*             <div class="intro">*/
/*                 <h1 class="text-center">{{ 'Nos écoles' | trans }}</h1>*/
/*                 <p class="text-justify">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p>*/
/*             </div>*/
/*             */
/*             <div class="row articles" id="imagesenseignements">*/
/*             {% for ecole in tabEcole %}*/
/*                 <div class="col-md-4 col-sm-6 item">*/
/*                     <a href="{{ path('musikasvitrine_ecole', { 'id': ecole.id }) }}"><img class="img-responsive" src="{{ asset(ecole.image) }}"></a>*/
/*                     <h3 class="name">{{ ecole.nom }}</h3>*/
/*                     <p class="text-justify description">{{ ecole.description[:200] }}...</p><a href="{{ path('musikasvitrine_ecole', { 'id': ecole.id }) }}" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a>*/
/*                 </div>*/
/*             {% endfor %}*/
/*             </div>*/
/*             */
/*         </div>*/
/*         </div>*/
/*     </section>*/
/* {% endblock %}*/
