<?php

/* musikasvitrineBundle:Default:evenement.html.twig */
class __TwigTemplate_36beebb1c712b0c443b75e450d918a3aec32ad1d69728c3a79fa64919d7dd1e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMere.html.twig", "musikasvitrineBundle:Default:evenement.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMere.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4dd6d7b6831ced5043b694406a9ccdbca03be8c74798c5a2ba388d00dc511aea = $this->env->getExtension("native_profiler");
        $__internal_4dd6d7b6831ced5043b694406a9ccdbca03be8c74798c5a2ba388d00dc511aea->enter($__internal_4dd6d7b6831ced5043b694406a9ccdbca03be8c74798c5a2ba388d00dc511aea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasvitrineBundle:Default:evenement.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4dd6d7b6831ced5043b694406a9ccdbca03be8c74798c5a2ba388d00dc511aea->leave($__internal_4dd6d7b6831ced5043b694406a9ccdbca03be8c74798c5a2ba388d00dc511aea_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_727abd1f65a1877e6f4e11d419af1bdaf49e8f65659e2a4b9d9ab19fcb340f98 = $this->env->getExtension("native_profiler");
        $__internal_727abd1f65a1877e6f4e11d419af1bdaf49e8f65659e2a4b9d9ab19fcb340f98->enter($__internal_727abd1f65a1877e6f4e11d419af1bdaf49e8f65659e2a4b9d9ab19fcb340f98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <section>
        <div id=\"contenu\" class=\"article-list\">
        <div class=\"container\">
            <div class=\"intro\">
                <h1 class=\"text-center\">";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "titre", array()), "html", null, true);
        echo " </h1>
                <!--<p class=\"text-center\"></p>-->
            </div>
            <div class=\"row articles\">
                <div class=\"col-md-12 col-sm-6 item\">
                  <a href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "image", array()), "html", null, true);
        echo "\"></a>
                    <!--<a href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "image", array())), "html", null, true);
        echo "\"></a> -->
                    <!--<h3 class=\"name\">Article Title</h3>-->
                    <p >";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "contenuFr", array()), "html", null, true);
        echo " </p>

                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-md-12\">
                    <a class=\"btn btn-default\" type=\"button\" id=\"btn_retour\" href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_equipe");
        echo "\">Retour aux actualités</a>
                </div>
            </div>

        </div>
        </div>
    </section>
";
        
        $__internal_727abd1f65a1877e6f4e11d419af1bdaf49e8f65659e2a4b9d9ab19fcb340f98->leave($__internal_727abd1f65a1877e6f4e11d419af1bdaf49e8f65659e2a4b9d9ab19fcb340f98_prof);

    }

    public function getTemplateName()
    {
        return "musikasvitrineBundle:Default:evenement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 23,  63 => 16,  58 => 14,  54 => 13,  46 => 8,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMere.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <section>*/
/*         <div id="contenu" class="article-list">*/
/*         <div class="container">*/
/*             <div class="intro">*/
/*                 <h1 class="text-center">{{ article.titre }} </h1>*/
/*                 <!--<p class="text-center"></p>-->*/
/*             </div>*/
/*             <div class="row articles">*/
/*                 <div class="col-md-12 col-sm-6 item">*/
/*                   <a href="#"><img class="img-responsive" src="{{ article.image }}"></a>*/
/*                     <!--<a href="#"><img class="img-responsive" src="{{ asset(article.image) }}"></a> -->*/
/*                     <!--<h3 class="name">Article Title</h3>-->*/
/*                     <p >{{ article.contenuFr }} </p>*/
/* */
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <a class="btn btn-default" type="button" id="btn_retour" href="{{ path('musikasvitrine_equipe') }}">Retour aux actualités</a>*/
/*                 </div>*/
/*             </div>*/
/* */
/*         </div>*/
/*         </div>*/
/*     </section>*/
/* {% endblock %}*/
