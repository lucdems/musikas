<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_16ac5dbf238dea0378e9bb481d924438f45cf4e87217d6108ff33bdd4e2f7603 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e5e49126ab0e4b2ef2470b87e119c084a79ad1eb673949f7fad3b24865ab224f = $this->env->getExtension("native_profiler");
        $__internal_e5e49126ab0e4b2ef2470b87e119c084a79ad1eb673949f7fad3b24865ab224f->enter($__internal_e5e49126ab0e4b2ef2470b87e119c084a79ad1eb673949f7fad3b24865ab224f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_e5e49126ab0e4b2ef2470b87e119c084a79ad1eb673949f7fad3b24865ab224f->leave($__internal_e5e49126ab0e4b2ef2470b87e119c084a79ad1eb673949f7fad3b24865ab224f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'search')) ?>*/
/* */
