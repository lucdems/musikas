<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_c942327346e88bfb70484aea6ee30b8e3a876360b0f28a40bcadb0eaf67b898e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e82f119daec6261e51965ff8c2e0ec87c319de56e36cfd00a077391837a73c49 = $this->env->getExtension("native_profiler");
        $__internal_e82f119daec6261e51965ff8c2e0ec87c319de56e36cfd00a077391837a73c49->enter($__internal_e82f119daec6261e51965ff8c2e0ec87c319de56e36cfd00a077391837a73c49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_e82f119daec6261e51965ff8c2e0ec87c319de56e36cfd00a077391837a73c49->leave($__internal_e82f119daec6261e51965ff8c2e0ec87c319de56e36cfd00a077391837a73c49_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
