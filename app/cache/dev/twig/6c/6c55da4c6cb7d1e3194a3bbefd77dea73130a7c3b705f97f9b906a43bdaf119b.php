<?php

/* EasyAdminBundle:default:field_smallint.html.twig */
class __TwigTemplate_a26649a859ee3f6da08257aff4dfdceb0151cbf4a32444b5f7bed5331c4034f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_65ebc56f3a0221d973c365c519b5fdc00b7aabaea087a1eafcc0b426b8ec515b = $this->env->getExtension("native_profiler");
        $__internal_65ebc56f3a0221d973c365c519b5fdc00b7aabaea087a1eafcc0b426b8ec515b->enter($__internal_65ebc56f3a0221d973c365c519b5fdc00b7aabaea087a1eafcc0b426b8ec515b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_smallint.html.twig"));

        // line 1
        if ($this->getAttribute((isset($context["field_options"]) ? $context["field_options"] : $this->getContext($context, "field_options")), "format", array())) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, sprintf($this->getAttribute((isset($context["field_options"]) ? $context["field_options"] : $this->getContext($context, "field_options")), "format", array()), (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        }
        
        $__internal_65ebc56f3a0221d973c365c519b5fdc00b7aabaea087a1eafcc0b426b8ec515b->leave($__internal_65ebc56f3a0221d973c365c519b5fdc00b7aabaea087a1eafcc0b426b8ec515b_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_smallint.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  24 => 2,  22 => 1,);
    }
}
/* {% if field_options.format %}*/
/*     {{ field_options.format|format(value) }}*/
/* {% else %}*/
/*     {{ value|number_format }}*/
/* {% endif %}*/
/* */
