<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_1e77784852bae0e8963c5d047d00e1175ce27cc9dfb473368b0f1a5f8d732aca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_de8f9796bff5408775084f0db3c255433ffa3f96d6aca22d7e359509f432a900 = $this->env->getExtension("native_profiler");
        $__internal_de8f9796bff5408775084f0db3c255433ffa3f96d6aca22d7e359509f432a900->enter($__internal_de8f9796bff5408775084f0db3c255433ffa3f96d6aca22d7e359509f432a900_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_de8f9796bff5408775084f0db3c255433ffa3f96d6aca22d7e359509f432a900->leave($__internal_de8f9796bff5408775084f0db3c255433ffa3f96d6aca22d7e359509f432a900_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($widget == 'single_text'): ?>*/
/*     <?php echo $view['form']->block($form, 'form_widget_simple'); ?>*/
/* <?php else: ?>*/
/*     <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*         <?php echo $view['form']->widget($form['date']).' '.$view['form']->widget($form['time']) ?>*/
/*     </div>*/
/* <?php endif ?>*/
/* */
