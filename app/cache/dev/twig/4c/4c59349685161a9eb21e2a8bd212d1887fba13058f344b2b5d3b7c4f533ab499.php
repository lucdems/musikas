<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_9ed08e8cdb3a7d2bea54cc08a32b10f89c0e874ee373a32b28b01caeca03c852 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_14d308d230699b8fc5cf55bf0896644a26add6ab5984355fc4a008b033b55eb2 = $this->env->getExtension("native_profiler");
        $__internal_14d308d230699b8fc5cf55bf0896644a26add6ab5984355fc4a008b033b55eb2->enter($__internal_14d308d230699b8fc5cf55bf0896644a26add6ab5984355fc4a008b033b55eb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_14d308d230699b8fc5cf55bf0896644a26add6ab5984355fc4a008b033b55eb2->leave($__internal_14d308d230699b8fc5cf55bf0896644a26add6ab5984355fc4a008b033b55eb2_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f38c8ba20cb8688a6803626ca788ccf1011ca01fee06c6a8bd01ef384a7b3039 = $this->env->getExtension("native_profiler");
        $__internal_f38c8ba20cb8688a6803626ca788ccf1011ca01fee06c6a8bd01ef384a7b3039->enter($__internal_f38c8ba20cb8688a6803626ca788ccf1011ca01fee06c6a8bd01ef384a7b3039_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_f38c8ba20cb8688a6803626ca788ccf1011ca01fee06c6a8bd01ef384a7b3039->leave($__internal_f38c8ba20cb8688a6803626ca788ccf1011ca01fee06c6a8bd01ef384a7b3039_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "@FOSUser/layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "@FOSUser/Profile/edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
