<?php

/* EasyAdminBundle:default:field_tel.html.twig */
class __TwigTemplate_7332355ca17d731b7be0ab3e0e15a852e88df2425a74b602d85ed62f19f0a72a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec3536cad5f252007caacf88fee3f64f3d88b026c88abaf4b0407e5b306aee5b = $this->env->getExtension("native_profiler");
        $__internal_ec3536cad5f252007caacf88fee3f64f3d88b026c88abaf4b0407e5b306aee5b->enter($__internal_ec3536cad5f252007caacf88fee3f64f3d88b026c88abaf4b0407e5b306aee5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_tel.html.twig"));

        // line 1
        echo "<a href=\"tel:";
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
        echo "</a>
";
        
        $__internal_ec3536cad5f252007caacf88fee3f64f3d88b026c88abaf4b0407e5b306aee5b->leave($__internal_ec3536cad5f252007caacf88fee3f64f3d88b026c88abaf4b0407e5b306aee5b_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_tel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <a href="tel:{{ value }}">{{ value }}</a>*/
/* */
