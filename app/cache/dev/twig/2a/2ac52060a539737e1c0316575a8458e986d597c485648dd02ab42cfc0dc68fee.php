<?php

/* EasyAdminBundle:default:field_bigint.html.twig */
class __TwigTemplate_96fc5eadb7d5f05fbf648303a24dd80f6c515ce56a2a5f4365508a289895d2e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2b8f5e8c39418f29894f4f859310ef22f05a77fc3badb3fbb171a2364da4352b = $this->env->getExtension("native_profiler");
        $__internal_2b8f5e8c39418f29894f4f859310ef22f05a77fc3badb3fbb171a2364da4352b->enter($__internal_2b8f5e8c39418f29894f4f859310ef22f05a77fc3badb3fbb171a2364da4352b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_bigint.html.twig"));

        // line 1
        if ($this->getAttribute((isset($context["field_options"]) ? $context["field_options"] : $this->getContext($context, "field_options")), "format", array())) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, sprintf($this->getAttribute((isset($context["field_options"]) ? $context["field_options"] : $this->getContext($context, "field_options")), "format", array()), (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        }
        
        $__internal_2b8f5e8c39418f29894f4f859310ef22f05a77fc3badb3fbb171a2364da4352b->leave($__internal_2b8f5e8c39418f29894f4f859310ef22f05a77fc3badb3fbb171a2364da4352b_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_bigint.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  24 => 2,  22 => 1,);
    }
}
/* {% if field_options.format %}*/
/*     {{ field_options.format|format(value) }}*/
/* {% else %}*/
/*     {{ value|number_format }}*/
/* {% endif %}*/
/* */
