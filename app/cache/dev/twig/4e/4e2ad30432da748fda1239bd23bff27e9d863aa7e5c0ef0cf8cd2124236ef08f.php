<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_ea6b5c57ef6a7aa36a012e3507a8e31a81cc82e831f7669b07914490d27a367d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ead600315cc3b20092a407dfa1a8c7dec30a43ab844f1fd241f0763a8732eead = $this->env->getExtension("native_profiler");
        $__internal_ead600315cc3b20092a407dfa1a8c7dec30a43ab844f1fd241f0763a8732eead->enter($__internal_ead600315cc3b20092a407dfa1a8c7dec30a43ab844f1fd241f0763a8732eead_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ead600315cc3b20092a407dfa1a8c7dec30a43ab844f1fd241f0763a8732eead->leave($__internal_ead600315cc3b20092a407dfa1a8c7dec30a43ab844f1fd241f0763a8732eead_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_259281de32225377327ca458f1a36e0864be93eb1e1e0141dfc15dcbd7c2ee2d = $this->env->getExtension("native_profiler");
        $__internal_259281de32225377327ca458f1a36e0864be93eb1e1e0141dfc15dcbd7c2ee2d->enter($__internal_259281de32225377327ca458f1a36e0864be93eb1e1e0141dfc15dcbd7c2ee2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_259281de32225377327ca458f1a36e0864be93eb1e1e0141dfc15dcbd7c2ee2d->leave($__internal_259281de32225377327ca458f1a36e0864be93eb1e1e0141dfc15dcbd7c2ee2d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "@FOSUser/layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "@FOSUser/Resetting/request_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
