<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_d922e2b89814005685f23e48fa171fa4f051f1ff0c7886274735d90319dc6d0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf20e9d2111a5855f980da0fc241fb179b704dfc8b6127063d3920dff6562014 = $this->env->getExtension("native_profiler");
        $__internal_cf20e9d2111a5855f980da0fc241fb179b704dfc8b6127063d3920dff6562014->enter($__internal_cf20e9d2111a5855f980da0fc241fb179b704dfc8b6127063d3920dff6562014_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cf20e9d2111a5855f980da0fc241fb179b704dfc8b6127063d3920dff6562014->leave($__internal_cf20e9d2111a5855f980da0fc241fb179b704dfc8b6127063d3920dff6562014_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_2f5988be61b26acd997e2017ac53f5aec18239efd061de293382b0f399298fdb = $this->env->getExtension("native_profiler");
        $__internal_2f5988be61b26acd997e2017ac53f5aec18239efd061de293382b0f399298fdb->enter($__internal_2f5988be61b26acd997e2017ac53f5aec18239efd061de293382b0f399298fdb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_2f5988be61b26acd997e2017ac53f5aec18239efd061de293382b0f399298fdb->leave($__internal_2f5988be61b26acd997e2017ac53f5aec18239efd061de293382b0f399298fdb_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "@FOSUser/layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "@FOSUser/Group/new_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
