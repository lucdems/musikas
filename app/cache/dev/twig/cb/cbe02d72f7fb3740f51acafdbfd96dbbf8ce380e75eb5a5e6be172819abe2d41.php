<?php

/* EasyAdminBundle:default:field_datetimetz.html.twig */
class __TwigTemplate_c10b50067bdfd38a0a29be55f048befb7ca4852e42dcbfe17b5018d2bd366b16 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fe8895780c63c597d5fc01a8e299681cc06dac0638d90e6e70246aeb8920a48e = $this->env->getExtension("native_profiler");
        $__internal_fe8895780c63c597d5fc01a8e299681cc06dac0638d90e6e70246aeb8920a48e->enter($__internal_fe8895780c63c597d5fc01a8e299681cc06dac0638d90e6e70246aeb8920a48e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_datetimetz.html.twig"));

        // line 1
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), $this->getAttribute((isset($context["field_options"]) ? $context["field_options"] : $this->getContext($context, "field_options")), "format", array())), "html", null, true);
        echo "
";
        
        $__internal_fe8895780c63c597d5fc01a8e299681cc06dac0638d90e6e70246aeb8920a48e->leave($__internal_fe8895780c63c597d5fc01a8e299681cc06dac0638d90e6e70246aeb8920a48e_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_datetimetz.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ value|date(field_options.format) }}*/
/* */
