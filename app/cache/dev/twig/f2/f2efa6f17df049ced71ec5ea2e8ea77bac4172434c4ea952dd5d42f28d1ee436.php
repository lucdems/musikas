<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_23d8440e216228664acb6dfb75f4915be02653dd540ce6e624131b73e2562c2e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a7aafd31548800078720bb21e3481889ef140b058abcdc1c170c02cb1832af34 = $this->env->getExtension("native_profiler");
        $__internal_a7aafd31548800078720bb21e3481889ef140b058abcdc1c170c02cb1832af34->enter($__internal_a7aafd31548800078720bb21e3481889ef140b058abcdc1c170c02cb1832af34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a7aafd31548800078720bb21e3481889ef140b058abcdc1c170c02cb1832af34->leave($__internal_a7aafd31548800078720bb21e3481889ef140b058abcdc1c170c02cb1832af34_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_8faa334f867af5ef19cac385f81ec107a11228027ab54be66026a4e5b2e85e43 = $this->env->getExtension("native_profiler");
        $__internal_8faa334f867af5ef19cac385f81ec107a11228027ab54be66026a4e5b2e85e43->enter($__internal_8faa334f867af5ef19cac385f81ec107a11228027ab54be66026a4e5b2e85e43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_8faa334f867af5ef19cac385f81ec107a11228027ab54be66026a4e5b2e85e43->leave($__internal_8faa334f867af5ef19cac385f81ec107a11228027ab54be66026a4e5b2e85e43_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "@FOSUser/layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/*     {{ include('@FOSUser/Security/login_content.html.twig') }}*/
/* {% endblock fos_user_content %}*/
/* */
