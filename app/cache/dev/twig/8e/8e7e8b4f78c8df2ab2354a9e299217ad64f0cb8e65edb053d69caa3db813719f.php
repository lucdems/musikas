<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_4c970ee622454b319872cc6a0fbad701e43988fa1af96795f1a9c6658cad121a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6015e7d7fae47933e618f803b31a30d3d94baa6cfd4ff27237a321bbbf46e63f = $this->env->getExtension("native_profiler");
        $__internal_6015e7d7fae47933e618f803b31a30d3d94baa6cfd4ff27237a321bbbf46e63f->enter($__internal_6015e7d7fae47933e618f803b31a30d3d94baa6cfd4ff27237a321bbbf46e63f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_6015e7d7fae47933e618f803b31a30d3d94baa6cfd4ff27237a321bbbf46e63f->leave($__internal_6015e7d7fae47933e618f803b31a30d3d94baa6cfd4ff27237a321bbbf46e63f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr style="display: none">*/
/*     <td colspan="2">*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
