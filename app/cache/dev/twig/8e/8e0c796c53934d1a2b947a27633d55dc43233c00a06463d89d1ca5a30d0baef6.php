<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_59530f5832398b0361105a2e02194d4b2482ad8c4912f8631acd2bf848bafb54 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMerePrive.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMerePrive.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5afdb401af4244417602b7ea43f73512154dd6ade460312ed8a2037d255da3ad = $this->env->getExtension("native_profiler");
        $__internal_5afdb401af4244417602b7ea43f73512154dd6ade460312ed8a2037d255da3ad->enter($__internal_5afdb401af4244417602b7ea43f73512154dd6ade460312ed8a2037d255da3ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5afdb401af4244417602b7ea43f73512154dd6ade460312ed8a2037d255da3ad->leave($__internal_5afdb401af4244417602b7ea43f73512154dd6ade460312ed8a2037d255da3ad_prof);

    }

    // line 4
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_811ffc426097f40f39b0f6399395a378df77976a56dfea0edd29a9dbb496a9ea = $this->env->getExtension("native_profiler");
        $__internal_811ffc426097f40f39b0f6399395a378df77976a56dfea0edd29a9dbb496a9ea->enter($__internal_811ffc426097f40f39b0f6399395a378df77976a56dfea0edd29a9dbb496a9ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 5
        $this->displayBlock('fos_user_content', $context, $blocks);
        
        $__internal_811ffc426097f40f39b0f6399395a378df77976a56dfea0edd29a9dbb496a9ea->leave($__internal_811ffc426097f40f39b0f6399395a378df77976a56dfea0edd29a9dbb496a9ea_prof);

    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_738a9637aea14d965d2cd095102adef908d47c682355a41bf70bccf68395d10a = $this->env->getExtension("native_profiler");
        $__internal_738a9637aea14d965d2cd095102adef908d47c682355a41bf70bccf68395d10a->enter($__internal_738a9637aea14d965d2cd095102adef908d47c682355a41bf70bccf68395d10a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        // line 10
        echo "
";
        
        $__internal_738a9637aea14d965d2cd095102adef908d47c682355a41bf70bccf68395d10a->leave($__internal_738a9637aea14d965d2cd095102adef908d47c682355a41bf70bccf68395d10a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 10,  60 => 8,  58 => 7,  53 => 6,  41 => 5,  35 => 4,  11 => 1,);
    }
}
/* {% extends "::default/vueMerePrive.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block contenu %}*/
/* {% block fos_user_content %}*/
/*     <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>*/
/*     {% if targetUrl %}*/
/*     <p><a href="{{ targetUrl }}">{{ 'registration.back'|trans }}</a></p>*/
/*     {% endif %}*/
/* */
/* {% endblock fos_user_content %}*/
/* {% endblock contenu %}*/
