<?php

/* @EasyAdmin/default/edit.html.twig */
class __TwigTemplate_6291c578c6cc3ec042cd6c0b626505c7bc5398a5fa6d0ca0eb8951d8bd7ea371 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'content_title' => array($this, 'block_content_title'),
            'main' => array($this, 'block_main'),
            'entity_form' => array($this, 'block_entity_form'),
            'delete_form' => array($this, 'block_delete_form'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 8
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "templates", array()), "layout", array()), "@EasyAdmin/default/edit.html.twig", 8);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_521fd3cdfbc95c3d98ccde75fab4e301f6522dd06a0ffb4aa4a5aba9210e8725 = $this->env->getExtension("native_profiler");
        $__internal_521fd3cdfbc95c3d98ccde75fab4e301f6522dd06a0ffb4aa4a5aba9210e8725->enter($__internal_521fd3cdfbc95c3d98ccde75fab4e301f6522dd06a0ffb4aa4a5aba9210e8725_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/edit.html.twig"));

        // line 1
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), $this->env->getExtension('easyadmin_extension')->getBackendConfiguration("design.form_theme"));
        // line 3
        $context["_entity_config"] = $this->env->getExtension('easyadmin_extension')->getEntityConfiguration($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "entity"), "method"));
        // line 4
        $context["_entity_id"] = $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), $this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "primary_key_field_name", array()));
        // line 5
        $context["__internal_3ea5e13b798a8317a28965b02657d5b9ae8321a9a790078a4f646063b3009684"] = $this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "translation_domain", array());
        // line 6
        $context["_trans_parameters"] = array("%entity_name%" => $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "name", array()), array(),         // line 5
(isset($context["__internal_3ea5e13b798a8317a28965b02657d5b9ae8321a9a790078a4f646063b3009684"]) ? $context["__internal_3ea5e13b798a8317a28965b02657d5b9ae8321a9a790078a4f646063b3009684"] : $this->getContext($context, "__internal_3ea5e13b798a8317a28965b02657d5b9ae8321a9a790078a4f646063b3009684"))), "%entity_label%" => $this->env->getExtension('translator')->trans($this->getAttribute(        // line 6
(isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "label", array()), array(),         // line 5
(isset($context["__internal_3ea5e13b798a8317a28965b02657d5b9ae8321a9a790078a4f646063b3009684"]) ? $context["__internal_3ea5e13b798a8317a28965b02657d5b9ae8321a9a790078a4f646063b3009684"] : $this->getContext($context, "__internal_3ea5e13b798a8317a28965b02657d5b9ae8321a9a790078a4f646063b3009684"))), "%entity_id%" =>         // line 6
(isset($context["_entity_id"]) ? $context["_entity_id"] : $this->getContext($context, "_entity_id")));
        // line 8
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_521fd3cdfbc95c3d98ccde75fab4e301f6522dd06a0ffb4aa4a5aba9210e8725->leave($__internal_521fd3cdfbc95c3d98ccde75fab4e301f6522dd06a0ffb4aa4a5aba9210e8725_prof);

    }

    // line 10
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_e31547b85d0b104a2c710f6a0d98ab793b5442d62a3c27161e9e2813b863ffe5 = $this->env->getExtension("native_profiler");
        $__internal_e31547b85d0b104a2c710f6a0d98ab793b5442d62a3c27161e9e2813b863ffe5->enter($__internal_e31547b85d0b104a2c710f6a0d98ab793b5442d62a3c27161e9e2813b863ffe5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo twig_escape_filter($this->env, ((("easyadmin-edit-" . $this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "name", array())) . "-") . (isset($context["_entity_id"]) ? $context["_entity_id"] : $this->getContext($context, "_entity_id"))), "html", null, true);
        
        $__internal_e31547b85d0b104a2c710f6a0d98ab793b5442d62a3c27161e9e2813b863ffe5->leave($__internal_e31547b85d0b104a2c710f6a0d98ab793b5442d62a3c27161e9e2813b863ffe5_prof);

    }

    // line 11
    public function block_body_class($context, array $blocks = array())
    {
        $__internal_5a0a1ad3550513513887298379ba9995cf5b4d2197ba90f631d7fb795bdf23d5 = $this->env->getExtension("native_profiler");
        $__internal_5a0a1ad3550513513887298379ba9995cf5b4d2197ba90f631d7fb795bdf23d5->enter($__internal_5a0a1ad3550513513887298379ba9995cf5b4d2197ba90f631d7fb795bdf23d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        echo twig_escape_filter($this->env, ("edit edit-" . twig_lower_filter($this->env, $this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "name", array()))), "html", null, true);
        
        $__internal_5a0a1ad3550513513887298379ba9995cf5b4d2197ba90f631d7fb795bdf23d5->leave($__internal_5a0a1ad3550513513887298379ba9995cf5b4d2197ba90f631d7fb795bdf23d5_prof);

    }

    // line 13
    public function block_content_title($context, array $blocks = array())
    {
        $__internal_6930e78e19b3a35f525cae0e67a554674ab42d49a32ddcd8be8b29019558bf02 = $this->env->getExtension("native_profiler");
        $__internal_6930e78e19b3a35f525cae0e67a554674ab42d49a32ddcd8be8b29019558bf02->enter($__internal_6930e78e19b3a35f525cae0e67a554674ab42d49a32ddcd8be8b29019558bf02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        // line 14
        ob_start();
        // line 15
        echo "    ";
        $context["_default_title"] = $this->env->getExtension('translator')->trans("edit.page_title", (isset($context["_trans_parameters"]) ? $context["_trans_parameters"] : $this->getContext($context, "_trans_parameters")), "EasyAdminBundle");
        // line 16
        echo "    ";
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : null), "edit", array(), "any", false, true), "title", array(), "any", true, true)) ? ($this->env->getExtension('translator')->trans($this->getAttribute($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "edit", array()), "title", array()), (isset($context["_trans_parameters"]) ? $context["_trans_parameters"] : $this->getContext($context, "_trans_parameters")),         // line 5
(isset($context["__internal_3ea5e13b798a8317a28965b02657d5b9ae8321a9a790078a4f646063b3009684"]) ? $context["__internal_3ea5e13b798a8317a28965b02657d5b9ae8321a9a790078a4f646063b3009684"] : $this->getContext($context, "__internal_3ea5e13b798a8317a28965b02657d5b9ae8321a9a790078a4f646063b3009684")))) : (        // line 16
(isset($context["_default_title"]) ? $context["_default_title"] : $this->getContext($context, "_default_title")))), "html", null, true);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_6930e78e19b3a35f525cae0e67a554674ab42d49a32ddcd8be8b29019558bf02->leave($__internal_6930e78e19b3a35f525cae0e67a554674ab42d49a32ddcd8be8b29019558bf02_prof);

    }

    // line 20
    public function block_main($context, array $blocks = array())
    {
        $__internal_22cf338118635b5f3bb7a27ee2dc7a0fcc58dbed98b6686db86555267df9cc1f = $this->env->getExtension("native_profiler");
        $__internal_22cf338118635b5f3bb7a27ee2dc7a0fcc58dbed98b6686db86555267df9cc1f->enter($__internal_22cf338118635b5f3bb7a27ee2dc7a0fcc58dbed98b6686db86555267df9cc1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 21
        echo "    ";
        $this->displayBlock('entity_form', $context, $blocks);
        // line 24
        echo "
    ";
        // line 25
        $this->displayBlock('delete_form', $context, $blocks);
        
        $__internal_22cf338118635b5f3bb7a27ee2dc7a0fcc58dbed98b6686db86555267df9cc1f->leave($__internal_22cf338118635b5f3bb7a27ee2dc7a0fcc58dbed98b6686db86555267df9cc1f_prof);

    }

    // line 21
    public function block_entity_form($context, array $blocks = array())
    {
        $__internal_57022de96ee05292e4ce827a290889f2185acca19aad2038c3f6bdd72eae3795 = $this->env->getExtension("native_profiler");
        $__internal_57022de96ee05292e4ce827a290889f2185acca19aad2038c3f6bdd72eae3795->enter($__internal_57022de96ee05292e4ce827a290889f2185acca19aad2038c3f6bdd72eae3795_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        // line 22
        echo "        ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
    ";
        
        $__internal_57022de96ee05292e4ce827a290889f2185acca19aad2038c3f6bdd72eae3795->leave($__internal_57022de96ee05292e4ce827a290889f2185acca19aad2038c3f6bdd72eae3795_prof);

    }

    // line 25
    public function block_delete_form($context, array $blocks = array())
    {
        $__internal_024ca18c175119366f32f73751b5426170346590d077ce57ea10ca5d85f3b62c = $this->env->getExtension("native_profiler");
        $__internal_024ca18c175119366f32f73751b5426170346590d077ce57ea10ca5d85f3b62c->enter($__internal_024ca18c175119366f32f73751b5426170346590d077ce57ea10ca5d85f3b62c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "delete_form"));

        // line 26
        echo "        ";
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_delete_form.html.twig", array("view" => "edit", "referer" => $this->getAttribute($this->getAttribute($this->getAttribute(        // line 28
(isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "referer", 1 => ""), "method"), "delete_form" =>         // line 29
(isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), "_translation_domain" => $this->getAttribute(        // line 30
(isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "translation_domain", array()), "_trans_parameters" =>         // line 31
(isset($context["_trans_parameters"]) ? $context["_trans_parameters"] : $this->getContext($context, "_trans_parameters")), "_entity_config" =>         // line 32
(isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config"))), false);
        // line 33
        echo "
    ";
        
        $__internal_024ca18c175119366f32f73751b5426170346590d077ce57ea10ca5d85f3b62c->leave($__internal_024ca18c175119366f32f73751b5426170346590d077ce57ea10ca5d85f3b62c_prof);

    }

    // line 37
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_2c545336f7cdcd4110b0fe1a2a5352abb230c9fd64a04a6a8d54710fd8fb82b0 = $this->env->getExtension("native_profiler");
        $__internal_2c545336f7cdcd4110b0fe1a2a5352abb230c9fd64a04a6a8d54710fd8fb82b0->enter($__internal_2c545336f7cdcd4110b0fe1a2a5352abb230c9fd64a04a6a8d54710fd8fb82b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        // line 38
        echo "    ";
        $this->displayParentBlock("body_javascript", $context, $blocks);
        echo "

    <script type=\"text/javascript\">
        \$(function() {
            \$('.edit-form').areYouSure({ 'message': '";
        // line 42
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("form.are_you_sure", array(), "EasyAdminBundle"), "js"), "html", null, true);
        echo "' });

            \$('.form-actions').easyAdminSticky();

            \$('a.action-delete').on('click', function(e) {
                e.preventDefault();

                \$('#modal-delete').modal({ backdrop: true, keyboard: true })
                    .off('click', '#modal-delete-button')
                    .on('click', '#modal-delete-button', function () {
                        \$('#delete-form').trigger('submit');
                    });
            });
        });
    </script>

    ";
        // line 58
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_select2_widget.html.twig");
        echo "
";
        
        $__internal_2c545336f7cdcd4110b0fe1a2a5352abb230c9fd64a04a6a8d54710fd8fb82b0->leave($__internal_2c545336f7cdcd4110b0fe1a2a5352abb230c9fd64a04a6a8d54710fd8fb82b0_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 58,  173 => 42,  165 => 38,  159 => 37,  151 => 33,  149 => 32,  148 => 31,  147 => 30,  146 => 29,  145 => 28,  143 => 26,  137 => 25,  127 => 22,  121 => 21,  114 => 25,  111 => 24,  108 => 21,  102 => 20,  92 => 16,  91 => 5,  89 => 16,  86 => 15,  84 => 14,  78 => 13,  66 => 11,  54 => 10,  47 => 8,  45 => 6,  44 => 5,  43 => 6,  42 => 5,  41 => 6,  39 => 5,  37 => 4,  35 => 3,  33 => 1,  24 => 8,);
    }
}
/* {% form_theme form with easyadmin_config('design.form_theme') %}*/
/* */
/* {% set _entity_config = easyadmin_entity(app.request.query.get('entity')) %}*/
/* {% set _entity_id = attribute(entity, _entity_config.primary_key_field_name) %}*/
/* {% trans_default_domain _entity_config.translation_domain %}*/
/* {% set _trans_parameters = { '%entity_name%': _entity_config.name|trans, '%entity_label%': _entity_config.label|trans, '%entity_id%': _entity_id } %}*/
/* */
/* {% extends _entity_config.templates.layout %}*/
/* */
/* {% block body_id 'easyadmin-edit-' ~ _entity_config.name ~ '-' ~ _entity_id %}*/
/* {% block body_class 'edit edit-' ~ _entity_config.name|lower %}*/
/* */
/* {% block content_title %}*/
/* {% spaceless %}*/
/*     {% set _default_title = 'edit.page_title'|trans(_trans_parameters, 'EasyAdminBundle') %}*/
/*     {{ _entity_config.edit.title is defined ? _entity_config.edit.title|trans(_trans_parameters) : _default_title }}*/
/* {% endspaceless %}*/
/* {% endblock %}*/
/* */
/* {% block main %}*/
/*     {% block entity_form %}*/
/*         {{ form(form) }}*/
/*     {% endblock entity_form %}*/
/* */
/*     {% block delete_form %}*/
/*         {{ include('@EasyAdmin/default/includes/_delete_form.html.twig', {*/
/*             view: 'edit',*/
/*             referer: app.request.query.get('referer', ''),*/
/*             delete_form: delete_form,*/
/*             _translation_domain: _entity_config.translation_domain,*/
/*             _trans_parameters: _trans_parameters,*/
/*             _entity_config: _entity_config,*/
/*         }, with_context = false) }}*/
/*     {% endblock delete_form %}*/
/* {% endblock %}*/
/* */
/* {% block body_javascript %}*/
/*     {{ parent() }}*/
/* */
/*     <script type="text/javascript">*/
/*         $(function() {*/
/*             $('.edit-form').areYouSure({ 'message': '{{ 'form.are_you_sure'|trans({}, 'EasyAdminBundle')|e('js') }}' });*/
/* */
/*             $('.form-actions').easyAdminSticky();*/
/* */
/*             $('a.action-delete').on('click', function(e) {*/
/*                 e.preventDefault();*/
/* */
/*                 $('#modal-delete').modal({ backdrop: true, keyboard: true })*/
/*                     .off('click', '#modal-delete-button')*/
/*                     .on('click', '#modal-delete-button', function () {*/
/*                         $('#delete-form').trigger('submit');*/
/*                     });*/
/*             });*/
/*         });*/
/*     </script>*/
/* */
/*     {{ include('@EasyAdmin/default/includes/_select2_widget.html.twig') }}*/
/* {% endblock %}*/
/* */
