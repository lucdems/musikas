<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_9eaabc1e394f8ca521e72d7cc295d24afb2fdfe4b1a9a7e53decd31ce524daea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0840bcbb0fa21835161c56c30a271fe19027be30bd74e32cbd8a1de0a6cfdeb2 = $this->env->getExtension("native_profiler");
        $__internal_0840bcbb0fa21835161c56c30a271fe19027be30bd74e32cbd8a1de0a6cfdeb2->enter($__internal_0840bcbb0fa21835161c56c30a271fe19027be30bd74e32cbd8a1de0a6cfdeb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_0840bcbb0fa21835161c56c30a271fe19027be30bd74e32cbd8a1de0a6cfdeb2->leave($__internal_0840bcbb0fa21835161c56c30a271fe19027be30bd74e32cbd8a1de0a6cfdeb2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="radio"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     value="<?php echo $view->escape($value) ?>"*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
