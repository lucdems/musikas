<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_8b45a02b380197efa9c357da873f099e1912c348213806b99507adba767f1286 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aae0982c551fb36c06d15d9a3734223f32b362a0f55b60a775216e6aab010ee6 = $this->env->getExtension("native_profiler");
        $__internal_aae0982c551fb36c06d15d9a3734223f32b362a0f55b60a775216e6aab010ee6->enter($__internal_aae0982c551fb36c06d15d9a3734223f32b362a0f55b60a775216e6aab010ee6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_aae0982c551fb36c06d15d9a3734223f32b362a0f55b60a775216e6aab010ee6->leave($__internal_aae0982c551fb36c06d15d9a3734223f32b362a0f55b60a775216e6aab010ee6_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_18da6e5b7f314517f15da501d3df9a8f0245026f788447bb40a50d2860eb1969 = $this->env->getExtension("native_profiler");
        $__internal_18da6e5b7f314517f15da501d3df9a8f0245026f788447bb40a50d2860eb1969->enter($__internal_18da6e5b7f314517f15da501d3df9a8f0245026f788447bb40a50d2860eb1969_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_18da6e5b7f314517f15da501d3df9a8f0245026f788447bb40a50d2860eb1969->leave($__internal_18da6e5b7f314517f15da501d3df9a8f0245026f788447bb40a50d2860eb1969_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "@FOSUser/layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "@FOSUser/Resetting/reset_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
