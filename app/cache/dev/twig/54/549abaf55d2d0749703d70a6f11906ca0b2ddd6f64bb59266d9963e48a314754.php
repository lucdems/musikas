<?php

/* musikasespacePriveBundle:Default:espace_prive.html.twig */
class __TwigTemplate_58b03cce0f51f807f75ce0d2476b0bdf5279d7a1cd7707fa45a5d7b54b4337c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMerePrive.html.twig", "musikasespacePriveBundle:Default:espace_prive.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMerePrive.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_71f66a66bcdda65bb49951030d8a44641d68d810b80b3f1b8791d221286ada6a = $this->env->getExtension("native_profiler");
        $__internal_71f66a66bcdda65bb49951030d8a44641d68d810b80b3f1b8791d221286ada6a->enter($__internal_71f66a66bcdda65bb49951030d8a44641d68d810b80b3f1b8791d221286ada6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasespacePriveBundle:Default:espace_prive.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_71f66a66bcdda65bb49951030d8a44641d68d810b80b3f1b8791d221286ada6a->leave($__internal_71f66a66bcdda65bb49951030d8a44641d68d810b80b3f1b8791d221286ada6a_prof);

    }

    public function block_contenu($context, array $blocks = array())
    {
        $__internal_6e9fff16b7034741cce230d7900bd390102f70f5489fbd3790b9b4763e75d64e = $this->env->getExtension("native_profiler");
        $__internal_6e9fff16b7034741cce230d7900bd390102f70f5489fbd3790b9b4763e75d64e->enter($__internal_6e9fff16b7034741cce230d7900bd390102f70f5489fbd3790b9b4763e75d64e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 2
        echo "<div class=\"container\" id=\"contenu_site\">
    <div class=\"row articles\">
        ";
        // line 4
        echo twig_include($this->env, $context, "default/barre.html.twig");
        echo "

        <div class=\"col-md-8 item\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12\">

                            <link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
                            <p></p>
                            <h1>Bienvenue dans votre espace privé!</h1>
                            <p>Vous pouvez ici profiter des fonctionnalités de votre espace privé en consultant vos documents.</p>
                            <p></p>
                            <p></p>

                            <div class=\"row\">
                                <!--<div class=\"col-lg-3 col-md-6\">
                                    <div class=\"panel panel-primary\">
                                        <div class=\"panel-heading\">
                                            <div class=\"row\">
                                                <div class=\"col-xs-3\"><i class=\"fa fa-file-text  fa-4x\"></i></div>
                                                <div class=\"col-xs-9 text-right\">
                                                    <div class=\"huge\">26</div>
                                                    <div>Documents</div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href=\"#\">
                                            <div class=\"panel-footer\"><span class=\"pull-left\">View Details</span><span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>
                                                <div class=\"clearfix\"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>-->
                                <div class=\"col-lg-4 col-md-6\">
                                    <div class=\"panel panel-green\">
                                        <div class=\"panel-heading\">
                                            <div class=\"row\">
                                                <div class=\"col-xs-4\"><i class=\"fa fa-file-text fa-4x\"></i></div>
                                                <div class=\"col-xs-8 text-right\">
                                                    <div class=\"huge\"></div>
                                                    <div>Documents</div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href=\"#\">
                                            <div class=\"panel-footer\"><span class=\"pull-left\">Consulter</span><span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>
                                                <div class=\"clearfix\"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class=\"col-lg-4 col-md-6\">
                                    <div class=\"panel panel-yellow\">
                                        <div class=\"panel-heading\">
                                            <div class=\"row\">
                                                <div class=\"col-xs-4\"><i class=\"fa fa-pencil-square-o  fa-4x\"></i></div>
                                                <div class=\"col-xs-8 text-right\">
                                                    <div>Inscription</div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href=\"#\">
                                            <div class=\"panel-footer\"><span class=\"pull-left\">Consulter</span><span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>
                                                <div class=\"clearfix\"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class=\"col-lg-4 col-md-6\">
                                    <div class=\"panel panel-red\">
                                        <div class=\"panel-heading\">
                                            <div class=\"row\">
                                                <div class=\"col-xs-4\"><i class=\"fa fa-user fa-4x\"></i></div>
                                                <div class=\"col-xs-8 text-right\">
                                                    <div>Profil</div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href=\"#\">
                                            <div class=\"panel-footer\"><span class=\"pull-left\">Consulter</span><span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>
                                                <div class=\"clearfix\"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</section>
";
        
        $__internal_6e9fff16b7034741cce230d7900bd390102f70f5489fbd3790b9b4763e75d64e->leave($__internal_6e9fff16b7034741cce230d7900bd390102f70f5489fbd3790b9b4763e75d64e_prof);

    }

    public function getTemplateName()
    {
        return "musikasespacePriveBundle:Default:espace_prive.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 4,  39 => 2,  11 => 1,);
    }
}
/* {% extends "::default/vueMerePrive.html.twig" %} {% block contenu %}*/
/* <div class="container" id="contenu_site">*/
/*     <div class="row articles">*/
/*         {{ include('default/barre.html.twig') }}*/
/* */
/*         <div class="col-md-8 item">*/
/*             <div class="panel panel-default">*/
/*                 <div class="panel-body">*/
/*                     <div class="row">*/
/*                         <div class="col-md-12">*/
/* */
/*                             <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />*/
/*                             <p></p>*/
/*                             <h1>Bienvenue dans votre espace privé!</h1>*/
/*                             <p>Vous pouvez ici profiter des fonctionnalités de votre espace privé en consultant vos documents.</p>*/
/*                             <p></p>*/
/*                             <p></p>*/
/* */
/*                             <div class="row">*/
/*                                 <!--<div class="col-lg-3 col-md-6">*/
/*                                     <div class="panel panel-primary">*/
/*                                         <div class="panel-heading">*/
/*                                             <div class="row">*/
/*                                                 <div class="col-xs-3"><i class="fa fa-file-text  fa-4x"></i></div>*/
/*                                                 <div class="col-xs-9 text-right">*/
/*                                                     <div class="huge">26</div>*/
/*                                                     <div>Documents</div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <a href="#">*/
/*                                             <div class="panel-footer"><span class="pull-left">View Details</span><span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>*/
/*                                                 <div class="clearfix"></div>*/
/*                                             </div>*/
/*                                         </a>*/
/*                                     </div>*/
/*                                 </div>-->*/
/*                                 <div class="col-lg-4 col-md-6">*/
/*                                     <div class="panel panel-green">*/
/*                                         <div class="panel-heading">*/
/*                                             <div class="row">*/
/*                                                 <div class="col-xs-4"><i class="fa fa-file-text fa-4x"></i></div>*/
/*                                                 <div class="col-xs-8 text-right">*/
/*                                                     <div class="huge"></div>*/
/*                                                     <div>Documents</div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <a href="#">*/
/*                                             <div class="panel-footer"><span class="pull-left">Consulter</span><span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>*/
/*                                                 <div class="clearfix"></div>*/
/*                                             </div>*/
/*                                         </a>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="col-lg-4 col-md-6">*/
/*                                     <div class="panel panel-yellow">*/
/*                                         <div class="panel-heading">*/
/*                                             <div class="row">*/
/*                                                 <div class="col-xs-4"><i class="fa fa-pencil-square-o  fa-4x"></i></div>*/
/*                                                 <div class="col-xs-8 text-right">*/
/*                                                     <div>Inscription</div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <a href="#">*/
/*                                             <div class="panel-footer"><span class="pull-left">Consulter</span><span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>*/
/*                                                 <div class="clearfix"></div>*/
/*                                             </div>*/
/*                                         </a>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="col-lg-4 col-md-6">*/
/*                                     <div class="panel panel-red">*/
/*                                         <div class="panel-heading">*/
/*                                             <div class="row">*/
/*                                                 <div class="col-xs-4"><i class="fa fa-user fa-4x"></i></div>*/
/*                                                 <div class="col-xs-8 text-right">*/
/*                                                     <div>Profil</div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <a href="#">*/
/*                                             <div class="panel-footer"><span class="pull-left">Consulter</span><span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>*/
/*                                                 <div class="clearfix"></div>*/
/*                                             </div>*/
/*                                         </a>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* </section>*/
/* </section>*/
/* {% endblock %}*/
