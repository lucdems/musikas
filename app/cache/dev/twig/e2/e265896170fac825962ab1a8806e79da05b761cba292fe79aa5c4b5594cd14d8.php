<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_e5ebac2537fc178bf7749bfbaaf548c2490a5a0750bbdb9a80809b0fc52a05df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_197e5757bc5dd6a38e9d92f8d6591f487af48627eca23e0d001aac0b0daf7813 = $this->env->getExtension("native_profiler");
        $__internal_197e5757bc5dd6a38e9d92f8d6591f487af48627eca23e0d001aac0b0daf7813->enter($__internal_197e5757bc5dd6a38e9d92f8d6591f487af48627eca23e0d001aac0b0daf7813_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_197e5757bc5dd6a38e9d92f8d6591f487af48627eca23e0d001aac0b0daf7813->leave($__internal_197e5757bc5dd6a38e9d92f8d6591f487af48627eca23e0d001aac0b0daf7813_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'password')) ?>*/
/* */
