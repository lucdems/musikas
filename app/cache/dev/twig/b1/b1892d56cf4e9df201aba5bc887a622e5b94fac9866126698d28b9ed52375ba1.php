<?php

/* musikasvitrineBundle:Default:ressources.html.twig */
class __TwigTemplate_be62823372dddc96ab961f6c4cf77dc73a51cdd4ae583a66642f02f38f469dfb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMere.html.twig", "musikasvitrineBundle:Default:ressources.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMere.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_65c984b9908ee4731cd129d2add22f302f53b351ec3aa7fa3a2215fc8e205c9d = $this->env->getExtension("native_profiler");
        $__internal_65c984b9908ee4731cd129d2add22f302f53b351ec3aa7fa3a2215fc8e205c9d->enter($__internal_65c984b9908ee4731cd129d2add22f302f53b351ec3aa7fa3a2215fc8e205c9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasvitrineBundle:Default:ressources.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_65c984b9908ee4731cd129d2add22f302f53b351ec3aa7fa3a2215fc8e205c9d->leave($__internal_65c984b9908ee4731cd129d2add22f302f53b351ec3aa7fa3a2215fc8e205c9d_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_f55bb9f6d88124bf27df54b7ec277a18738f08070941109ef64450cbb8a20bb4 = $this->env->getExtension("native_profiler");
        $__internal_f55bb9f6d88124bf27df54b7ec277a18738f08070941109ef64450cbb8a20bb4->enter($__internal_f55bb9f6d88124bf27df54b7ec277a18738f08070941109ef64450cbb8a20bb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <section>
        <div id=\"contenu\" class=\"article-list\">
        <div class=\"container\">
            <div class=\"intro\">
                <h1 class=\"text-center\">Statuts de l'association </h1>
                <p class=\"text-center\">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p>
            </div>
<div class=\"row articles\">
                <div class=\"col-md-12 col-sm-6 item\">
                    <a href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/imageaccueil1.jpg"), "html", null, true);
        echo "\"></a>
                    <h3 class=\"name\">Article Title</h3>
                    <p class=\"description\">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href=\"#\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a></div>
                <div
                class=\"col-md-4 col-sm-6 item\">
                    <a href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/imageaccueil2.jpg"), "html", null, true);
        echo "\"></a>
                    <h3 class=\"name\">Article Title</h3>
                    <p class=\"description\">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href=\"#\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a></div>
            <div
            class=\"col-md-4 col-sm-6 item\">
                <a href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/loft.jpg"), "html", null, true);
        echo "\"></a>
                <h3 class=\"name\">Article Title</h3>
                <p class=\"description\">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href=\"#\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a></div>
        <div
        class=\"col-md-4 col-sm-6 item\">
            <a href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/notes-de-musique-partition.jpg"), "html", null, true);
        echo "\"></a>
            <h3 class=\"name\">Article Title</h3>
            <p class=\"description\">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href=\"#\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a></div>
    <div
    class=\"col-md-4 col-sm-6 item\">
        <a href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/imageaccueil2.jpg"), "html", null, true);
        echo "\"></a>
        <h3 class=\"name\">Article Title</h3>
        <p class=\"description\">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href=\"#\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a></div>
        <div
        class=\"col-md-4 col-sm-6 item\">
            <a href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/loft.jpg"), "html", null, true);
        echo "\"></a>
            <h3 class=\"name\">Article Title</h3>
            <p class=\"description\">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href=\"#\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a></div>
            <div
            class=\"col-md-4 col-sm-6 item\">
                <a href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/photography_background.jpg"), "html", null, true);
        echo "\"></a>
                <h3 class=\"name\">Article Title</h3>
                <p class=\"description\">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href=\"#\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a></div>
                </div>
                </div>
                </div>
    </section>
";
        
        $__internal_f55bb9f6d88124bf27df54b7ec277a18738f08070941109ef64450cbb8a20bb4->leave($__internal_f55bb9f6d88124bf27df54b7ec277a18738f08070941109ef64450cbb8a20bb4_prof);

    }

    public function getTemplateName()
    {
        return "musikasvitrineBundle:Default:ressources.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 43,  91 => 38,  83 => 33,  75 => 28,  67 => 23,  59 => 18,  51 => 13,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMere.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <section>*/
/*         <div id="contenu" class="article-list">*/
/*         <div class="container">*/
/*             <div class="intro">*/
/*                 <h1 class="text-center">Statuts de l'association </h1>*/
/*                 <p class="text-center">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p>*/
/*             </div>*/
/* <div class="row articles">*/
/*                 <div class="col-md-12 col-sm-6 item">*/
/*                     <a href="#"><img class="img-responsive" src="{{ asset('bundles/assets/img/imageaccueil1.jpg') }}"></a>*/
/*                     <h3 class="name">Article Title</h3>*/
/*                     <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href="#" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a></div>*/
/*                 <div*/
/*                 class="col-md-4 col-sm-6 item">*/
/*                     <a href="#"><img class="img-responsive" src="{{ asset('bundles/assets/img/imageaccueil2.jpg') }}"></a>*/
/*                     <h3 class="name">Article Title</h3>*/
/*                     <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href="#" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a></div>*/
/*             <div*/
/*             class="col-md-4 col-sm-6 item">*/
/*                 <a href="#"><img class="img-responsive" src="{{ asset('bundles/assets/img/loft.jpg') }}"></a>*/
/*                 <h3 class="name">Article Title</h3>*/
/*                 <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href="#" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a></div>*/
/*         <div*/
/*         class="col-md-4 col-sm-6 item">*/
/*             <a href="#"><img class="img-responsive" src="{{ asset('bundles/assets/img/notes-de-musique-partition.jpg') }}"></a>*/
/*             <h3 class="name">Article Title</h3>*/
/*             <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href="#" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a></div>*/
/*     <div*/
/*     class="col-md-4 col-sm-6 item">*/
/*         <a href="#"><img class="img-responsive" src="{{ asset('bundles/assets/img/imageaccueil2.jpg') }}"></a>*/
/*         <h3 class="name">Article Title</h3>*/
/*         <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href="#" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a></div>*/
/*         <div*/
/*         class="col-md-4 col-sm-6 item">*/
/*             <a href="#"><img class="img-responsive" src="{{ asset('bundles/assets/img/loft.jpg') }}"></a>*/
/*             <h3 class="name">Article Title</h3>*/
/*             <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href="#" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a></div>*/
/*             <div*/
/*             class="col-md-4 col-sm-6 item">*/
/*                 <a href="#"><img class="img-responsive" src="{{ asset('bundles/assets/img/photography_background.jpg') }}"></a>*/
/*                 <h3 class="name">Article Title</h3>*/
/*                 <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href="#" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a></div>*/
/*                 </div>*/
/*                 </div>*/
/*                 </div>*/
/*     </section>*/
/* {% endblock %}*/
