<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_36961ac9c82c26c4a2602820b8c7dc696b9f19619b16f1e6290c6a0e0a89b820 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_71678bb7d8d11e129490f0ffbdc80cd0ee13d11997dd8ad027c5f0fd2f87b4db = $this->env->getExtension("native_profiler");
        $__internal_71678bb7d8d11e129490f0ffbdc80cd0ee13d11997dd8ad027c5f0fd2f87b4db->enter($__internal_71678bb7d8d11e129490f0ffbdc80cd0ee13d11997dd8ad027c5f0fd2f87b4db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_71678bb7d8d11e129490f0ffbdc80cd0ee13d11997dd8ad027c5f0fd2f87b4db->leave($__internal_71678bb7d8d11e129490f0ffbdc80cd0ee13d11997dd8ad027c5f0fd2f87b4db_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'url')) ?>*/
/* */
