<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_b25b78c1bc91957c51383c447919bfde2d59af5eae8adf45609acbdaeb103c90 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1500d21c546fc0d98eab489d990681ceb17fbf0af57480065cf8f690c3f5d5d4 = $this->env->getExtension("native_profiler");
        $__internal_1500d21c546fc0d98eab489d990681ceb17fbf0af57480065cf8f690c3f5d5d4->enter($__internal_1500d21c546fc0d98eab489d990681ceb17fbf0af57480065cf8f690c3f5d5d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1500d21c546fc0d98eab489d990681ceb17fbf0af57480065cf8f690c3f5d5d4->leave($__internal_1500d21c546fc0d98eab489d990681ceb17fbf0af57480065cf8f690c3f5d5d4_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f024c8940aedeca428fdfc7c4443f39f1a0e50967254b915ec8300ee65fa75b7 = $this->env->getExtension("native_profiler");
        $__internal_f024c8940aedeca428fdfc7c4443f39f1a0e50967254b915ec8300ee65fa75b7->enter($__internal_f024c8940aedeca428fdfc7c4443f39f1a0e50967254b915ec8300ee65fa75b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_f024c8940aedeca428fdfc7c4443f39f1a0e50967254b915ec8300ee65fa75b7->leave($__internal_f024c8940aedeca428fdfc7c4443f39f1a0e50967254b915ec8300ee65fa75b7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "@FOSUser/layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "@FOSUser/ChangePassword/change_password_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
