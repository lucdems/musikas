<?php

/* musikasvitrineBundle:Default:ecole.html.twig */
class __TwigTemplate_41f2dea7c6732ad3cfd217b3502ca9a4daf39450064871d69967897e673982cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMere.html.twig", "musikasvitrineBundle:Default:ecole.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMere.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5bce936a9150c2445c41efbce143658efeb0f9a70021e8e50f05d7c2139c26e4 = $this->env->getExtension("native_profiler");
        $__internal_5bce936a9150c2445c41efbce143658efeb0f9a70021e8e50f05d7c2139c26e4->enter($__internal_5bce936a9150c2445c41efbce143658efeb0f9a70021e8e50f05d7c2139c26e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasvitrineBundle:Default:ecole.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5bce936a9150c2445c41efbce143658efeb0f9a70021e8e50f05d7c2139c26e4->leave($__internal_5bce936a9150c2445c41efbce143658efeb0f9a70021e8e50f05d7c2139c26e4_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_e38699d36911a1eec0513a651dda56a29dbdd594c377e543d8a843c217102847 = $this->env->getExtension("native_profiler");
        $__internal_e38699d36911a1eec0513a651dda56a29dbdd594c377e543d8a843c217102847->enter($__internal_e38699d36911a1eec0513a651dda56a29dbdd594c377e543d8a843c217102847_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "

<script src=\"https://use.fontawesome.com/c276733435.js\"></script>


    <section>
        <div class=\"article-list\">
        <div class=\"container\" id=\"contenuenseignement\">
            <div class=\"intro\">
                <h1 class=\"text-center\">";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ecole"]) ? $context["ecole"] : $this->getContext($context, "ecole")), "nom", array()), "html", null, true);
        echo "</h1>
                <p class=\"text-justify\">";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ecole"]) ? $context["ecole"] : $this->getContext($context, "ecole")), "description", array()), "html", null, true);
        echo " </p>
                <img class=\"img-responsive\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["ecole"]) ? $context["ecole"] : $this->getContext($context, "ecole")), "image", array())), "html", null, true);
        echo "\" style=\"margin-bottom:20px\">
                

            </div>
            
            <div class=\"container\" id=\"contact_ecole\" style=\"width:300px\">
            <div class=\"row\">
                <div class=\"col-md-12\">

                    <ul class=\"list-unstyled fa-ul\">
                        <li><i class=\"fa fa-map-marker fa-li\"></i>";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ecole"]) ? $context["ecole"] : $this->getContext($context, "ecole")), "adresse", array()), "html", null, true);
        echo "</li>
                        <li><i class=\"fa fa-envelope fa-li\"></i>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ecole"]) ? $context["ecole"] : $this->getContext($context, "ecole")), "mail", array()), "html", null, true);
        echo "</li>
                        <li><i class=\"fa fa-phone fa-li\"></i>";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["ecole"]) ? $context["ecole"] : $this->getContext($context, "ecole")), "telephone", array()), "html", null, true);
        echo "</li>
                    </ul>
                </div>
            </div>
            </div>
            
            
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <a class=\"btn btn-default\" type=\"button\" id=\"btn_retour\" href=\"";
        // line 36
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_ecoles");
        echo "\">Retour aux écoles</a>
                </div>
            </div>
            
            <div class=\"container\" id=\"contenuenseignement\">
                <h1 class=\"text-center\"> ";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nos enseignements"), "html", null, true);
        echo " : </h1>
            </div>
            <div class=\"row articles\" id=\"imagesenseignements\">
            
            ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tabEnseignement"]) ? $context["tabEnseignement"] : $this->getContext($context, "tabEnseignement")));
        foreach ($context['_seq'] as $context["_key"] => $context["enseignement"]) {
            // line 46
            echo "                
                <div class=\"col-md-4 col-sm-6 item\">
                    <a href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("musikasvitrine_enseignement", array("id" => $this->getAttribute($context["enseignement"], "id", array()))), "html", null, true);
            echo "\"><img class=\"img-responsive\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["enseignement"], "image", array())), "html", null, true);
            echo "\"></a>
                    <h3 class=\"name\">";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["enseignement"], "nom", array()), "html", null, true);
            echo "</h3>
                    <p class=\"text-justify description\"> ";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["enseignement"], "description", array()), "html", null, true);
            echo "</p><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("musikasvitrine_enseignement", array("id" => $this->getAttribute($context["enseignement"], "id", array()))), "html", null, true);
            echo "\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a>
                </div>
                
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enseignement'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "            
        </div>
        </div>
    </section>
";
        
        $__internal_e38699d36911a1eec0513a651dda56a29dbdd594c377e543d8a843c217102847->leave($__internal_e38699d36911a1eec0513a651dda56a29dbdd594c377e543d8a843c217102847_prof);

    }

    public function getTemplateName()
    {
        return "musikasvitrineBundle:Default:ecole.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 54,  125 => 50,  121 => 49,  115 => 48,  111 => 46,  107 => 45,  100 => 41,  92 => 36,  80 => 27,  76 => 26,  72 => 25,  59 => 15,  55 => 14,  51 => 13,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMere.html.twig" %}*/
/* */
/* {% block contenu %}*/
/* */
/* */
/* <script src="https://use.fontawesome.com/c276733435.js"></script>*/
/* */
/* */
/*     <section>*/
/*         <div class="article-list">*/
/*         <div class="container" id="contenuenseignement">*/
/*             <div class="intro">*/
/*                 <h1 class="text-center">{{ ecole.nom }}</h1>*/
/*                 <p class="text-justify">{{ ecole.description }} </p>*/
/*                 <img class="img-responsive" src="{{ asset(ecole.image) }}" style="margin-bottom:20px">*/
/*                 */
/* */
/*             </div>*/
/*             */
/*             <div class="container" id="contact_ecole" style="width:300px">*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/* */
/*                     <ul class="list-unstyled fa-ul">*/
/*                         <li><i class="fa fa-map-marker fa-li"></i>{{ ecole.adresse }}</li>*/
/*                         <li><i class="fa fa-envelope fa-li"></i>{{ ecole.mail }}</li>*/
/*                         <li><i class="fa fa-phone fa-li"></i>{{ ecole.telephone }}</li>*/
/*                     </ul>*/
/*                 </div>*/
/*             </div>*/
/*             </div>*/
/*             */
/*             */
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <a class="btn btn-default" type="button" id="btn_retour" href="{{ path('musikasvitrine_ecoles') }}">Retour aux écoles</a>*/
/*                 </div>*/
/*             </div>*/
/*             */
/*             <div class="container" id="contenuenseignement">*/
/*                 <h1 class="text-center"> {{ 'Nos enseignements'| trans }} : </h1>*/
/*             </div>*/
/*             <div class="row articles" id="imagesenseignements">*/
/*             */
/*             {% for enseignement in tabEnseignement %}*/
/*                 */
/*                 <div class="col-md-4 col-sm-6 item">*/
/*                     <a href="{{ path('musikasvitrine_enseignement', { 'id': enseignement.id }) }}"><img class="img-responsive" src="{{ asset(enseignement.image)}}"></a>*/
/*                     <h3 class="name">{{ enseignement.nom }}</h3>*/
/*                     <p class="text-justify description"> {{ enseignement.description }}</p><a href="{{ path('musikasvitrine_enseignement', { 'id': enseignement.id }) }}" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a>*/
/*                 </div>*/
/*                 */
/*             {% endfor %}*/
/*             */
/*         </div>*/
/*         </div>*/
/*     </section>*/
/* {% endblock %}*/
