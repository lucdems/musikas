<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_1beaf7fe8da3c5e21e2c7334011201ba3c9b7ae0191dab3de92756723fd7778a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7fcf28f81a43b82c8c6b91640cf1e77e34d397b3a376d54814e2bb78fc73713b = $this->env->getExtension("native_profiler");
        $__internal_7fcf28f81a43b82c8c6b91640cf1e77e34d397b3a376d54814e2bb78fc73713b->enter($__internal_7fcf28f81a43b82c8c6b91640cf1e77e34d397b3a376d54814e2bb78fc73713b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_7fcf28f81a43b82c8c6b91640cf1e77e34d397b3a376d54814e2bb78fc73713b->leave($__internal_7fcf28f81a43b82c8c6b91640cf1e77e34d397b3a376d54814e2bb78fc73713b_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_bc370bb5d10be861620fe37efec14aa167b102c0ae105589920dff40aab90361 = $this->env->getExtension("native_profiler");
        $__internal_bc370bb5d10be861620fe37efec14aa167b102c0ae105589920dff40aab90361->enter($__internal_bc370bb5d10be861620fe37efec14aa167b102c0ae105589920dff40aab90361_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_bc370bb5d10be861620fe37efec14aa167b102c0ae105589920dff40aab90361->leave($__internal_bc370bb5d10be861620fe37efec14aa167b102c0ae105589920dff40aab90361_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
