<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_a87c18c9476a918bc4dad57e8d94ba311dbac48ca4c095d9de906fe416fe2ff1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2e7166a8d179134540e9890733bb0930d128b0a06365df0c6b61711b6b9df0dc = $this->env->getExtension("native_profiler");
        $__internal_2e7166a8d179134540e9890733bb0930d128b0a06365df0c6b61711b6b9df0dc->enter($__internal_2e7166a8d179134540e9890733bb0930d128b0a06365df0c6b61711b6b9df0dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_2e7166a8d179134540e9890733bb0930d128b0a06365df0c6b61711b6b9df0dc->leave($__internal_2e7166a8d179134540e9890733bb0930d128b0a06365df0c6b61711b6b9df0dc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
