<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_849489a69a3f55f8b887b229db5928285ad6eece12082cefe67cbd99d803a2a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fdcea55cdcdce93ab3b03c515cabda9ffe0a05b9816a1de50c4ec7334828198d = $this->env->getExtension("native_profiler");
        $__internal_fdcea55cdcdce93ab3b03c515cabda9ffe0a05b9816a1de50c4ec7334828198d->enter($__internal_fdcea55cdcdce93ab3b03c515cabda9ffe0a05b9816a1de50c4ec7334828198d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_fdcea55cdcdce93ab3b03c515cabda9ffe0a05b9816a1de50c4ec7334828198d->leave($__internal_fdcea55cdcdce93ab3b03c515cabda9ffe0a05b9816a1de50c4ec7334828198d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'number')) ?>*/
/* */
