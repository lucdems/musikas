<?php

/* musikasvitrineBundle:Default:enseignements.html.twig */
class __TwigTemplate_126dcb37cde375c012b760e6d4eded871b2570257e3da84ffc3fd0163409d522 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMere.html.twig", "musikasvitrineBundle:Default:enseignements.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMere.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c03816bb737a77736a90f141ee734b17e0d550d4c8cdb0835ccc1ff5f25008de = $this->env->getExtension("native_profiler");
        $__internal_c03816bb737a77736a90f141ee734b17e0d550d4c8cdb0835ccc1ff5f25008de->enter($__internal_c03816bb737a77736a90f141ee734b17e0d550d4c8cdb0835ccc1ff5f25008de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasvitrineBundle:Default:enseignements.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c03816bb737a77736a90f141ee734b17e0d550d4c8cdb0835ccc1ff5f25008de->leave($__internal_c03816bb737a77736a90f141ee734b17e0d550d4c8cdb0835ccc1ff5f25008de_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_06ccd14e2ec6f45f0dd44c3e998c6a38b7fe4e222a2a1ac99f65329217b6b34f = $this->env->getExtension("native_profiler");
        $__internal_06ccd14e2ec6f45f0dd44c3e998c6a38b7fe4e222a2a1ac99f65329217b6b34f->enter($__internal_06ccd14e2ec6f45f0dd44c3e998c6a38b7fe4e222a2a1ac99f65329217b6b34f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <div class=\"article-list\">
        <div class=\"container\" id=\"contenuenseignement\">
            <div class=\"intro\">
                <h1 class=\"text-center\">";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nos cursus"), "html", null, true);
        echo "</h1>
                <p class=\"text-justify\">Tous les habitants de la communauté de commune d'ERROBI peuvent bénéficier d'une réduction de 20%. Les familles bénéficient de 20€ de remise par inscription à partir du second en enfant (ex: premier 180€, deuxième 160€, troisième </p>
            </div>
            
            <div class=\"row articles\" id=\"imagesenseignements\">
                
                
                
        <div class=\"row\">
            <div class=\"col-md-12\">
                <section class=\"pricing-table\">
                    <div class=\"container\">
                        <!--<div class=\"center\">
                            <h2>Nos cursus</h2>
                            <p class=\"lead\">Tous les habitants de la communauté de commune d'ERROBI peuvent bénéficier d'une réduction de 20%. Les familles bénéficient de 20€ de remise par inscription à partir du second en enfant (ex: premier 180€, deuxième 160€, troisième
                                140€).</p>
                        </div>-->
                        <div class=\"pricing-area text-center\">
                            <div class=\"row\">
                                <div class=\"col-sm-4 plan price red wow fadeInDown\">
                                    <ul class=\"list-group\">
                                        <li class=\"list-group-item heading\">
                                            <h1>";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Culture musicale"), "html", null, true);
        echo "</h1><span class=\"price\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("A partir de"), "html", null, true);
        echo " 120€</span></li>
                                        <li class=\"list-group-item\"><span>";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Eveil"), "html", null, true);
        echo " </span></li>
                                        <li class=\"list-group-item\"><span>";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Initiation"), "html", null, true);
        echo " (CP)</span></li>
                                        <li class=\"list-group-item\"><span>";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Formation Musicale"), "html", null, true);
        echo " (CE1+)</span></li>
                                        <li class=\"list-group-item\"><span>";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Culture musicale"), "html", null, true);
        echo "</span></li>
                                        <li class=\"list-group-item plan-action\"><a href=\"#\" class=\"btn\">";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Details"), "html", null, true);
        echo " </a></li>
                                    </ul>
                                </div>
                                <div class=\"col-sm-4 plan price green wow fadeInDown\">
                                    <ul class=\"list-group\">
                                        <li class=\"list-group-item heading\">
                                            <h1>";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pratique collective"), "html", null, true);
        echo "</h1><span class=\"price\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("A partir de"), "html", null, true);
        echo " 100€</span></li>
                                        <li class=\"list-group-item\"><span>";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Harmonie"), "html", null, true);
        echo " </span></li>
                                        <li class=\"list-group-item\"><span>";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Groupes Musiques Amplifiées"), "html", null, true);
        echo "</span></li>
                                        <li class=\"list-group-item\"><span>";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Musique traditionnelles"), "html", null, true);
        echo "</span></li>
                                        <li class=\"list-group-item\"><span>";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Chant choral (enfant ou adulte)"), "html", null, true);
        echo "</span></li>
                                        <li class=\"list-group-item plan-action\"><a href=\"#\" class=\"btn\">";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Details"), "html", null, true);
        echo " </a></li>
                                    </ul>
                                </div>
                                <div class=\"col-sm-4 plan price yellow wow fadeInDown\"><img class=\"hidden\" src=\"assets/img/ribon_one.png\">
                                    <ul class=\"list-group\">
                                        <li class=\"list-group-item heading\">
                                            <h1>Cursus instrumental</h1><span class=\"price\">";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("A partir de"), "html", null, true);
        echo " 160€</span></li>
                                        <li class=\"list-group-item\"><span>";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Instrument"), "html", null, true);
        echo " </span></li>
                                        <li class=\"list-group-item\"><span>";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pratique collective"), "html", null, true);
        echo "</span></li>
                                        <li class=\"list-group-item\"><span>";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Formation Musicale"), "html", null, true);
        echo "</span></li>
                                        <li class=\"list-group-item\"><span>";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Instrument"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("supplémentaire"), "html", null, true);
        echo "</span></li>
                                        <li class=\"list-group-item plan-action\"><a href=\"#\" class=\"btn\">";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Details"), "html", null, true);
        echo " </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class=\"intro\">
                <h1 class=\"text-center\">";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nos enseignements"), "html", null, true);
        echo "</h1>
                <p class=\"text-justify\">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p>
            </div>
        
        ";
        // line 70
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tabEnseignement"]) ? $context["tabEnseignement"] : $this->getContext($context, "tabEnseignement")));
        foreach ($context['_seq'] as $context["_key"] => $context["enseignement"]) {
            // line 71
            echo "                
                <div class=\"col-md-4 col-sm-6 item\">
                    <a href=\"";
            // line 73
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("musikasvitrine_enseignement", array("id" => $this->getAttribute($context["enseignement"], "id", array()))), "html", null, true);
            echo "\"><img class=\"img-responsive\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["enseignement"], "image", array())), "html", null, true);
            echo "\"></a>
                    <h3 class=\"name\">";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute($context["enseignement"], "nom", array()), "html", null, true);
            echo "</h3>
                    <p class=\"text-justify description\"> ";
            // line 75
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["enseignement"], "description", array()), 0, 150), "html", null, true);
            echo "...</p><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("musikasvitrine_enseignement", array("id" => $this->getAttribute($context["enseignement"], "id", array()))), "html", null, true);
            echo "\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a>
                </div>
                
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enseignement'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "        
            </div>
        </div>
    </div>
";
        
        $__internal_06ccd14e2ec6f45f0dd44c3e998c6a38b7fe4e222a2a1ac99f65329217b6b34f->leave($__internal_06ccd14e2ec6f45f0dd44c3e998c6a38b7fe4e222a2a1ac99f65329217b6b34f_prof);

    }

    public function getTemplateName()
    {
        return "musikasvitrineBundle:Default:enseignements.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 79,  192 => 75,  188 => 74,  182 => 73,  178 => 71,  174 => 70,  167 => 66,  154 => 56,  148 => 55,  144 => 54,  140 => 53,  136 => 52,  132 => 51,  123 => 45,  119 => 44,  115 => 43,  111 => 42,  107 => 41,  101 => 40,  92 => 34,  88 => 33,  84 => 32,  80 => 31,  76 => 30,  70 => 29,  45 => 7,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMere.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <div class="article-list">*/
/*         <div class="container" id="contenuenseignement">*/
/*             <div class="intro">*/
/*                 <h1 class="text-center">{{ 'Nos cursus' |trans }}</h1>*/
/*                 <p class="text-justify">Tous les habitants de la communauté de commune d'ERROBI peuvent bénéficier d'une réduction de 20%. Les familles bénéficient de 20€ de remise par inscription à partir du second en enfant (ex: premier 180€, deuxième 160€, troisième </p>*/
/*             </div>*/
/*             */
/*             <div class="row articles" id="imagesenseignements">*/
/*                 */
/*                 */
/*                 */
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <section class="pricing-table">*/
/*                     <div class="container">*/
/*                         <!--<div class="center">*/
/*                             <h2>Nos cursus</h2>*/
/*                             <p class="lead">Tous les habitants de la communauté de commune d'ERROBI peuvent bénéficier d'une réduction de 20%. Les familles bénéficient de 20€ de remise par inscription à partir du second en enfant (ex: premier 180€, deuxième 160€, troisième*/
/*                                 140€).</p>*/
/*                         </div>-->*/
/*                         <div class="pricing-area text-center">*/
/*                             <div class="row">*/
/*                                 <div class="col-sm-4 plan price red wow fadeInDown">*/
/*                                     <ul class="list-group">*/
/*                                         <li class="list-group-item heading">*/
/*                                             <h1>{{ 'Culture musicale' | trans }}</h1><span class="price">{{ 'A partir de' |trans }} 120€</span></li>*/
/*                                         <li class="list-group-item"><span>{{ 'Eveil'| trans }} </span></li>*/
/*                                         <li class="list-group-item"><span>{{ 'Initiation' | trans }} (CP)</span></li>*/
/*                                         <li class="list-group-item"><span>{{ 'Formation Musicale' |trans}} (CE1+)</span></li>*/
/*                                         <li class="list-group-item"><span>{{ 'Culture musicale' | trans }}</span></li>*/
/*                                         <li class="list-group-item plan-action"><a href="#" class="btn">{{ 'Details' | trans}} </a></li>*/
/*                                     </ul>*/
/*                                 </div>*/
/*                                 <div class="col-sm-4 plan price green wow fadeInDown">*/
/*                                     <ul class="list-group">*/
/*                                         <li class="list-group-item heading">*/
/*                                             <h1>{{ 'Pratique collective' | trans }}</h1><span class="price">{{ 'A partir de' |trans }} 100€</span></li>*/
/*                                         <li class="list-group-item"><span>{{ 'Harmonie' | trans }} </span></li>*/
/*                                         <li class="list-group-item"><span>{{ 'Groupes Musiques Amplifiées' | trans }}</span></li>*/
/*                                         <li class="list-group-item"><span>{{ 'Musique traditionnelles' | trans }}</span></li>*/
/*                                         <li class="list-group-item"><span>{{ 'Chant choral (enfant ou adulte)' | trans }}</span></li>*/
/*                                         <li class="list-group-item plan-action"><a href="#" class="btn">{{ 'Details' | trans}} </a></li>*/
/*                                     </ul>*/
/*                                 </div>*/
/*                                 <div class="col-sm-4 plan price yellow wow fadeInDown"><img class="hidden" src="assets/img/ribon_one.png">*/
/*                                     <ul class="list-group">*/
/*                                         <li class="list-group-item heading">*/
/*                                             <h1>Cursus instrumental</h1><span class="price">{{ 'A partir de' |trans }} 160€</span></li>*/
/*                                         <li class="list-group-item"><span>{{ 'Instrument' | trans }} </span></li>*/
/*                                         <li class="list-group-item"><span>{{ 'Pratique collective' | trans }}</span></li>*/
/*                                         <li class="list-group-item"><span>{{ 'Formation Musicale' |trans}}</span></li>*/
/*                                         <li class="list-group-item"><span>{{ 'Instrument' | trans }} {{ 'supplémentaire' | trans }}</span></li>*/
/*                                         <li class="list-group-item plan-action"><a href="#" class="btn">{{ 'Details' | trans}} </a></li>*/
/*                                     </ul>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </section>*/
/*             </div>*/
/*         </div>*/
/*         <div class="intro">*/
/*                 <h1 class="text-center">{{ 'Nos enseignements' | trans }}</h1>*/
/*                 <p class="text-justify">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p>*/
/*             </div>*/
/*         */
/*         {% for enseignement in tabEnseignement %}*/
/*                 */
/*                 <div class="col-md-4 col-sm-6 item">*/
/*                     <a href="{{ path('musikasvitrine_enseignement', { 'id': enseignement.id }) }}"><img class="img-responsive" src="{{ asset(enseignement.image)}}"></a>*/
/*                     <h3 class="name">{{ enseignement.nom }}</h3>*/
/*                     <p class="text-justify description"> {{ enseignement.description [:150]}}...</p><a href="{{ path('musikasvitrine_enseignement', { 'id': enseignement.id }) }}" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a>*/
/*                 </div>*/
/*                 */
/*         {% endfor %}*/
/*         */
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
