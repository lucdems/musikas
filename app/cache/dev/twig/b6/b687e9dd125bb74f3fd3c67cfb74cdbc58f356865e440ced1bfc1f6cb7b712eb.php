<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_1734652444fa4fabaf840adbbb6cd00ab8351b094bf7106a5d4757391dcc0bee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4203c2b2d502585629112f2c1f61045533eba6f025bb101df446225aa147615f = $this->env->getExtension("native_profiler");
        $__internal_4203c2b2d502585629112f2c1f61045533eba6f025bb101df446225aa147615f->enter($__internal_4203c2b2d502585629112f2c1f61045533eba6f025bb101df446225aa147615f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4203c2b2d502585629112f2c1f61045533eba6f025bb101df446225aa147615f->leave($__internal_4203c2b2d502585629112f2c1f61045533eba6f025bb101df446225aa147615f_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_8e27dfffc0bedac3c6e2ea3c8c20cacd2441524e880060d423d8c01b8b6a4b4b = $this->env->getExtension("native_profiler");
        $__internal_8e27dfffc0bedac3c6e2ea3c8c20cacd2441524e880060d423d8c01b8b6a4b4b->enter($__internal_8e27dfffc0bedac3c6e2ea3c8c20cacd2441524e880060d423d8c01b8b6a4b4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_8e27dfffc0bedac3c6e2ea3c8c20cacd2441524e880060d423d8c01b8b6a4b4b->leave($__internal_8e27dfffc0bedac3c6e2ea3c8c20cacd2441524e880060d423d8c01b8b6a4b4b_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_34042ff93794088f99b0e037e974326466b47db3f170cd11dcfa586c447d2e39 = $this->env->getExtension("native_profiler");
        $__internal_34042ff93794088f99b0e037e974326466b47db3f170cd11dcfa586c447d2e39->enter($__internal_34042ff93794088f99b0e037e974326466b47db3f170cd11dcfa586c447d2e39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_34042ff93794088f99b0e037e974326466b47db3f170cd11dcfa586c447d2e39->leave($__internal_34042ff93794088f99b0e037e974326466b47db3f170cd11dcfa586c447d2e39_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_b5630f58326261bd3e3030d0dbf001482cc4740bc183c8d2677c5367fd3ffbb3 = $this->env->getExtension("native_profiler");
        $__internal_b5630f58326261bd3e3030d0dbf001482cc4740bc183c8d2677c5367fd3ffbb3->enter($__internal_b5630f58326261bd3e3030d0dbf001482cc4740bc183c8d2677c5367fd3ffbb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_b5630f58326261bd3e3030d0dbf001482cc4740bc183c8d2677c5367fd3ffbb3->leave($__internal_b5630f58326261bd3e3030d0dbf001482cc4740bc183c8d2677c5367fd3ffbb3_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
