<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_e30c1d0b58c9dfb1c4b7d9867014d4abfb831e72b7855a9fe256a83f2072b73b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ee30d2da2fa1a73671b69fee9673342b8d048e4d9374d8d3ce1abd39e0650343 = $this->env->getExtension("native_profiler");
        $__internal_ee30d2da2fa1a73671b69fee9673342b8d048e4d9374d8d3ce1abd39e0650343->enter($__internal_ee30d2da2fa1a73671b69fee9673342b8d048e4d9374d8d3ce1abd39e0650343_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_ee30d2da2fa1a73671b69fee9673342b8d048e4d9374d8d3ce1abd39e0650343->leave($__internal_ee30d2da2fa1a73671b69fee9673342b8d048e4d9374d8d3ce1abd39e0650343_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
