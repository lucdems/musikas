<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_504ba6d2a7cd7fecd6edc6c4c1aed72d90977cb6edb62769ace245dc09f692fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aeedad032dc8ee18697a4c4617169ab9539ced43882b58b9bd6e1f45aa425957 = $this->env->getExtension("native_profiler");
        $__internal_aeedad032dc8ee18697a4c4617169ab9539ced43882b58b9bd6e1f45aa425957->enter($__internal_aeedad032dc8ee18697a4c4617169ab9539ced43882b58b9bd6e1f45aa425957_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_aeedad032dc8ee18697a4c4617169ab9539ced43882b58b9bd6e1f45aa425957->leave($__internal_aeedad032dc8ee18697a4c4617169ab9539ced43882b58b9bd6e1f45aa425957_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($form->vars['multipart']): ?>enctype="multipart/form-data"<?php endif ?>*/
/* */
