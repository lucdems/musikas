<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_6f73d8f8792d18cd992aa62774a679cb81fc37ec0e41f06861f28bd1cf1fc3f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_043b275537f5570fb68eb674b2619b0c73b517060960571ba36611b62924a1d5 = $this->env->getExtension("native_profiler");
        $__internal_043b275537f5570fb68eb674b2619b0c73b517060960571ba36611b62924a1d5->enter($__internal_043b275537f5570fb68eb674b2619b0c73b517060960571ba36611b62924a1d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_043b275537f5570fb68eb674b2619b0c73b517060960571ba36611b62924a1d5->leave($__internal_043b275537f5570fb68eb674b2619b0c73b517060960571ba36611b62924a1d5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (isset($prototype)): ?>*/
/*     <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>*/
/* <?php endif ?>*/
/* <?php echo $view['form']->widget($form, array('attr' => $attr)) ?>*/
/* */
