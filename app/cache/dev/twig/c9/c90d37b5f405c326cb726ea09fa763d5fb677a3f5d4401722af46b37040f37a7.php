<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_c072e17fc34de6bf6c6626231232017c5b8dc30fc4096983766fe4c95b25ea9e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMere.html.twig", "@FOSUser/layout.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMere.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_da50727cafed5cbe9ecc8e5560ac5c279d8a42d47e6263cf2ba49ab56cf0645b = $this->env->getExtension("native_profiler");
        $__internal_da50727cafed5cbe9ecc8e5560ac5c279d8a42d47e6263cf2ba49ab56cf0645b->enter($__internal_da50727cafed5cbe9ecc8e5560ac5c279d8a42d47e6263cf2ba49ab56cf0645b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_da50727cafed5cbe9ecc8e5560ac5c279d8a42d47e6263cf2ba49ab56cf0645b->leave($__internal_da50727cafed5cbe9ecc8e5560ac5c279d8a42d47e6263cf2ba49ab56cf0645b_prof);

    }

    // line 4
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_b941d7d087a8ac304b5e2dc7db6ecd2bbd6182adb5899870c62a5d688232ce2a = $this->env->getExtension("native_profiler");
        $__internal_b941d7d087a8ac304b5e2dc7db6ecd2bbd6182adb5899870c62a5d688232ce2a->enter($__internal_b941d7d087a8ac304b5e2dc7db6ecd2bbd6182adb5899870c62a5d688232ce2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 5
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 6
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 8
        echo "    <div id=\"connexion\" class=\"login-dark\">
        <div class=\"container\">
            <div id=\"zoneconnexion\">
                <form method=\"post\" action=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\">
                        ";
        // line 12
        if ((isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token"))) {
            // line 13
            echo "                            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
            echo "\" />
                        ";
        }
        // line 15
        echo "                    <h2 class=\"sr-only\">Login Form</h2>
                    <div class=\"illustration\"><i class=\"icon ion-ios-locked-outline\"></i></div>
                    <div class=\"form-group\">
                        <input class=\"form-control\" type=\"text\" name=\"_username\" placeholder=\"Identifiant\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\">
                    </div>
                    <div class=\"form-group\">
                        <input class=\"form-control\" type=\"password\" name=\"_password\" placeholder=\"Mot de passe\" required=\"required\">
                    </div>
                    <div class=\"form-group\">
                        <button class=\"btn btn-primary btn-block\" type=\"submit\" id=\"_submit\">Connexion</button>
                    </div><a href=\"#\" class=\"forgot\">Mot de passe oublié ?</a></form>
            </div>
        </div>
    </div>
    
";
        
        $__internal_b941d7d087a8ac304b5e2dc7db6ecd2bbd6182adb5899870c62a5d688232ce2a->leave($__internal_b941d7d087a8ac304b5e2dc7db6ecd2bbd6182adb5899870c62a5d688232ce2a_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 18,  65 => 15,  59 => 13,  57 => 12,  53 => 11,  48 => 8,  42 => 6,  40 => 5,  34 => 4,  11 => 1,);
    }
}
/* {% extends "::default/vueMere.html.twig" %}*/
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block contenu %}*/
/* {% if error %}*/
/*     <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>*/
/* {% endif %}*/
/*     <div id="connexion" class="login-dark">*/
/*         <div class="container">*/
/*             <div id="zoneconnexion">*/
/*                 <form method="post" action="{{ path("fos_user_security_check") }}">*/
/*                         {% if csrf_token %}*/
/*                             <input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />*/
/*                         {% endif %}*/
/*                     <h2 class="sr-only">Login Form</h2>*/
/*                     <div class="illustration"><i class="icon ion-ios-locked-outline"></i></div>*/
/*                     <div class="form-group">*/
/*                         <input class="form-control" type="text" name="_username" placeholder="Identifiant" value="{{ last_username }}" required="required">*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <input class="form-control" type="password" name="_password" placeholder="Mot de passe" required="required">*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <button class="btn btn-primary btn-block" type="submit" id="_submit">Connexion</button>*/
/*                     </div><a href="#" class="forgot">Mot de passe oublié ?</a></form>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     */
/* {% endblock %}*/
