<?php

/* @EasyAdmin/default/field_id.html.twig */
class __TwigTemplate_2602fa249f46ea056a9d8f6fe6a6653322a502933a9dc468f664f0b6111ff63a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b0ef4982f56b1c8f7863b42fec844f74c1dd0d094e1691a90407d163993f3e1 = $this->env->getExtension("native_profiler");
        $__internal_0b0ef4982f56b1c8f7863b42fec844f74c1dd0d094e1691a90407d163993f3e1->enter($__internal_0b0ef4982f56b1c8f7863b42fec844f74c1dd0d094e1691a90407d163993f3e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/field_id.html.twig"));

        // line 2
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
        echo "
";
        
        $__internal_0b0ef4982f56b1c8f7863b42fec844f74c1dd0d094e1691a90407d163993f3e1->leave($__internal_0b0ef4982f56b1c8f7863b42fec844f74c1dd0d094e1691a90407d163993f3e1_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/field_id.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,);
    }
}
/* {# this field template is used to avoid formatting the special ID attribute as a number #}*/
/* {{ value }}*/
/* */
