<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_06a6480ddf04666e8333df474b53c0abcd6638e431eb1a5eb15a6a5a07cc99bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_43f0393023a8807018ee74208f402e5e602ad5b7604424273f86c10a0eb0522d = $this->env->getExtension("native_profiler");
        $__internal_43f0393023a8807018ee74208f402e5e602ad5b7604424273f86c10a0eb0522d->enter($__internal_43f0393023a8807018ee74208f402e5e602ad5b7604424273f86c10a0eb0522d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_43f0393023a8807018ee74208f402e5e602ad5b7604424273f86c10a0eb0522d->leave($__internal_43f0393023a8807018ee74208f402e5e602ad5b7604424273f86c10a0eb0522d_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_b77c7507df2c4108845852e6fa4dc437df4b95ea057c7958d7fd884a65eb9c01 = $this->env->getExtension("native_profiler");
        $__internal_b77c7507df2c4108845852e6fa4dc437df4b95ea057c7958d7fd884a65eb9c01->enter($__internal_b77c7507df2c4108845852e6fa4dc437df4b95ea057c7958d7fd884a65eb9c01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_b77c7507df2c4108845852e6fa4dc437df4b95ea057c7958d7fd884a65eb9c01->leave($__internal_b77c7507df2c4108845852e6fa4dc437df4b95ea057c7958d7fd884a65eb9c01_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_48df3eb4f431a3ee1bd5d1481d529cff35c24f7325280a8402948f43564bcd0b = $this->env->getExtension("native_profiler");
        $__internal_48df3eb4f431a3ee1bd5d1481d529cff35c24f7325280a8402948f43564bcd0b->enter($__internal_48df3eb4f431a3ee1bd5d1481d529cff35c24f7325280a8402948f43564bcd0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_48df3eb4f431a3ee1bd5d1481d529cff35c24f7325280a8402948f43564bcd0b->leave($__internal_48df3eb4f431a3ee1bd5d1481d529cff35c24f7325280a8402948f43564bcd0b_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_c5995528e4eec8da46dc59afa4932b73e3b56e0c5d49b2afc34692591998ab99 = $this->env->getExtension("native_profiler");
        $__internal_c5995528e4eec8da46dc59afa4932b73e3b56e0c5d49b2afc34692591998ab99->enter($__internal_c5995528e4eec8da46dc59afa4932b73e3b56e0c5d49b2afc34692591998ab99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_c5995528e4eec8da46dc59afa4932b73e3b56e0c5d49b2afc34692591998ab99->leave($__internal_c5995528e4eec8da46dc59afa4932b73e3b56e0c5d49b2afc34692591998ab99_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
