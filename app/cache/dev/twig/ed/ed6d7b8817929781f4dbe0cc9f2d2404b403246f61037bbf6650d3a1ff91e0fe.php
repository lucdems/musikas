<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_ceb2ec1bcfcb7c97535701cd5e825cf0206385221bf478d1a252c45d4ec3d4fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_deeb1cd6f8a41bb918ca8ee18df5a6965583934ab42774fc3a5e54fd65ab8ecb = $this->env->getExtension("native_profiler");
        $__internal_deeb1cd6f8a41bb918ca8ee18df5a6965583934ab42774fc3a5e54fd65ab8ecb->enter($__internal_deeb1cd6f8a41bb918ca8ee18df5a6965583934ab42774fc3a5e54fd65ab8ecb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_deeb1cd6f8a41bb918ca8ee18df5a6965583934ab42774fc3a5e54fd65ab8ecb->leave($__internal_deeb1cd6f8a41bb918ca8ee18df5a6965583934ab42774fc3a5e54fd65ab8ecb_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
