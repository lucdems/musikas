<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_1e547fdf75bfa38a7f16b3e0f23ae2b820e69e7d8853f1ae2252225135b8e937 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_691fc81fc1dd8232e19ebf99676f371e128d5c8fbee0aeabfb68821b7d64552a = $this->env->getExtension("native_profiler");
        $__internal_691fc81fc1dd8232e19ebf99676f371e128d5c8fbee0aeabfb68821b7d64552a->enter($__internal_691fc81fc1dd8232e19ebf99676f371e128d5c8fbee0aeabfb68821b7d64552a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_691fc81fc1dd8232e19ebf99676f371e128d5c8fbee0aeabfb68821b7d64552a->leave($__internal_691fc81fc1dd8232e19ebf99676f371e128d5c8fbee0aeabfb68821b7d64552a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child) : ?>*/
/*     <?php echo $view['form']->row($child) ?>*/
/* <?php endforeach; ?>*/
/* */
