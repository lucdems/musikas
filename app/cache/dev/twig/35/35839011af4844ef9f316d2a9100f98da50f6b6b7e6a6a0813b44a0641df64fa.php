<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_6b90098eb2bfc621aa1ba4ff1bfa95c25eb678b15eec6c21bd2db44a193e90cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_acba08861275e1bff5ac979f446ea4546242b499471030e5adbba4240af27ce9 = $this->env->getExtension("native_profiler");
        $__internal_acba08861275e1bff5ac979f446ea4546242b499471030e5adbba4240af27ce9->enter($__internal_acba08861275e1bff5ac979f446ea4546242b499471030e5adbba4240af27ce9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_acba08861275e1bff5ac979f446ea4546242b499471030e5adbba4240af27ce9->leave($__internal_acba08861275e1bff5ac979f446ea4546242b499471030e5adbba4240af27ce9_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_7f698ae274a5102cb669a9719af7f93ace7cd9910092fa90f9883d4625ff6fe5 = $this->env->getExtension("native_profiler");
        $__internal_7f698ae274a5102cb669a9719af7f93ace7cd9910092fa90f9883d4625ff6fe5->enter($__internal_7f698ae274a5102cb669a9719af7f93ace7cd9910092fa90f9883d4625ff6fe5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_7f698ae274a5102cb669a9719af7f93ace7cd9910092fa90f9883d4625ff6fe5->leave($__internal_7f698ae274a5102cb669a9719af7f93ace7cd9910092fa90f9883d4625ff6fe5_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "@FOSUser/layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "@FOSUser/Group/show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
