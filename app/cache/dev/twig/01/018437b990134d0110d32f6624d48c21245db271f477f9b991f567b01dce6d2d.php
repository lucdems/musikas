<?php

/* ::default/vueMerePrive.html.twig */
class __TwigTemplate_75eafd7bc681d3de2e595f8d46909274649784f02bdf490d3ccc73a487618477 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'header' => array($this, 'block_header'),
            'contenu' => array($this, 'block_contenu'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_535a5f69234de591e5617f575904d1d565334df0fbad1b839093aea932e726de = $this->env->getExtension("native_profiler");
        $__internal_535a5f69234de591e5617f575904d1d565334df0fbad1b839093aea932e726de->enter($__internal_535a5f69234de591e5617f575904d1d565334df0fbad1b839093aea932e726de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::default/vueMerePrive.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 9
        $this->displayBlock('body', $context, $blocks);
        // line 81
        echo "        
        ";
        // line 82
        $this->displayBlock('javascripts', $context, $blocks);
        // line 89
        echo "    </body>
";
        // line 90
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 136
        echo "</html>
";
        
        $__internal_535a5f69234de591e5617f575904d1d565334df0fbad1b839093aea932e726de->leave($__internal_535a5f69234de591e5617f575904d1d565334df0fbad1b839093aea932e726de_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_598e186fa372c0da34ea941334a54aaf4ec668672c761782d8092b3ab7e62e58 = $this->env->getExtension("native_profiler");
        $__internal_598e186fa372c0da34ea941334a54aaf4ec668672c761782d8092b3ab7e62e58->enter($__internal_598e186fa372c0da34ea941334a54aaf4ec668672c761782d8092b3ab7e62e58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Musikas";
        
        $__internal_598e186fa372c0da34ea941334a54aaf4ec668672c761782d8092b3ab7e62e58->leave($__internal_598e186fa372c0da34ea941334a54aaf4ec668672c761782d8092b3ab7e62e58_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_969e40904cee7806b5c9d0a71fe0642765560273602d908aaf440d3660457546 = $this->env->getExtension("native_profiler");
        $__internal_969e40904cee7806b5c9d0a71fe0642765560273602d908aaf440d3660457546->enter($__internal_969e40904cee7806b5c9d0a71fe0642765560273602d908aaf440d3660457546_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "        
            ";
        // line 11
        $this->displayBlock('header', $context, $blocks);
        // line 34
        echo "            
            
            
            ";
        // line 37
        $this->displayBlock('contenu', $context, $blocks);
        // line 41
        echo "            
            ";
        // line 42
        $this->displayBlock('footer', $context, $blocks);
        // line 79
        echo "            
        ";
        
        $__internal_969e40904cee7806b5c9d0a71fe0642765560273602d908aaf440d3660457546->leave($__internal_969e40904cee7806b5c9d0a71fe0642765560273602d908aaf440d3660457546_prof);

    }

    // line 11
    public function block_header($context, array $blocks = array())
    {
        $__internal_807a43e908ca4c74330740ffd74f0bd7469e986521a0ed388a054bbb95a7c0f6 = $this->env->getExtension("native_profiler");
        $__internal_807a43e908ca4c74330740ffd74f0bd7469e986521a0ed388a054bbb95a7c0f6->enter($__internal_807a43e908ca4c74330740ffd74f0bd7469e986521a0ed388a054bbb95a7c0f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 12
        echo "            <header>
                <section>
                    <nav class=\"navbar navbar-default\">
                        <div class=\"container\">
                            <div class=\"navbar-header\">
                                <a class=\"navbar-brand navbar-link\" href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_accueil");
        echo "\"><img class=\"img-responsive\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/musikaslogo2.png"), "html", null, true);
        echo "\" id=\"icon\"></a>
                                <button class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navcol-1\" id=\"menuxs\"><span class=\"sr-only\">Toggle navigation</span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span></button>
                            </div>
                            <div class=\"collapse navbar-collapse\" id=\"navcol-1\">
                                <ul class=\"nav navbar-nav navbar-right\">
                                    <li role=\"presentation\"><a href=\"#\" id=\"boutondrapeaufrancais\"> </a></li>
                                    <li role=\"presentation\" id=\"test\"><a href=\"#\" id=\"boutondrapeaubasque\"> </a></li>
                                    <li role=\"presentation\"><a href=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_accueil");
        echo "\">Accueil </a></li>
                                    <li class=\"active\" role=\"presentation\"><a href=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("musikasespace_prive_homepage");
        echo "\">Espace privé</a></li>
                                    <li role=\"presentation\"><a href=\"#\" id=\"btnformulaire\">Inscription</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </section>
            </header>
            ";
        
        $__internal_807a43e908ca4c74330740ffd74f0bd7469e986521a0ed388a054bbb95a7c0f6->leave($__internal_807a43e908ca4c74330740ffd74f0bd7469e986521a0ed388a054bbb95a7c0f6_prof);

    }

    // line 37
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_f104f85f19e7f15da8bfca53c1eb44202c39a583a2298719188f9fcd72e07c39 = $this->env->getExtension("native_profiler");
        $__internal_f104f85f19e7f15da8bfca53c1eb44202c39a583a2298719188f9fcd72e07c39->enter($__internal_f104f85f19e7f15da8bfca53c1eb44202c39a583a2298719188f9fcd72e07c39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 38
        echo "                
            
            ";
        
        $__internal_f104f85f19e7f15da8bfca53c1eb44202c39a583a2298719188f9fcd72e07c39->leave($__internal_f104f85f19e7f15da8bfca53c1eb44202c39a583a2298719188f9fcd72e07c39_prof);

    }

    // line 42
    public function block_footer($context, array $blocks = array())
    {
        $__internal_d68787fe27b59336a21ed052b1f2b43eea124cfed773e306f7e85deb0db436e2 = $this->env->getExtension("native_profiler");
        $__internal_d68787fe27b59336a21ed052b1f2b43eea124cfed773e306f7e85deb0db436e2->enter($__internal_d68787fe27b59336a21ed052b1f2b43eea124cfed773e306f7e85deb0db436e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 43
        echo "                <div class=\"footer-clean\">
                <footer>
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-3 col-sm-4 item\">
                                <h3>Contact </h3>
                                <ul>
                                    <li><a href=\"#\">Se rendre à l'école</a></li>
                                    <li><a href=\"#\">Nous joindre</a></li>
                                    <li><a href=\"#\">autre contact...</a></li>
                                </ul>
                            </div>
                            <div class=\"col-md-3 col-sm-4 item\">
                                <h3>Liens </h3>
                                <ul>
                                    <li><a href=\"#\">Financeur </a></li>
                                    <li><a href=\"#\">.... </a></li>
                                    <li><a href=\"#\">autres écoles...</a></li>
                                </ul>
                            </div>
                            <div class=\"col-md-3 col-sm-4 item\">
                                <h3>Mention légales</h3>
                                <ul>
                                    <li><a href=\"#\">Statuts </a></li>
                                    <li><a href=\"#\">Compte rendu CA</a></li>
                                    <li><a href=\"#\">Documents </a></li>
                                </ul>
                            </div>
                            <div class=\"col-lg-3 col-md-3 item social\"><a href=\"#\"><i class=\"icon ion-social-facebook\"></i></a>
                            <p class=\"copyright\">Musikas © 2017</p>
                            </div>
                        </div>
                    </div>
                </footer>
                </div>
            ";
        
        $__internal_d68787fe27b59336a21ed052b1f2b43eea124cfed773e306f7e85deb0db436e2->leave($__internal_d68787fe27b59336a21ed052b1f2b43eea124cfed773e306f7e85deb0db436e2_prof);

    }

    // line 82
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_3385bb76225385c0d89350408e9127ece3f963fb9d219e924b33fd295f357e93 = $this->env->getExtension("native_profiler");
        $__internal_3385bb76225385c0d89350408e9127ece3f963fb9d219e924b33fd295f357e93->enter($__internal_3385bb76225385c0d89350408e9127ece3f963fb9d219e924b33fd295f357e93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 83
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/js/menuresponsif.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/js/Pricing-Tables.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/js/Pricing-Tables1.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_3385bb76225385c0d89350408e9127ece3f963fb9d219e924b33fd295f357e93->leave($__internal_3385bb76225385c0d89350408e9127ece3f963fb9d219e924b33fd295f357e93_prof);

    }

    // line 90
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f903acebf17297126cee32b3c75a354065d900143d132b508d6e7b59533d5700 = $this->env->getExtension("native_profiler");
        $__internal_f903acebf17297126cee32b3c75a354065d900143d132b508d6e7b59533d5700->enter($__internal_f903acebf17297126cee32b3c75a354065d900143d132b508d6e7b59533d5700_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 91
        echo "<style>
    body { background: #F5F5F5; font: 18px/1.5 sans-serif; }
    h1, h2 { line-height: 1.2; margin: 0 0 .5em; }
    h1 { font-size: 36px; }
    h2 { font-size: 21px; margin-bottom: 1em; }
    p { margin: 0 0 1em 0; }
    a { color: #0000F0; }
    a:hover { text-decoration: none; }
    code { background: #F5F5F5; max-width: 100px; padding: 2px 6px; word-wrap: break-word; }
    #wrapper { background: #FFF; margin: 1em auto; max-width: 800px; width: 95%; }
    #container { padding: 2em; }
    #welcome, #status { margin-bottom: 2em; }
    #welcome h1 span { display: block; font-size: 75%; }
    #icon-status, #icon-book { float: left; height: 64px; margin-right: 1em; margin-top: -4px; width: 64px; }
    #icon-book { display: none; }

    @media (min-width: 768px) {
        #wrapper { width: 80%; margin: 2em auto; }
        #icon-book { display: inline-block; }
        #status a, #next a { display: block; }

        @-webkit-keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        @keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        .sf-toolbar { opacity: 0; -webkit-animation: fade-in 1s .2s forwards; animation: fade-in 1s .2s forwards;}
    }
</style>
    <link rel=\"stylesheet\" href=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("https://fonts.googleapis.com/css?family=Roboto:300,400,500,700"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("https://fonts.googleapis.com/css?family=Lora"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/fonts/ionicons.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Article-Clean.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Article-List-1.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Article-List.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Footer-Clean.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("https://daneden.github.io/animate.css/animate.min.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Login-Form-Dark.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Pricing-Tables.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Pricing-Tables1.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/user.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Sidebar-Menu.css.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Sidebar-Menu1.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Box-panels.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Box-panels1.css"), "html", null, true);
        echo "\"/>

";
        
        $__internal_f903acebf17297126cee32b3c75a354065d900143d132b508d6e7b59533d5700->leave($__internal_f903acebf17297126cee32b3c75a354065d900143d132b508d6e7b59533d5700_prof);

    }

    public function getTemplateName()
    {
        return "::default/vueMerePrive.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  346 => 133,  342 => 132,  338 => 131,  334 => 130,  330 => 129,  326 => 128,  322 => 127,  318 => 126,  314 => 125,  310 => 124,  306 => 123,  302 => 122,  298 => 121,  294 => 120,  290 => 119,  286 => 118,  282 => 117,  254 => 91,  248 => 90,  239 => 87,  235 => 86,  231 => 85,  227 => 84,  222 => 83,  216 => 82,  174 => 43,  168 => 42,  159 => 38,  153 => 37,  137 => 25,  133 => 24,  121 => 17,  114 => 12,  108 => 11,  100 => 79,  98 => 42,  95 => 41,  93 => 37,  88 => 34,  86 => 11,  83 => 10,  77 => 9,  65 => 5,  57 => 136,  55 => 90,  52 => 89,  50 => 82,  47 => 81,  45 => 9,  39 => 6,  35 => 5,  29 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Musikas{% endblock %}</title>*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}*/
/*         */
/*             {% block header %}*/
/*             <header>*/
/*                 <section>*/
/*                     <nav class="navbar navbar-default">*/
/*                         <div class="container">*/
/*                             <div class="navbar-header">*/
/*                                 <a class="navbar-brand navbar-link" href="{{ path('musikasvitrine_accueil') }}"><img class="img-responsive" src="{{ asset('bundles/assets/img/musikaslogo2.png') }}" id="icon"></a>*/
/*                                 <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1" id="menuxs"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>*/
/*                             </div>*/
/*                             <div class="collapse navbar-collapse" id="navcol-1">*/
/*                                 <ul class="nav navbar-nav navbar-right">*/
/*                                     <li role="presentation"><a href="#" id="boutondrapeaufrancais"> </a></li>*/
/*                                     <li role="presentation" id="test"><a href="#" id="boutondrapeaubasque"> </a></li>*/
/*                                     <li role="presentation"><a href="{{ path('musikasvitrine_accueil') }}">Accueil </a></li>*/
/*                                     <li class="active" role="presentation"><a href="{{ path('musikasespace_prive_homepage') }}">Espace privé</a></li>*/
/*                                     <li role="presentation"><a href="#" id="btnformulaire">Inscription</a></li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                     </nav>*/
/*                 </section>*/
/*             </header>*/
/*             {% endblock %}*/
/*             */
/*             */
/*             */
/*             {% block contenu %}*/
/*                 */
/*             */
/*             {% endblock %}*/
/*             */
/*             {% block footer %}*/
/*                 <div class="footer-clean">*/
/*                 <footer>*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-3 col-sm-4 item">*/
/*                                 <h3>Contact </h3>*/
/*                                 <ul>*/
/*                                     <li><a href="#">Se rendre à l'école</a></li>*/
/*                                     <li><a href="#">Nous joindre</a></li>*/
/*                                     <li><a href="#">autre contact...</a></li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                             <div class="col-md-3 col-sm-4 item">*/
/*                                 <h3>Liens </h3>*/
/*                                 <ul>*/
/*                                     <li><a href="#">Financeur </a></li>*/
/*                                     <li><a href="#">.... </a></li>*/
/*                                     <li><a href="#">autres écoles...</a></li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                             <div class="col-md-3 col-sm-4 item">*/
/*                                 <h3>Mention légales</h3>*/
/*                                 <ul>*/
/*                                     <li><a href="#">Statuts </a></li>*/
/*                                     <li><a href="#">Compte rendu CA</a></li>*/
/*                                     <li><a href="#">Documents </a></li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                             <div class="col-lg-3 col-md-3 item social"><a href="#"><i class="icon ion-social-facebook"></i></a>*/
/*                             <p class="copyright">Musikas © 2017</p>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </footer>*/
/*                 </div>*/
/*             {% endblock %}*/
/*             */
/*         {% endblock %}*/
/*         */
/*         {% block javascripts %}*/
/*             <script src="{{ asset('bundles/assets/js/jquery.min.js') }}"></script>*/
/*             <script src="{{ asset('bundles/assets/bootstrap/js/bootstrap.min.js') }}"></script>*/
/*             <script src="{{ asset('bundles/assets/js/menuresponsif.js') }}"></script>*/
/*             <script src="{{ asset('bundles/assets/js/Pricing-Tables.js') }}"></script>*/
/*             <script src="{{ asset('bundles/assets/js/Pricing-Tables1.js') }}"></script>*/
/*         {% endblock %}*/
/*     </body>*/
/* {% block stylesheets %}*/
/* <style>*/
/*     body { background: #F5F5F5; font: 18px/1.5 sans-serif; }*/
/*     h1, h2 { line-height: 1.2; margin: 0 0 .5em; }*/
/*     h1 { font-size: 36px; }*/
/*     h2 { font-size: 21px; margin-bottom: 1em; }*/
/*     p { margin: 0 0 1em 0; }*/
/*     a { color: #0000F0; }*/
/*     a:hover { text-decoration: none; }*/
/*     code { background: #F5F5F5; max-width: 100px; padding: 2px 6px; word-wrap: break-word; }*/
/*     #wrapper { background: #FFF; margin: 1em auto; max-width: 800px; width: 95%; }*/
/*     #container { padding: 2em; }*/
/*     #welcome, #status { margin-bottom: 2em; }*/
/*     #welcome h1 span { display: block; font-size: 75%; }*/
/*     #icon-status, #icon-book { float: left; height: 64px; margin-right: 1em; margin-top: -4px; width: 64px; }*/
/*     #icon-book { display: none; }*/
/* */
/*     @media (min-width: 768px) {*/
/*         #wrapper { width: 80%; margin: 2em auto; }*/
/*         #icon-book { display: inline-block; }*/
/*         #status a, #next a { display: block; }*/
/* */
/*         @-webkit-keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }*/
/*         @keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }*/
/*         .sf-toolbar { opacity: 0; -webkit-animation: fade-in 1s .2s forwards; animation: fade-in 1s .2s forwards;}*/
/*     }*/
/* </style>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/bootstrap/css/bootstrap.min.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('https://fonts.googleapis.com/css?family=Lora') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/fonts/ionicons.min.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Article-Clean.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Article-List-1.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Article-List.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Footer-Clean.css') }}">*/
/*     <link rel="stylesheet" href="{{ asset('https://daneden.github.io/animate.css/animate.min.css') }}">*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Login-Form-Dark.css') }}">*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Pricing-Tables.css') }}">*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Pricing-Tables1.css') }}">*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/user.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Sidebar-Menu.css.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Sidebar-Menu1.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Box-panels.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Box-panels1.css') }}"/>*/
/* */
/* {% endblock %}*/
/* </html>*/
/* */
