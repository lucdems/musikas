<?php

/* FOSUserBundle:Registration:register_content.html.twig */
class __TwigTemplate_4d25ae7539e01cf7bf6d89f47f67b119fe6738ee93e369d35675d057cda7534f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_109709000dc6aa38b17a29cf7002202d983b3ca65922b356b48824b672fdfff8 = $this->env->getExtension("native_profiler");
        $__internal_109709000dc6aa38b17a29cf7002202d983b3ca65922b356b48824b672fdfff8->enter($__internal_109709000dc6aa38b17a29cf7002202d983b3ca65922b356b48824b672fdfff8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register_content.html.twig"));

        // line 1
        echo "
";
        // line 3
        echo "
";
        // line 4
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('routing')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "
    ";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 9
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo " 

 




<!--
    <section>
        <div class=\"register-photo\">
    <div class=\"container\">
        <div class=\"form-container\">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"row\">
                        <div class=\"col-md-6\">
                            <form name=\"fos_user_registration_form\" method=\"post\" action=\"/musikas/web/app_dev.php/register/\" class=\"fos_user_registration_register\">
                                <h2 class=\"text-center\"><strong>Cr</strong>éer un compte.</h2>
                                
                                <div class=\"form-group\">
                                    <input class=\"form-control\" type=\"email\" placeholder=\"Email\" id=\"fos_user_registration_form_email\" name=\"fos_user_registration_form[email]\" required=\"required\" />
                                </div>
                                <div class=\"form-group\">
                                    <input class=\"form-control\" type=\"text\" placeholder=\"Prénom\" id=\"fos_user_registration_form_prenom\" name=\"fos_user_registration_form[prenom]\" required=\"required\" maxlength=\"255\"/>
                                </div>
                                <div class=\"form-group\">
                                    <input class=\"form-control\" type=\"text\"  placeholder=\"Nom\" id=\"fos_user_registration_form_nom\" name=\"fos_user_registration_form[nom]\" required=\"required\" maxlength=\"255\" />
                                </div>
                                <div class=\"form-group\">
                                    <input class=\"form-control\" type=\"password\" placeholder=\"Mot de passe\" id=\"fos_user_registration_form_plainPassword_first\" name=\"fos_user_registration_form[plainPassword][first]\" required=\"required\" />
                                </div>
                                <div class=\"form-group\">
                                    <input class=\"form-control\" type=\"password\" placeholder=\"Répéter le mot de passe\" id=\"fos_user_registration_form_plainPassword_second\" name=\"fos_user_registration_form[plainPassword][second]\" required=\"required\" />
                                        <input type=\"hidden\" id=\"fos_user_registration_form__token\" name=\"fos_user_registration_form[_token]\" value=\"FA48VnKs5pA4Z8W6EmzLoPoJ8xkobkox1U9nC3obAdE\" />
                                </div>
                                    
                                <div class=\"form-group\">
                                    <button class=\"btn btn-primary btn-block\" type=\"submit\">S&#39;inscrire </button>
                                </div><a href=\"#\" class=\"already\">Vous avez déjà un compte ? Connectez vous-ici.</a></form>
                        </div>
                        <div class=\"col-md-6\">
                            <div class=\"visible-md-block visible-lg-block image-holder\"><img class=\"img-responsive\" src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/jazz_in_his_soul.jpg"), "html", null, true);
        echo "\" id=\"img_connexion\" /></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    </section>
    -->
";
        
        $__internal_109709000dc6aa38b17a29cf7002202d983b3ca65922b356b48824b672fdfff8->leave($__internal_109709000dc6aa38b17a29cf7002202d983b3ca65922b356b48824b672fdfff8_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 50,  42 => 9,  37 => 7,  32 => 5,  28 => 4,  25 => 3,  22 => 1,);
    }
}
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}*/
/*     {{ form_widget(form) }}*/
/*     <div>*/
/*         <input type="submit" value="{{ 'registration.submit'|trans }}" />*/
/*     </div>*/
/* {{ form_end(form) }} */
/* */
/*  */
/* */
/* */
/* */
/* */
/* <!--*/
/*     <section>*/
/*         <div class="register-photo">*/
/*     <div class="container">*/
/*         <div class="form-container">*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <div class="row">*/
/*                         <div class="col-md-6">*/
/*                             <form name="fos_user_registration_form" method="post" action="/musikas/web/app_dev.php/register/" class="fos_user_registration_register">*/
/*                                 <h2 class="text-center"><strong>Cr</strong>éer un compte.</h2>*/
/*                                 */
/*                                 <div class="form-group">*/
/*                                     <input class="form-control" type="email" placeholder="Email" id="fos_user_registration_form_email" name="fos_user_registration_form[email]" required="required" />*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <input class="form-control" type="text" placeholder="Prénom" id="fos_user_registration_form_prenom" name="fos_user_registration_form[prenom]" required="required" maxlength="255"/>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <input class="form-control" type="text"  placeholder="Nom" id="fos_user_registration_form_nom" name="fos_user_registration_form[nom]" required="required" maxlength="255" />*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <input class="form-control" type="password" placeholder="Mot de passe" id="fos_user_registration_form_plainPassword_first" name="fos_user_registration_form[plainPassword][first]" required="required" />*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <input class="form-control" type="password" placeholder="Répéter le mot de passe" id="fos_user_registration_form_plainPassword_second" name="fos_user_registration_form[plainPassword][second]" required="required" />*/
/*                                         <input type="hidden" id="fos_user_registration_form__token" name="fos_user_registration_form[_token]" value="FA48VnKs5pA4Z8W6EmzLoPoJ8xkobkox1U9nC3obAdE" />*/
/*                                 </div>*/
/*                                     */
/*                                 <div class="form-group">*/
/*                                     <button class="btn btn-primary btn-block" type="submit">S&#39;inscrire </button>*/
/*                                 </div><a href="#" class="already">Vous avez déjà un compte ? Connectez vous-ici.</a></form>*/
/*                         </div>*/
/*                         <div class="col-md-6">*/
/*                             <div class="visible-md-block visible-lg-block image-holder"><img class="img-responsive" src="{{ asset('bundles/assets/img/jazz_in_his_soul.jpg') }}" id="img_connexion" /></div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/*     </section>*/
/*     -->*/
/* */
