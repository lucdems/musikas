<?php

/* @EasyAdmin/default/layout.html.twig */
class __TwigTemplate_06a884852f36fe87e8ded6d6945327c896fc2740da98cdebd1bdcde58a7669c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'head_stylesheets' => array($this, 'block_head_stylesheets'),
            'head_favicon' => array($this, 'block_head_favicon'),
            'head_javascript' => array($this, 'block_head_javascript'),
            'adminlte_options' => array($this, 'block_adminlte_options'),
            'body' => array($this, 'block_body'),
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'wrapper' => array($this, 'block_wrapper'),
            'header' => array($this, 'block_header'),
            'header_logo' => array($this, 'block_header_logo'),
            'header_custom_menu' => array($this, 'block_header_custom_menu'),
            'user_menu' => array($this, 'block_user_menu'),
            'sidebar' => array($this, 'block_sidebar'),
            'main_menu_wrapper' => array($this, 'block_main_menu_wrapper'),
            'content' => array($this, 'block_content'),
            'flash_messages' => array($this, 'block_flash_messages'),
            'content_header' => array($this, 'block_content_header'),
            'content_title' => array($this, 'block_content_title'),
            'content_help' => array($this, 'block_content_help'),
            'main' => array($this, 'block_main'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1da0bf2a3fa6fefeea49cf471a856c60feb711896e422a678a85b4ca6bfc9d16 = $this->env->getExtension("native_profiler");
        $__internal_1da0bf2a3fa6fefeea49cf471a856c60feb711896e422a678a85b4ca6bfc9d16->enter($__internal_1da0bf2a3fa6fefeea49cf471a856c60feb711896e422a678a85b4ca6bfc9d16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, _twig_default_filter(twig_first($this->env, twig_split_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "locale", array()), "_")), "en"), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"robots\" content=\"noindex, nofollow, noarchive, nosnippet, noodp, noimageindex, notranslate, nocache\" />
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <meta name=\"generator\" content=\"EasyAdmin\" />

        <title>";
        // line 10
        $this->displayBlock('page_title', $context, $blocks);
        echo "</title>

        ";
        // line 12
        $this->displayBlock('head_stylesheets', $context, $blocks);
        // line 18
        echo "
        ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('easyadmin_extension')->getBackendConfiguration("design.assets.css"));
        foreach ($context['_seq'] as $context["_key"] => $context["css_asset"]) {
            // line 20
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($context["css_asset"]), "html", null, true);
            echo "\">
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['css_asset'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "
        ";
        // line 23
        $this->displayBlock('head_favicon', $context, $blocks);
        // line 27
        echo "
        ";
        // line 28
        $this->displayBlock('head_javascript', $context, $blocks);
        // line 45
        echo "
        ";
        // line 46
        if ($this->env->getExtension('easyadmin_extension')->getBackendConfiguration("design.rtl")) {
            // line 47
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/easyadmin/stylesheet/bootstrap-rtl.min.css"), "html", null, true);
            echo "\">
            <link rel=\"stylesheet\" href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/easyadmin/stylesheet/adminlte-rtl.min.css"), "html", null, true);
            echo "\">
        ";
        }
        // line 50
        echo "
        <!--[if lt IE 9]>
            <script src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/easyadmin/stylesheet/html5shiv.min.css"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/easyadmin/stylesheet/respond.min.css"), "html", null, true);
        echo "\"></script>
        <![endif]-->
    </head>

    ";
        // line 57
        $this->displayBlock('body', $context, $blocks);
        // line 147
        echo "</html>
";
        
        $__internal_1da0bf2a3fa6fefeea49cf471a856c60feb711896e422a678a85b4ca6bfc9d16->leave($__internal_1da0bf2a3fa6fefeea49cf471a856c60feb711896e422a678a85b4ca6bfc9d16_prof);

    }

    // line 10
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_020d533452b49579e3c73164731730e2c363da6271cf168e57e34ab9d8727580 = $this->env->getExtension("native_profiler");
        $__internal_020d533452b49579e3c73164731730e2c363da6271cf168e57e34ab9d8727580->enter($__internal_020d533452b49579e3c73164731730e2c363da6271cf168e57e34ab9d8727580_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        echo strip_tags($this->renderBlock("content_title", $context, $blocks));
        
        $__internal_020d533452b49579e3c73164731730e2c363da6271cf168e57e34ab9d8727580->leave($__internal_020d533452b49579e3c73164731730e2c363da6271cf168e57e34ab9d8727580_prof);

    }

    // line 12
    public function block_head_stylesheets($context, array $blocks = array())
    {
        $__internal_d38d06e7206f214fa583a37af043ef5d982cfb64127718cef163f509f2026d8f = $this->env->getExtension("native_profiler");
        $__internal_d38d06e7206f214fa583a37af043ef5d982cfb64127718cef163f509f2026d8f->enter($__internal_d38d06e7206f214fa583a37af043ef5d982cfb64127718cef163f509f2026d8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_stylesheets"));

        // line 13
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/easyadmin/stylesheet/easyadmin-all.min.css"), "html", null, true);
        echo "\">
            <style>
                ";
        // line 15
        echo $this->env->getExtension('easyadmin_extension')->getBackendConfiguration("_internal.custom_css");
        echo "
            </style>
        ";
        
        $__internal_d38d06e7206f214fa583a37af043ef5d982cfb64127718cef163f509f2026d8f->leave($__internal_d38d06e7206f214fa583a37af043ef5d982cfb64127718cef163f509f2026d8f_prof);

    }

    // line 23
    public function block_head_favicon($context, array $blocks = array())
    {
        $__internal_0527e45daa8fba9c1af295a3819d2a4605b6da3442ba47a51a143e0d499fd001 = $this->env->getExtension("native_profiler");
        $__internal_0527e45daa8fba9c1af295a3819d2a4605b6da3442ba47a51a143e0d499fd001->enter($__internal_0527e45daa8fba9c1af295a3819d2a4605b6da3442ba47a51a143e0d499fd001_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_favicon"));

        // line 24
        echo "            ";
        $context["favicon"] = $this->env->getExtension('easyadmin_extension')->getBackendConfiguration("design.assets.favicon");
        // line 25
        echo "            <link rel=\"icon\" type=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["favicon"]) ? $context["favicon"] : $this->getContext($context, "favicon")), "mime_type", array()), "html", null, true);
        echo "\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["favicon"]) ? $context["favicon"] : $this->getContext($context, "favicon")), "path", array())), "html", null, true);
        echo "\" />
        ";
        
        $__internal_0527e45daa8fba9c1af295a3819d2a4605b6da3442ba47a51a143e0d499fd001->leave($__internal_0527e45daa8fba9c1af295a3819d2a4605b6da3442ba47a51a143e0d499fd001_prof);

    }

    // line 28
    public function block_head_javascript($context, array $blocks = array())
    {
        $__internal_253df7629bfa43c954f687acf18a7751fb924e961f55e827b06f857759d75bc5 = $this->env->getExtension("native_profiler");
        $__internal_253df7629bfa43c954f687acf18a7751fb924e961f55e827b06f857759d75bc5->enter($__internal_253df7629bfa43c954f687acf18a7751fb924e961f55e827b06f857759d75bc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_javascript"));

        // line 29
        echo "            ";
        $this->displayBlock('adminlte_options', $context, $blocks);
        // line 42
        echo "
            <script src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/easyadmin/javascript/easyadmin-all.min.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_253df7629bfa43c954f687acf18a7751fb924e961f55e827b06f857759d75bc5->leave($__internal_253df7629bfa43c954f687acf18a7751fb924e961f55e827b06f857759d75bc5_prof);

    }

    // line 29
    public function block_adminlte_options($context, array $blocks = array())
    {
        $__internal_095e7bf894293ebe5a19c710afd35a463c0cc60d82811510a14f28d455ac6fce = $this->env->getExtension("native_profiler");
        $__internal_095e7bf894293ebe5a19c710afd35a463c0cc60d82811510a14f28d455ac6fce->enter($__internal_095e7bf894293ebe5a19c710afd35a463c0cc60d82811510a14f28d455ac6fce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "adminlte_options"));

        // line 30
        echo "                <script type=\"text/javascript\">
                    var AdminLTEOptions = {
                        animationSpeed: 'normal',
                        sidebarExpandOnHover: false,
                        enableBoxRefresh: false,
                        enableBSToppltip: false,
                        enableFastclick: false,
                        enableControlSidebar: false,
                        enableBoxWidget: false
                    };
                </script>
            ";
        
        $__internal_095e7bf894293ebe5a19c710afd35a463c0cc60d82811510a14f28d455ac6fce->leave($__internal_095e7bf894293ebe5a19c710afd35a463c0cc60d82811510a14f28d455ac6fce_prof);

    }

    // line 57
    public function block_body($context, array $blocks = array())
    {
        $__internal_fc10527b17517f323d58594a92c4f8a696548234ff79379dce3546bcec604c6c = $this->env->getExtension("native_profiler");
        $__internal_fc10527b17517f323d58594a92c4f8a696548234ff79379dce3546bcec604c6c->enter($__internal_fc10527b17517f323d58594a92c4f8a696548234ff79379dce3546bcec604c6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 58
        echo "    <body id=\"";
        $this->displayBlock('body_id', $context, $blocks);
        echo "\" class=\"easyadmin sidebar-mini ";
        $this->displayBlock('body_class', $context, $blocks);
        echo " ";
        echo (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "cookies", array()), "has", array(0 => "_easyadmin_navigation_iscollapsed"), "method")) ? ("sidebar-collapse") : (""));
        echo "\">
        <div class=\"wrapper\">
        ";
        // line 60
        $this->displayBlock('wrapper', $context, $blocks);
        // line 138
        echo "        </div>

        ";
        // line 140
        $this->displayBlock('body_javascript', $context, $blocks);
        // line 141
        echo "
        ";
        // line 142
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('easyadmin_extension')->getBackendConfiguration("design.assets.js"));
        foreach ($context['_seq'] as $context["_key"] => $context["js_asset"]) {
            // line 143
            echo "            <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($context["js_asset"]), "html", null, true);
            echo "\"></script>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['js_asset'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 145
        echo "    </body>
    ";
        
        $__internal_fc10527b17517f323d58594a92c4f8a696548234ff79379dce3546bcec604c6c->leave($__internal_fc10527b17517f323d58594a92c4f8a696548234ff79379dce3546bcec604c6c_prof);

    }

    // line 58
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_544c6cf5f2d8a6bf65fea75f47bf69bf83111132eec0dca30755b857ae9a07aa = $this->env->getExtension("native_profiler");
        $__internal_544c6cf5f2d8a6bf65fea75f47bf69bf83111132eec0dca30755b857ae9a07aa->enter($__internal_544c6cf5f2d8a6bf65fea75f47bf69bf83111132eec0dca30755b857ae9a07aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_544c6cf5f2d8a6bf65fea75f47bf69bf83111132eec0dca30755b857ae9a07aa->leave($__internal_544c6cf5f2d8a6bf65fea75f47bf69bf83111132eec0dca30755b857ae9a07aa_prof);

    }

    public function block_body_class($context, array $blocks = array())
    {
        $__internal_0c718b22ea7dd861aa65a7eeab43ebcb10aa2f4f6611714d932dd164731a04f0 = $this->env->getExtension("native_profiler");
        $__internal_0c718b22ea7dd861aa65a7eeab43ebcb10aa2f4f6611714d932dd164731a04f0->enter($__internal_0c718b22ea7dd861aa65a7eeab43ebcb10aa2f4f6611714d932dd164731a04f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        
        $__internal_0c718b22ea7dd861aa65a7eeab43ebcb10aa2f4f6611714d932dd164731a04f0->leave($__internal_0c718b22ea7dd861aa65a7eeab43ebcb10aa2f4f6611714d932dd164731a04f0_prof);

    }

    // line 60
    public function block_wrapper($context, array $blocks = array())
    {
        $__internal_830ce734cf4fe1089d2ac123f7c2251d28825447552f82e0526562fd1c312411 = $this->env->getExtension("native_profiler");
        $__internal_830ce734cf4fe1089d2ac123f7c2251d28825447552f82e0526562fd1c312411->enter($__internal_830ce734cf4fe1089d2ac123f7c2251d28825447552f82e0526562fd1c312411_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "wrapper"));

        // line 61
        echo "            <header class=\"main-header\">
            ";
        // line 62
        $this->displayBlock('header', $context, $blocks);
        // line 95
        echo "            </header>

            <aside class=\"main-sidebar\">
            ";
        // line 98
        $this->displayBlock('sidebar', $context, $blocks);
        // line 109
        echo "            </aside>

            <div class=\"content-wrapper\">
            ";
        // line 112
        $this->displayBlock('content', $context, $blocks);
        // line 136
        echo "            </div>
        ";
        
        $__internal_830ce734cf4fe1089d2ac123f7c2251d28825447552f82e0526562fd1c312411->leave($__internal_830ce734cf4fe1089d2ac123f7c2251d28825447552f82e0526562fd1c312411_prof);

    }

    // line 62
    public function block_header($context, array $blocks = array())
    {
        $__internal_178276d5177d8d802a4afa9eabb744d5a2c273c6b7c92994416eab7cd7cbf799 = $this->env->getExtension("native_profiler");
        $__internal_178276d5177d8d802a4afa9eabb744d5a2c273c6b7c92994416eab7cd7cbf799->enter($__internal_178276d5177d8d802a4afa9eabb744d5a2c273c6b7c92994416eab7cd7cbf799_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 63
        echo "                <nav class=\"navbar\" role=\"navigation\">
                    <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                        <span class=\"sr-only\">";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("toggle_navigation", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
                    </a>

                    <div id=\"header-logo\">
                        ";
        // line 69
        $this->displayBlock('header_logo', $context, $blocks);
        // line 74
        echo "                    </div>

                    <div class=\"navbar-custom-menu\">
                    ";
        // line 77
        $this->displayBlock('header_custom_menu', $context, $blocks);
        // line 92
        echo "                    </div>
                </nav>
            ";
        
        $__internal_178276d5177d8d802a4afa9eabb744d5a2c273c6b7c92994416eab7cd7cbf799->leave($__internal_178276d5177d8d802a4afa9eabb744d5a2c273c6b7c92994416eab7cd7cbf799_prof);

    }

    // line 69
    public function block_header_logo($context, array $blocks = array())
    {
        $__internal_f1e5354ce442106f4c8257f066da9a20eb4e9d0bf6189f5bc4f52f1c4430c89e = $this->env->getExtension("native_profiler");
        $__internal_f1e5354ce442106f4c8257f066da9a20eb4e9d0bf6189f5bc4f52f1c4430c89e->enter($__internal_f1e5354ce442106f4c8257f066da9a20eb4e9d0bf6189f5bc4f52f1c4430c89e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_logo"));

        // line 70
        echo "                            <a class=\"logo ";
        echo (((twig_length_filter($this->env, $this->env->getExtension('easyadmin_extension')->getBackendConfiguration("site_name")) > 14)) ? ("logo-long") : (""));
        echo "\" title=\"";
        echo twig_escape_filter($this->env, strip_tags($this->env->getExtension('easyadmin_extension')->getBackendConfiguration("site_name")), "html", null, true);
        echo "\" href=\"";
        echo $this->env->getExtension('routing')->getPath("easyadmin");
        echo "\">
                                ";
        // line 71
        echo $this->env->getExtension('easyadmin_extension')->getBackendConfiguration("site_name");
        echo "
                            </a>
                        ";
        
        $__internal_f1e5354ce442106f4c8257f066da9a20eb4e9d0bf6189f5bc4f52f1c4430c89e->leave($__internal_f1e5354ce442106f4c8257f066da9a20eb4e9d0bf6189f5bc4f52f1c4430c89e_prof);

    }

    // line 77
    public function block_header_custom_menu($context, array $blocks = array())
    {
        $__internal_b1d3ee3d737884faa35610aefd3e00b0353f75cae504384d221009f563a7d6e2 = $this->env->getExtension("native_profiler");
        $__internal_b1d3ee3d737884faa35610aefd3e00b0353f75cae504384d221009f563a7d6e2->enter($__internal_b1d3ee3d737884faa35610aefd3e00b0353f75cae504384d221009f563a7d6e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_custom_menu"));

        // line 78
        echo "                        <ul class=\"nav navbar-nav\">
                            <li class=\"user user-menu\">
                                ";
        // line 80
        $this->displayBlock('user_menu', $context, $blocks);
        // line 89
        echo "                            </li>
                        </ul>
                    ";
        
        $__internal_b1d3ee3d737884faa35610aefd3e00b0353f75cae504384d221009f563a7d6e2->leave($__internal_b1d3ee3d737884faa35610aefd3e00b0353f75cae504384d221009f563a7d6e2_prof);

    }

    // line 80
    public function block_user_menu($context, array $blocks = array())
    {
        $__internal_948257f516cf4657b7fc03eb9323f15400f6d365e93fe2aa4f35d53ad0ffbbf9 = $this->env->getExtension("native_profiler");
        $__internal_948257f516cf4657b7fc03eb9323f15400f6d365e93fe2aa4f35d53ad0ffbbf9->enter($__internal_948257f516cf4657b7fc03eb9323f15400f6d365e93fe2aa4f35d53ad0ffbbf9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_menu"));

        // line 81
        echo "                                    <span class=\"sr-only\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.logged_in_as", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
                                    <i class=\"hidden-xs fa fa-user\"></i>
                                    ";
        // line 83
        if ((($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()))) : (""))) {
            // line 84
            echo "                                        ";
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array(), "any", false, true), "username", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array(), "any", false, true), "username", array()), $this->env->getExtension('translator')->trans("user.unnamed", array(), "EasyAdminBundle"))) : ($this->env->getExtension('translator')->trans("user.unnamed", array(), "EasyAdminBundle"))), "html", null, true);
            echo "
                                    ";
        } else {
            // line 86
            echo "                                        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.anonymous", array(), "EasyAdminBundle"), "html", null, true);
            echo "
                                    ";
        }
        // line 88
        echo "                                ";
        
        $__internal_948257f516cf4657b7fc03eb9323f15400f6d365e93fe2aa4f35d53ad0ffbbf9->leave($__internal_948257f516cf4657b7fc03eb9323f15400f6d365e93fe2aa4f35d53ad0ffbbf9_prof);

    }

    // line 98
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_a9ebb36c92b140fb7cd6cae9813025ccab4fa5b27fbd60ee5cf9c79b2907c31b = $this->env->getExtension("native_profiler");
        $__internal_a9ebb36c92b140fb7cd6cae9813025ccab4fa5b27fbd60ee5cf9c79b2907c31b->enter($__internal_a9ebb36c92b140fb7cd6cae9813025ccab4fa5b27fbd60ee5cf9c79b2907c31b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 99
        echo "                <section class=\"sidebar\">
                    ";
        // line 100
        $this->displayBlock('main_menu_wrapper', $context, $blocks);
        // line 107
        echo "                </section>
            ";
        
        $__internal_a9ebb36c92b140fb7cd6cae9813025ccab4fa5b27fbd60ee5cf9c79b2907c31b->leave($__internal_a9ebb36c92b140fb7cd6cae9813025ccab4fa5b27fbd60ee5cf9c79b2907c31b_prof);

    }

    // line 100
    public function block_main_menu_wrapper($context, array $blocks = array())
    {
        $__internal_e7e90d12c4d8812ec800e8f51597fbcedf27d3df75f27eb9abbe1a5a5b9f2815 = $this->env->getExtension("native_profiler");
        $__internal_e7e90d12c4d8812ec800e8f51597fbcedf27d3df75f27eb9abbe1a5a5b9f2815->enter($__internal_e7e90d12c4d8812ec800e8f51597fbcedf27d3df75f27eb9abbe1a5a5b9f2815_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_menu_wrapper"));

        // line 101
        echo "                        ";
        echo twig_include($this->env, $context, array(0 => ((        // line 102
array_key_exists("_entity_config", $context)) ? ($this->getAttribute($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "templates", array()), "menu", array())) : ("")), 1 => $this->env->getExtension('easyadmin_extension')->getBackendConfiguration("design.templates.menu"), 2 => "@EasyAdmin/default/menu.html.twig"));
        // line 105
        echo "
                    ";
        
        $__internal_e7e90d12c4d8812ec800e8f51597fbcedf27d3df75f27eb9abbe1a5a5b9f2815->leave($__internal_e7e90d12c4d8812ec800e8f51597fbcedf27d3df75f27eb9abbe1a5a5b9f2815_prof);

    }

    // line 112
    public function block_content($context, array $blocks = array())
    {
        $__internal_4ac99553ed33c77c3bf8067f26d221e001a3c9181f84d5f91f30aa4c2e6e1e1b = $this->env->getExtension("native_profiler");
        $__internal_4ac99553ed33c77c3bf8067f26d221e001a3c9181f84d5f91f30aa4c2e6e1e1b->enter($__internal_4ac99553ed33c77c3bf8067f26d221e001a3c9181f84d5f91f30aa4c2e6e1e1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 113
        echo "                ";
        $this->displayBlock('flash_messages', $context, $blocks);
        // line 116
        echo "
                <section class=\"content-header\">
                ";
        // line 118
        $this->displayBlock('content_header', $context, $blocks);
        // line 121
        echo "                ";
        $this->displayBlock('content_help', $context, $blocks);
        // line 130
        echo "                </section>

                <section id=\"main\" class=\"content\">
                    ";
        // line 133
        $this->displayBlock('main', $context, $blocks);
        // line 134
        echo "                </section>
            ";
        
        $__internal_4ac99553ed33c77c3bf8067f26d221e001a3c9181f84d5f91f30aa4c2e6e1e1b->leave($__internal_4ac99553ed33c77c3bf8067f26d221e001a3c9181f84d5f91f30aa4c2e6e1e1b_prof);

    }

    // line 113
    public function block_flash_messages($context, array $blocks = array())
    {
        $__internal_7bcef522ce403a05b8d7e46cf16de2def1786f15ea4b628a9ea1279974533a42 = $this->env->getExtension("native_profiler");
        $__internal_7bcef522ce403a05b8d7e46cf16de2def1786f15ea4b628a9ea1279974533a42->enter($__internal_7bcef522ce403a05b8d7e46cf16de2def1786f15ea4b628a9ea1279974533a42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "flash_messages"));

        // line 114
        echo "                    ";
        echo twig_include($this->env, $context, ((array_key_exists("_entity_config", $context)) ? ($this->getAttribute($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "templates", array()), "flash_messages", array())) : ("@EasyAdmin/default/flash_messages.html.twig")));
        echo "
                ";
        
        $__internal_7bcef522ce403a05b8d7e46cf16de2def1786f15ea4b628a9ea1279974533a42->leave($__internal_7bcef522ce403a05b8d7e46cf16de2def1786f15ea4b628a9ea1279974533a42_prof);

    }

    // line 118
    public function block_content_header($context, array $blocks = array())
    {
        $__internal_dccc661c2bfed0b39a119b1cad1d367547b4c414bd468060070e181b24161302 = $this->env->getExtension("native_profiler");
        $__internal_dccc661c2bfed0b39a119b1cad1d367547b4c414bd468060070e181b24161302->enter($__internal_dccc661c2bfed0b39a119b1cad1d367547b4c414bd468060070e181b24161302_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        // line 119
        echo "                    <h1 class=\"title\">";
        $this->displayBlock('content_title', $context, $blocks);
        echo "</h1>
                ";
        
        $__internal_dccc661c2bfed0b39a119b1cad1d367547b4c414bd468060070e181b24161302->leave($__internal_dccc661c2bfed0b39a119b1cad1d367547b4c414bd468060070e181b24161302_prof);

    }

    public function block_content_title($context, array $blocks = array())
    {
        $__internal_5a04fa697351c822229576d42ee03a803bada0c0962ce3f49867c85965cbbc4b = $this->env->getExtension("native_profiler");
        $__internal_5a04fa697351c822229576d42ee03a803bada0c0962ce3f49867c85965cbbc4b->enter($__internal_5a04fa697351c822229576d42ee03a803bada0c0962ce3f49867c85965cbbc4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        
        $__internal_5a04fa697351c822229576d42ee03a803bada0c0962ce3f49867c85965cbbc4b->leave($__internal_5a04fa697351c822229576d42ee03a803bada0c0962ce3f49867c85965cbbc4b_prof);

    }

    // line 121
    public function block_content_help($context, array $blocks = array())
    {
        $__internal_fd138034c8b5d100494c992f873020767b3619ab1b6bf0a5888d91e61166ade6 = $this->env->getExtension("native_profiler");
        $__internal_fd138034c8b5d100494c992f873020767b3619ab1b6bf0a5888d91e61166ade6->enter($__internal_fd138034c8b5d100494c992f873020767b3619ab1b6bf0a5888d91e61166ade6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_help"));

        // line 122
        echo "                    ";
        if ((array_key_exists("_entity_config", $context) && (($this->getAttribute($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : null), $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "action"), "method"), array(), "array", false, true), "help", array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : null), $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "action"), "method"), array(), "array", false, true), "help", array(), "array"), false)) : (false)))) {
            // line 123
            echo "                        <div class=\"box box-widget help-entity\">
                            <div class=\"box-body\">
                                ";
            // line 125
            echo $this->env->getExtension('translator')->trans($this->getAttribute($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "action"), "method"), array(), "array"), "help", array(), "array"));
            echo "
                            </div>
                        </div>
                    ";
        }
        // line 129
        echo "                ";
        
        $__internal_fd138034c8b5d100494c992f873020767b3619ab1b6bf0a5888d91e61166ade6->leave($__internal_fd138034c8b5d100494c992f873020767b3619ab1b6bf0a5888d91e61166ade6_prof);

    }

    // line 133
    public function block_main($context, array $blocks = array())
    {
        $__internal_f04ec4afb66b09db1ecd76eb3ce15dd5115e3ffaf8e86424cc9acc02f5658764 = $this->env->getExtension("native_profiler");
        $__internal_f04ec4afb66b09db1ecd76eb3ce15dd5115e3ffaf8e86424cc9acc02f5658764->enter($__internal_f04ec4afb66b09db1ecd76eb3ce15dd5115e3ffaf8e86424cc9acc02f5658764_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        
        $__internal_f04ec4afb66b09db1ecd76eb3ce15dd5115e3ffaf8e86424cc9acc02f5658764->leave($__internal_f04ec4afb66b09db1ecd76eb3ce15dd5115e3ffaf8e86424cc9acc02f5658764_prof);

    }

    // line 140
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_02d6e5a936f3fa9fc3068f7830a00d03e13ce439afcf3d8d5828fbc27c05e45d = $this->env->getExtension("native_profiler");
        $__internal_02d6e5a936f3fa9fc3068f7830a00d03e13ce439afcf3d8d5828fbc27c05e45d->enter($__internal_02d6e5a936f3fa9fc3068f7830a00d03e13ce439afcf3d8d5828fbc27c05e45d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        
        $__internal_02d6e5a936f3fa9fc3068f7830a00d03e13ce439afcf3d8d5828fbc27c05e45d->leave($__internal_02d6e5a936f3fa9fc3068f7830a00d03e13ce439afcf3d8d5828fbc27c05e45d_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  597 => 140,  586 => 133,  579 => 129,  572 => 125,  568 => 123,  565 => 122,  559 => 121,  539 => 119,  533 => 118,  523 => 114,  517 => 113,  509 => 134,  507 => 133,  502 => 130,  499 => 121,  497 => 118,  493 => 116,  490 => 113,  484 => 112,  476 => 105,  474 => 102,  472 => 101,  466 => 100,  458 => 107,  456 => 100,  453 => 99,  447 => 98,  440 => 88,  434 => 86,  428 => 84,  426 => 83,  420 => 81,  414 => 80,  405 => 89,  403 => 80,  399 => 78,  393 => 77,  383 => 71,  374 => 70,  368 => 69,  359 => 92,  357 => 77,  352 => 74,  350 => 69,  343 => 65,  339 => 63,  333 => 62,  325 => 136,  323 => 112,  318 => 109,  316 => 98,  311 => 95,  309 => 62,  306 => 61,  300 => 60,  279 => 58,  271 => 145,  262 => 143,  258 => 142,  255 => 141,  253 => 140,  249 => 138,  247 => 60,  237 => 58,  231 => 57,  213 => 30,  207 => 29,  198 => 43,  195 => 42,  192 => 29,  186 => 28,  174 => 25,  171 => 24,  165 => 23,  155 => 15,  149 => 13,  143 => 12,  131 => 10,  123 => 147,  121 => 57,  114 => 53,  110 => 52,  106 => 50,  101 => 48,  96 => 47,  94 => 46,  91 => 45,  89 => 28,  86 => 27,  84 => 23,  81 => 22,  72 => 20,  68 => 19,  65 => 18,  63 => 12,  58 => 10,  47 => 2,  44 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="{{ app.request.locale|split('_')|first|default('en') }}">*/
/*     <head>*/
/*         <meta charset="utf-8">*/
/*         <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*         <meta name="robots" content="noindex, nofollow, noarchive, nosnippet, noodp, noimageindex, notranslate, nocache" />*/
/*         <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">*/
/*         <meta name="generator" content="EasyAdmin" />*/
/* */
/*         <title>{% block page_title %}{{ block('content_title')|striptags|raw }}{% endblock %}</title>*/
/* */
/*         {% block head_stylesheets %}*/
/*             <link rel="stylesheet" href="{{ asset('bundles/easyadmin/stylesheet/easyadmin-all.min.css') }}">*/
/*             <style>*/
/*                 {{ easyadmin_config('_internal.custom_css')|raw }}*/
/*             </style>*/
/*         {% endblock %}*/
/* */
/*         {% for css_asset in easyadmin_config('design.assets.css') %}*/
/*             <link rel="stylesheet" href="{{ asset(css_asset) }}">*/
/*         {% endfor %}*/
/* */
/*         {% block head_favicon %}*/
/*             {% set favicon = easyadmin_config('design.assets.favicon') %}*/
/*             <link rel="icon" type="{{ favicon.mime_type }}" href="{{ asset(favicon.path) }}" />*/
/*         {% endblock %}*/
/* */
/*         {% block head_javascript %}*/
/*             {% block adminlte_options %}*/
/*                 <script type="text/javascript">*/
/*                     var AdminLTEOptions = {*/
/*                         animationSpeed: 'normal',*/
/*                         sidebarExpandOnHover: false,*/
/*                         enableBoxRefresh: false,*/
/*                         enableBSToppltip: false,*/
/*                         enableFastclick: false,*/
/*                         enableControlSidebar: false,*/
/*                         enableBoxWidget: false*/
/*                     };*/
/*                 </script>*/
/*             {% endblock %}*/
/* */
/*             <script src="{{ asset('bundles/easyadmin/javascript/easyadmin-all.min.js') }}"></script>*/
/*         {% endblock head_javascript %}*/
/* */
/*         {% if easyadmin_config('design.rtl') %}*/
/*             <link rel="stylesheet" href="{{ asset('bundles/easyadmin/stylesheet/bootstrap-rtl.min.css') }}">*/
/*             <link rel="stylesheet" href="{{ asset('bundles/easyadmin/stylesheet/adminlte-rtl.min.css') }}">*/
/*         {% endif %}*/
/* */
/*         <!--[if lt IE 9]>*/
/*             <script src="{{ asset('bundles/easyadmin/stylesheet/html5shiv.min.css') }}"></script>*/
/*             <script src="{{ asset('bundles/easyadmin/stylesheet/respond.min.css') }}"></script>*/
/*         <![endif]-->*/
/*     </head>*/
/* */
/*     {% block body %}*/
/*     <body id="{% block body_id %}{% endblock %}" class="easyadmin sidebar-mini {% block body_class %}{% endblock %} {{ app.request.cookies.has('_easyadmin_navigation_iscollapsed') ? 'sidebar-collapse' }}">*/
/*         <div class="wrapper">*/
/*         {% block wrapper %}*/
/*             <header class="main-header">*/
/*             {% block header %}*/
/*                 <nav class="navbar" role="navigation">*/
/*                     <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">*/
/*                         <span class="sr-only">{{ 'toggle_navigation'|trans(domain = 'EasyAdminBundle') }}</span>*/
/*                     </a>*/
/* */
/*                     <div id="header-logo">*/
/*                         {% block header_logo %}*/
/*                             <a class="logo {{ easyadmin_config('site_name')|length > 14 ? 'logo-long' }}" title="{{ easyadmin_config('site_name')|striptags }}" href="{{ path('easyadmin') }}">*/
/*                                 {{ easyadmin_config('site_name')|raw }}*/
/*                             </a>*/
/*                         {% endblock header_logo %}*/
/*                     </div>*/
/* */
/*                     <div class="navbar-custom-menu">*/
/*                     {% block header_custom_menu %}*/
/*                         <ul class="nav navbar-nav">*/
/*                             <li class="user user-menu">*/
/*                                 {% block user_menu %}*/
/*                                     <span class="sr-only">{{ 'user.logged_in_as'|trans(domain = 'EasyAdminBundle') }}</span>*/
/*                                     <i class="hidden-xs fa fa-user"></i>*/
/*                                     {% if app.user|default %}*/
/*                                         {{ app.user.username|default('user.unnamed'|trans(domain = 'EasyAdminBundle')) }}*/
/*                                     {% else %}*/
/*                                         {{ 'user.anonymous'|trans(domain = 'EasyAdminBundle') }}*/
/*                                     {% endif %}*/
/*                                 {% endblock user_menu %}*/
/*                             </li>*/
/*                         </ul>*/
/*                     {% endblock header_custom_menu %}*/
/*                     </div>*/
/*                 </nav>*/
/*             {% endblock header %}*/
/*             </header>*/
/* */
/*             <aside class="main-sidebar">*/
/*             {% block sidebar %}*/
/*                 <section class="sidebar">*/
/*                     {% block main_menu_wrapper %}*/
/*                         {{ include([*/
/*                             _entity_config is defined ? _entity_config.templates.menu,*/
/*                             easyadmin_config('design.templates.menu'),*/
/*                             '@EasyAdmin/default/menu.html.twig'*/
/*                         ]) }}*/
/*                     {% endblock main_menu_wrapper %}*/
/*                 </section>*/
/*             {% endblock sidebar %}*/
/*             </aside>*/
/* */
/*             <div class="content-wrapper">*/
/*             {% block content %}*/
/*                 {% block flash_messages %}*/
/*                     {{ include(_entity_config is defined ? _entity_config.templates.flash_messages : '@EasyAdmin/default/flash_messages.html.twig') }}*/
/*                 {% endblock flash_messages %}*/
/* */
/*                 <section class="content-header">*/
/*                 {% block content_header %}*/
/*                     <h1 class="title">{% block content_title %}{% endblock %}</h1>*/
/*                 {% endblock content_header %}*/
/*                 {% block content_help %}*/
/*                     {% if _entity_config is defined and _entity_config[app.request.query.get('action')]['help']|default(false) %}*/
/*                         <div class="box box-widget help-entity">*/
/*                             <div class="box-body">*/
/*                                 {{ _entity_config[app.request.query.get('action')]['help']|trans|raw }}*/
/*                             </div>*/
/*                         </div>*/
/*                     {% endif %}*/
/*                 {% endblock content_help %}*/
/*                 </section>*/
/* */
/*                 <section id="main" class="content">*/
/*                     {% block main %}{% endblock %}*/
/*                 </section>*/
/*             {% endblock content %}*/
/*             </div>*/
/*         {% endblock wrapper %}*/
/*         </div>*/
/* */
/*         {% block body_javascript %}{% endblock body_javascript %}*/
/* */
/*         {% for js_asset in easyadmin_config('design.assets.js') %}*/
/*             <script src="{{ asset(js_asset) }}"></script>*/
/*         {% endfor %}*/
/*     </body>*/
/*     {% endblock body %}*/
/* </html>*/
/* */
