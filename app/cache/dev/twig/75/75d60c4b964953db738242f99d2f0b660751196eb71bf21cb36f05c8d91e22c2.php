<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_104c74869b59ae4848dec17aa717f63a6c260ca4ee346e07000b56b0e9fb9de7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1b35243a7f639a6cd7fa2629e8f39329c3aea698818533df97d7f0e93df7ef54 = $this->env->getExtension("native_profiler");
        $__internal_1b35243a7f639a6cd7fa2629e8f39329c3aea698818533df97d7f0e93df7ef54->enter($__internal_1b35243a7f639a6cd7fa2629e8f39329c3aea698818533df97d7f0e93df7ef54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_1b35243a7f639a6cd7fa2629e8f39329c3aea698818533df97d7f0e93df7ef54->leave($__internal_1b35243a7f639a6cd7fa2629e8f39329c3aea698818533df97d7f0e93df7ef54_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
