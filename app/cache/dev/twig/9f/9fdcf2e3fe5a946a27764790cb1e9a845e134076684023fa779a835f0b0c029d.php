<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_5f89dbe61367d4b028f1fd251b911279676043340f14e29be03a133b9847afc9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_271afdff43c5def681e205b5ee674dff5e1f6d6ca44962b3dcf1cbc8953443f8 = $this->env->getExtension("native_profiler");
        $__internal_271afdff43c5def681e205b5ee674dff5e1f6d6ca44962b3dcf1cbc8953443f8->enter($__internal_271afdff43c5def681e205b5ee674dff5e1f6d6ca44962b3dcf1cbc8953443f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_271afdff43c5def681e205b5ee674dff5e1f6d6ca44962b3dcf1cbc8953443f8->leave($__internal_271afdff43c5def681e205b5ee674dff5e1f6d6ca44962b3dcf1cbc8953443f8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->label($form) ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
