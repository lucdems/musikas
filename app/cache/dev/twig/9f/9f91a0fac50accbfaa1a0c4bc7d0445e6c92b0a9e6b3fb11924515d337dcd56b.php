<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_018454dcba14c981a1a1115e5dd82087c3536c75cc26f3f7f8449b73c4fb5c66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21914bba643475007cbc11b5b87fcddf38e52c800f820c4781efac4e32771d00 = $this->env->getExtension("native_profiler");
        $__internal_21914bba643475007cbc11b5b87fcddf38e52c800f820c4781efac4e32771d00->enter($__internal_21914bba643475007cbc11b5b87fcddf38e52c800f820c4781efac4e32771d00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_21914bba643475007cbc11b5b87fcddf38e52c800f820c4781efac4e32771d00->leave($__internal_21914bba643475007cbc11b5b87fcddf38e52c800f820c4781efac4e32771d00_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <textarea <?php echo $view['form']->block($form, 'widget_attributes') ?>><?php echo $view->escape($value) ?></textarea>*/
/* */
