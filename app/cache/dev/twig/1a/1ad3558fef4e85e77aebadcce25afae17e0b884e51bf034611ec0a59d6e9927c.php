<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_469d491d7a77c0a0ec2112576349ddd84cc2a3e5e3e6e1ea376ac46713aac55f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_666b095502fb0e5b1cc32a05a7e8ed6f7325afbcbefc602f12e53ef4f6e7a698 = $this->env->getExtension("native_profiler");
        $__internal_666b095502fb0e5b1cc32a05a7e8ed6f7325afbcbefc602f12e53ef4f6e7a698->enter($__internal_666b095502fb0e5b1cc32a05a7e8ed6f7325afbcbefc602f12e53ef4f6e7a698_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_666b095502fb0e5b1cc32a05a7e8ed6f7325afbcbefc602f12e53ef4f6e7a698->leave($__internal_666b095502fb0e5b1cc32a05a7e8ed6f7325afbcbefc602f12e53ef4f6e7a698_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
