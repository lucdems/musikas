<?php

/* :default:barre.html.twig */
class __TwigTemplate_1611896137ae3cd7e14b90a4c99e5b695f4132b5f169bece0b1027dbd377272a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b9c1c5b3de7e618b1d87e7ba81ad5666684adc856e045ab280a2452f2899232f = $this->env->getExtension("native_profiler");
        $__internal_b9c1c5b3de7e618b1d87e7ba81ad5666684adc856e045ab280a2452f2899232f->enter($__internal_b9c1c5b3de7e618b1d87e7ba81ad5666684adc856e045ab280a2452f2899232f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":default:barre.html.twig"));

        // line 1
        echo "<!DOCTYPE html>

<div class=\"col-md-4 item\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <div id=\"sidebar-wrapper\">
                    <ul class=\"sidebar-nav\">
                        <li class=\"sidebar-brand\"><a href=\"#\">Menu</a></li>
                        <li><a href=\"#\">Documents</a></li>
                        <li><a href=\"#\">Inscription</a></li>
                        <li><a href=\"#\">Profil</a></li>
                    </ul>
                </div>
                </div>
                
            </div>
            
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div>
                        <div class=\"panel panel-default\">
                            <div class=\"panel-body\">
                                <div class=\"media\">
                                    <div class=\"media-left\">
                                        <a><img src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "image", array())), "html", null, true);
        echo "\" class=\"img-circle\" style=\"height:50px; width:50px;\" /></a>
                                    </div>
                                    <div class=\"media-body\">
                                        <ul class=\"list-unstyled fa-ul\">
                                            <li><i class=\"fa fa-user fa-li\"></i><a href=\"#\">";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
        echo "</a></li>
                                            <li><i class=\"fa fa-envelope fa-li\"></i><a href=\"#\">james.doe@gmail.com</a></li>
                                            <li><i class=\"fa fa-phone fa-li\"></i>(555) 555-5555</li>
                                        </ul>
                                    </div>
                                </div>
                                <hr />
                <!--
                                <div><small><strong>Remplissage du profil</strong><i class=\"fa fa-info-circle text-primary\"></i></small>
                                    <div class=\"progress progress-high\">
                                        <div class=\"progress-bar progress-bar-success\" style=\"width:90%;\"><span class=\"sr-only\">90%</span></div>
                                </div>
                            </div>
                            <h6 class=\"text-uppercase\"><strong>Messages reçus:</strong></h6>
                            <div class=\"media\">
                                <div class=\"media-left\"><a href=\"&quot;#&quot;\"><span class=\"fa-stack fa-lg\"><i class=\"fa fa-circle fa-stack-2x text-primary\"></i><i class=\"fa fa-inbox fa-stack-1x fa-inverse\"></i></span></a></div>
                                <div class=\"media-body\">
                                    <p><a href=\"#\"><strong>Gala</strong><br /></a><small><em>May 12, 2016</em><br /><strong>Invitation:</strong>1 General Admission: \$25</small></p>
                                </div>
                            </div>
                            <div><a class=\"btn btn-link btn-sm text-uppercase btn-text\" data-toggle=\"collapse\" href=\"#collapse-1\"><strong>Also Attended</strong><i class=\"fa fa-chevron-down fa-fw\"></i></a>
                                <div class=\"collapse\" id=\"collapse-1\">
                                    <div class=\"media\">
                                        <div class=\"media-left\"><a href=\"&quot;#&quot;\"><span class=\"fa-stack fa-lg\"><i class=\"fa fa-circle fa-stack-2x text-primary\"></i><i class=\"fa fa-ticket fa-stack-1x fa-inverse\"></i></span></a></div>
                                        <div class=\"media-body\">
                                            <p><a href=\"#\"><strong>Fall Gala</strong><br /></a><small><em>October 12, 2015</em><br /><strong>Purchased:</strong>1 General Admission: \$25</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                -->
                            <div class=\"row\">
                                <div class=\"col-md-4 text-center\"><span class=\"fa-stack fa-lg\"><i class=\"fa fa-circle fa-stack-2x text-primary\"></i><i class=\"fa fa-file-text-o fa-stack-1x fa-inverse\"></i></span>
                                    <p><a href=\"#\">Documents</a></p>
                                </div>
                                <div class=\"col-md-4 text-center\"><span class=\"fa-stack fa-lg\"><i class=\"fa fa-circle fa-stack-2x text-primary\"></i><i class=\"fa fa-align-left fa-stack-1x fa-inverse\"></i></span>
                                    <p><a href=\"#\">Mon compte</a></p>
                                </div>
                                <div class=\"col-md-4 text-center\"><span class=\"fa-stack fa-lg\"><i class=\"fa fa-circle fa-stack-2x text-primary\"></i><i class=\"fa fa-sign-out fa-stack-1x fa-inverse\"></i></span>
                                    <p><a href=\"";
        // line 71
        echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
        echo "\">Déconnexion</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>";
        
        $__internal_b9c1c5b3de7e618b1d87e7ba81ad5666684adc856e045ab280a2452f2899232f->leave($__internal_b9c1c5b3de7e618b1d87e7ba81ad5666684adc856e045ab280a2452f2899232f_prof);

    }

    public function getTemplateName()
    {
        return ":default:barre.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 71,  57 => 31,  50 => 27,  22 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* */
/* <div class="col-md-4 item">*/
/*     <div class="row">*/
/*         <div class="col-md-12">*/
/*             <div class="panel panel-default">*/
/*                 <div class="panel-body">*/
/*                     <div id="sidebar-wrapper">*/
/*                     <ul class="sidebar-nav">*/
/*                         <li class="sidebar-brand"><a href="#">Menu</a></li>*/
/*                         <li><a href="#">Documents</a></li>*/
/*                         <li><a href="#">Inscription</a></li>*/
/*                         <li><a href="#">Profil</a></li>*/
/*                     </ul>*/
/*                 </div>*/
/*                 </div>*/
/*                 */
/*             </div>*/
/*             */
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <div>*/
/*                         <div class="panel panel-default">*/
/*                             <div class="panel-body">*/
/*                                 <div class="media">*/
/*                                     <div class="media-left">*/
/*                                         <a><img src="{{ asset(app.user.image) }}" class="img-circle" style="height:50px; width:50px;" /></a>*/
/*                                     </div>*/
/*                                     <div class="media-body">*/
/*                                         <ul class="list-unstyled fa-ul">*/
/*                                             <li><i class="fa fa-user fa-li"></i><a href="#">{{ app.user.username }}</a></li>*/
/*                                             <li><i class="fa fa-envelope fa-li"></i><a href="#">james.doe@gmail.com</a></li>*/
/*                                             <li><i class="fa fa-phone fa-li"></i>(555) 555-5555</li>*/
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <hr />*/
/*                 <!--*/
/*                                 <div><small><strong>Remplissage du profil</strong><i class="fa fa-info-circle text-primary"></i></small>*/
/*                                     <div class="progress progress-high">*/
/*                                         <div class="progress-bar progress-bar-success" style="width:90%;"><span class="sr-only">90%</span></div>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <h6 class="text-uppercase"><strong>Messages reçus:</strong></h6>*/
/*                             <div class="media">*/
/*                                 <div class="media-left"><a href="&quot;#&quot;"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-inbox fa-stack-1x fa-inverse"></i></span></a></div>*/
/*                                 <div class="media-body">*/
/*                                     <p><a href="#"><strong>Gala</strong><br /></a><small><em>May 12, 2016</em><br /><strong>Invitation:</strong>1 General Admission: $25</small></p>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div><a class="btn btn-link btn-sm text-uppercase btn-text" data-toggle="collapse" href="#collapse-1"><strong>Also Attended</strong><i class="fa fa-chevron-down fa-fw"></i></a>*/
/*                                 <div class="collapse" id="collapse-1">*/
/*                                     <div class="media">*/
/*                                         <div class="media-left"><a href="&quot;#&quot;"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-ticket fa-stack-1x fa-inverse"></i></span></a></div>*/
/*                                         <div class="media-body">*/
/*                                             <p><a href="#"><strong>Fall Gala</strong><br /></a><small><em>October 12, 2015</em><br /><strong>Purchased:</strong>1 General Admission: $25</small></p>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <hr />*/
/*                 -->*/
/*                             <div class="row">*/
/*                                 <div class="col-md-4 text-center"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-file-text-o fa-stack-1x fa-inverse"></i></span>*/
/*                                     <p><a href="#">Documents</a></p>*/
/*                                 </div>*/
/*                                 <div class="col-md-4 text-center"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-align-left fa-stack-1x fa-inverse"></i></span>*/
/*                                     <p><a href="#">Mon compte</a></p>*/
/*                                 </div>*/
/*                                 <div class="col-md-4 text-center"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-sign-out fa-stack-1x fa-inverse"></i></span>*/
/*                                     <p><a href="{{ path('fos_user_security_logout') }}">Déconnexion</a></p>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* </div>*/
