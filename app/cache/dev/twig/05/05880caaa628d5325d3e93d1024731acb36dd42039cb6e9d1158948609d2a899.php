<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_8dd2746340e714bd5e14a6a9ed469a5a9c35ac74d0301243b7e6dd0ae1cfba4a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ea7e2f16bda11cc886f9aa9e832c38d20fe09e23309882e10d9e53a155146e0a = $this->env->getExtension("native_profiler");
        $__internal_ea7e2f16bda11cc886f9aa9e832c38d20fe09e23309882e10d9e53a155146e0a->enter($__internal_ea7e2f16bda11cc886f9aa9e832c38d20fe09e23309882e10d9e53a155146e0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ea7e2f16bda11cc886f9aa9e832c38d20fe09e23309882e10d9e53a155146e0a->leave($__internal_ea7e2f16bda11cc886f9aa9e832c38d20fe09e23309882e10d9e53a155146e0a_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_a4fa3b8f58b96a0e625228e13dcc6c8675d49821264ed8695132680c40d45a6d = $this->env->getExtension("native_profiler");
        $__internal_a4fa3b8f58b96a0e625228e13dcc6c8675d49821264ed8695132680c40d45a6d->enter($__internal_a4fa3b8f58b96a0e625228e13dcc6c8675d49821264ed8695132680c40d45a6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_a4fa3b8f58b96a0e625228e13dcc6c8675d49821264ed8695132680c40d45a6d->leave($__internal_a4fa3b8f58b96a0e625228e13dcc6c8675d49821264ed8695132680c40d45a6d_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_d8e71651c055528a3aa73b381ec4e07cce393cc2f7ba655d30db36063335aaa5 = $this->env->getExtension("native_profiler");
        $__internal_d8e71651c055528a3aa73b381ec4e07cce393cc2f7ba655d30db36063335aaa5->enter($__internal_d8e71651c055528a3aa73b381ec4e07cce393cc2f7ba655d30db36063335aaa5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_d8e71651c055528a3aa73b381ec4e07cce393cc2f7ba655d30db36063335aaa5->leave($__internal_d8e71651c055528a3aa73b381ec4e07cce393cc2f7ba655d30db36063335aaa5_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
