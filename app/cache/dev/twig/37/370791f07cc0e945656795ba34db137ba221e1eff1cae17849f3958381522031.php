<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_9888a0ebb1067d77d8df44042ef63ceb0b70a68ca3e97a0a8638dea47804a74c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8d501ab8694c1d7c4564c02e8031ef5f04322c74db1e18e6083e87791ddb4db3 = $this->env->getExtension("native_profiler");
        $__internal_8d501ab8694c1d7c4564c02e8031ef5f04322c74db1e18e6083e87791ddb4db3->enter($__internal_8d501ab8694c1d7c4564c02e8031ef5f04322c74db1e18e6083e87791ddb4db3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_8d501ab8694c1d7c4564c02e8031ef5f04322c74db1e18e6083e87791ddb4db3->leave($__internal_8d501ab8694c1d7c4564c02e8031ef5f04322c74db1e18e6083e87791ddb4db3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'range'));*/
/* */
