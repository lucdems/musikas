<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_ad4ece65903f12eaceacf929082f8a4e79d3da96ec4d71efc8694036fa202484 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f60bcedb4f80a5d2a370f12e12ae9c63db1d5e97262432ecc680e206b3ad45be = $this->env->getExtension("native_profiler");
        $__internal_f60bcedb4f80a5d2a370f12e12ae9c63db1d5e97262432ecc680e206b3ad45be->enter($__internal_f60bcedb4f80a5d2a370f12e12ae9c63db1d5e97262432ecc680e206b3ad45be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f60bcedb4f80a5d2a370f12e12ae9c63db1d5e97262432ecc680e206b3ad45be->leave($__internal_f60bcedb4f80a5d2a370f12e12ae9c63db1d5e97262432ecc680e206b3ad45be_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_21988d20b76af5b4fbfe31b66d909adae27d5f341e1621d1de8b3c0c4dc57b0b = $this->env->getExtension("native_profiler");
        $__internal_21988d20b76af5b4fbfe31b66d909adae27d5f341e1621d1de8b3c0c4dc57b0b->enter($__internal_21988d20b76af5b4fbfe31b66d909adae27d5f341e1621d1de8b3c0c4dc57b0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_21988d20b76af5b4fbfe31b66d909adae27d5f341e1621d1de8b3c0c4dc57b0b->leave($__internal_21988d20b76af5b4fbfe31b66d909adae27d5f341e1621d1de8b3c0c4dc57b0b_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "@FOSUser/layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "@FOSUser/Profile/show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
