<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_86abfe826f075fba695afa581f877ecd73c7e36c6980653bbb6d742a7438fb80 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_299bf40500601e1f466cbc81a3e3b1bd5cc38f9c5a018c0b50896ffe9aad4858 = $this->env->getExtension("native_profiler");
        $__internal_299bf40500601e1f466cbc81a3e3b1bd5cc38f9c5a018c0b50896ffe9aad4858->enter($__internal_299bf40500601e1f466cbc81a3e3b1bd5cc38f9c5a018c0b50896ffe9aad4858_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_299bf40500601e1f466cbc81a3e3b1bd5cc38f9c5a018c0b50896ffe9aad4858->leave($__internal_299bf40500601e1f466cbc81a3e3b1bd5cc38f9c5a018c0b50896ffe9aad4858_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($compound): ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_compound')?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_simple')?>*/
/* <?php endif ?>*/
/* */
