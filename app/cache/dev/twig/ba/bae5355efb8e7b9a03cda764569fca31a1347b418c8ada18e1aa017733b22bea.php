<?php

/* @EasyAdmin/default/field_datetime.html.twig */
class __TwigTemplate_ebf72f7185ecb701b7a9ae0e9c4c0221f917843caca8a99ac98888954fb92f06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b7be6836767735b05af08d07cf8a5cda6bba80e4eba8bb9a9a606b8d51b906c8 = $this->env->getExtension("native_profiler");
        $__internal_b7be6836767735b05af08d07cf8a5cda6bba80e4eba8bb9a9a606b8d51b906c8->enter($__internal_b7be6836767735b05af08d07cf8a5cda6bba80e4eba8bb9a9a606b8d51b906c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/field_datetime.html.twig"));

        // line 1
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), $this->getAttribute((isset($context["field_options"]) ? $context["field_options"] : $this->getContext($context, "field_options")), "format", array())), "html", null, true);
        echo "
";
        
        $__internal_b7be6836767735b05af08d07cf8a5cda6bba80e4eba8bb9a9a606b8d51b906c8->leave($__internal_b7be6836767735b05af08d07cf8a5cda6bba80e4eba8bb9a9a606b8d51b906c8_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/field_datetime.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ value|date(field_options.format) }}*/
/* */
