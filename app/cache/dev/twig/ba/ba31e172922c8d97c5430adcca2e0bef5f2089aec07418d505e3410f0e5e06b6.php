<?php

/* EasyAdminBundle:default:field_date.html.twig */
class __TwigTemplate_05c8c96cde0ca48f2d9559f21b8c683ba51582dee5f9294276ad9aaae50ce622 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1ac0ed590f7209b2fa6c9e81633148530c9f29becec7817946bfbb9aa3205ebb = $this->env->getExtension("native_profiler");
        $__internal_1ac0ed590f7209b2fa6c9e81633148530c9f29becec7817946bfbb9aa3205ebb->enter($__internal_1ac0ed590f7209b2fa6c9e81633148530c9f29becec7817946bfbb9aa3205ebb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_date.html.twig"));

        // line 1
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), $this->getAttribute((isset($context["field_options"]) ? $context["field_options"] : $this->getContext($context, "field_options")), "format", array())), "html", null, true);
        echo "
";
        
        $__internal_1ac0ed590f7209b2fa6c9e81633148530c9f29becec7817946bfbb9aa3205ebb->leave($__internal_1ac0ed590f7209b2fa6c9e81633148530c9f29becec7817946bfbb9aa3205ebb_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_date.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ value|date(field_options.format) }}*/
/* */
