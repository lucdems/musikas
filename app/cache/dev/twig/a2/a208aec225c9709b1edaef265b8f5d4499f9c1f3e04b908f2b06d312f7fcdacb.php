<?php

/* EasyAdminBundle:default:field_time.html.twig */
class __TwigTemplate_9ddd254edc4a926a410278dc15f7fbc60f9dc261f7c35fb69045eebf1b53a425 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_339c25bbb7ec7073018b0213ce515d2e1d18059a9ff3bf8d8dcf15411f5e785b = $this->env->getExtension("native_profiler");
        $__internal_339c25bbb7ec7073018b0213ce515d2e1d18059a9ff3bf8d8dcf15411f5e785b->enter($__internal_339c25bbb7ec7073018b0213ce515d2e1d18059a9ff3bf8d8dcf15411f5e785b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_time.html.twig"));

        // line 1
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), $this->getAttribute((isset($context["field_options"]) ? $context["field_options"] : $this->getContext($context, "field_options")), "format", array())), "html", null, true);
        echo "
";
        
        $__internal_339c25bbb7ec7073018b0213ce515d2e1d18059a9ff3bf8d8dcf15411f5e785b->leave($__internal_339c25bbb7ec7073018b0213ce515d2e1d18059a9ff3bf8d8dcf15411f5e785b_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_time.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ value|date(field_options.format) }}*/
/* */
