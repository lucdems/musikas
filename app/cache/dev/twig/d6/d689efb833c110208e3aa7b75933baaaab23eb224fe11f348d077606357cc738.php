<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_f2bcb37c77fff2b221db05a2edc757b2783b399f3d9bb8ea6b4d974ea095e92c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3962012e220ced36e74141c139e4c704bbd51223c5c09e48e1ddb9e1dbb546ae = $this->env->getExtension("native_profiler");
        $__internal_3962012e220ced36e74141c139e4c704bbd51223c5c09e48e1ddb9e1dbb546ae->enter($__internal_3962012e220ced36e74141c139e4c704bbd51223c5c09e48e1ddb9e1dbb546ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_3962012e220ced36e74141c139e4c704bbd51223c5c09e48e1ddb9e1dbb546ae->leave($__internal_3962012e220ced36e74141c139e4c704bbd51223c5c09e48e1ddb9e1dbb546ae_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/* */
