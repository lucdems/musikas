<?php

/* musikasvitrineBundle:Default:equipe_prof.html.twig */
class __TwigTemplate_2dccdf16ccaf3a0ff6a79ed24e197dcbb436761b841a63f66cb77734783c4507 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMere.html.twig", "musikasvitrineBundle:Default:equipe_prof.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMere.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca244b2b96b7f601f2dbbd33ffd6e215a4fe1cf77d854e911a0a30e18d8a0d77 = $this->env->getExtension("native_profiler");
        $__internal_ca244b2b96b7f601f2dbbd33ffd6e215a4fe1cf77d854e911a0a30e18d8a0d77->enter($__internal_ca244b2b96b7f601f2dbbd33ffd6e215a4fe1cf77d854e911a0a30e18d8a0d77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasvitrineBundle:Default:equipe_prof.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ca244b2b96b7f601f2dbbd33ffd6e215a4fe1cf77d854e911a0a30e18d8a0d77->leave($__internal_ca244b2b96b7f601f2dbbd33ffd6e215a4fe1cf77d854e911a0a30e18d8a0d77_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_2cf2f2258feb2693019ad839c9ec7d45a79e2c9fd79467f509c36ede6249e681 = $this->env->getExtension("native_profiler");
        $__internal_2cf2f2258feb2693019ad839c9ec7d45a79e2c9fd79467f509c36ede6249e681->enter($__internal_2cf2f2258feb2693019ad839c9ec7d45a79e2c9fd79467f509c36ede6249e681_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <section>
        <div class=\"article-list\">
        <div class=\"container\" id=\"contenuenseignement\">
            
            
            <div class=\"intro\">
                <h1 class=\"text-center\">Page de : ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["prof"]) ? $context["prof"] : $this->getContext($context, "prof")), "username", array()), "html", null, true);
        echo " ~ ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["prof"]) ? $context["prof"] : $this->getContext($context, "prof")), "nom", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["prof"]) ? $context["prof"] : $this->getContext($context, "prof")), "prenom", array()), "html", null, true);
        echo "</h1>
            </div>
            
            <div class=\"row articles\" id=\"imagesequipe\">
                <div class=\"col-md-12\">
                    <a href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["prof"]) ? $context["prof"] : $this->getContext($context, "prof")), "image", array())), "html", null, true);
        echo "\"></a>
                    <p class=\"text-center\"></br></br>Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p>

                </div>
            </div>
            
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <a class=\"btn btn-default\" type=\"button\" id=\"btn_retour\" href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_equipe");
        echo "\">Retour à l'équipe</a>
                </div>
            </div>
            
            <div class=\"container\" id=\"contenuenseignement\">
                <h2 class=\"text-center\">Disciplines enseignées :</h2>
            </div>
            
        <div class=\"row articles\" id=\"imagesenseignements\">
        ";
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tabEnseignement"]) ? $context["tabEnseignement"] : $this->getContext($context, "tabEnseignement")));
        foreach ($context['_seq'] as $context["_key"] => $context["enseignement"]) {
            // line 33
            echo "                
                <div class=\"col-md-4 col-sm-6 item\">
                    <a href=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("musikasvitrine_enseignement", array("id" => $this->getAttribute($context["enseignement"], "id", array()))), "html", null, true);
            echo "\"><img class=\"img-responsive\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["enseignement"], "image", array())), "html", null, true);
            echo "\"></a>
                    <h3 class=\"name\">";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["enseignement"], "nom", array()), "html", null, true);
            echo "</h3>
                    <p class=\"text-justify description\"> ";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["enseignement"], "description", array()), "html", null, true);
            echo "</p><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("musikasvitrine_enseignement", array("id" => $this->getAttribute($context["enseignement"], "id", array()))), "html", null, true);
            echo "\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a>
                </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enseignement'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "        </div>
        
        </div>
        </div>
    </section>
";
        
        $__internal_2cf2f2258feb2693019ad839c9ec7d45a79e2c9fd79467f509c36ede6249e681->leave($__internal_2cf2f2258feb2693019ad839c9ec7d45a79e2c9fd79467f509c36ede6249e681_prof);

    }

    public function getTemplateName()
    {
        return "musikasvitrineBundle:Default:equipe_prof.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 40,  101 => 37,  97 => 36,  91 => 35,  87 => 33,  83 => 32,  71 => 23,  60 => 15,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMere.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <section>*/
/*         <div class="article-list">*/
/*         <div class="container" id="contenuenseignement">*/
/*             */
/*             */
/*             <div class="intro">*/
/*                 <h1 class="text-center">Page de : {{prof.username}} ~ {{prof.nom}} {{prof.prenom}}</h1>*/
/*             </div>*/
/*             */
/*             <div class="row articles" id="imagesequipe">*/
/*                 <div class="col-md-12">*/
/*                     <a href="#"><img class="img-responsive" src="{{ asset(prof.image) }}"></a>*/
/*                     <p class="text-center"></br></br>Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p>*/
/* */
/*                 </div>*/
/*             </div>*/
/*             */
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <a class="btn btn-default" type="button" id="btn_retour" href="{{ path('musikasvitrine_equipe') }}">Retour à l'équipe</a>*/
/*                 </div>*/
/*             </div>*/
/*             */
/*             <div class="container" id="contenuenseignement">*/
/*                 <h2 class="text-center">Disciplines enseignées :</h2>*/
/*             </div>*/
/*             */
/*         <div class="row articles" id="imagesenseignements">*/
/*         {% for enseignement in tabEnseignement %}*/
/*                 */
/*                 <div class="col-md-4 col-sm-6 item">*/
/*                     <a href="{{ path('musikasvitrine_enseignement', { 'id': enseignement.id }) }}"><img class="img-responsive" src="{{ asset(enseignement.image)}}"></a>*/
/*                     <h3 class="name">{{ enseignement.nom }}</h3>*/
/*                     <p class="text-justify description"> {{ enseignement.description }}</p><a href="{{ path('musikasvitrine_enseignement', { 'id': enseignement.id }) }}" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a>*/
/*                 </div>*/
/*         {% endfor %}*/
/*         </div>*/
/*         */
/*         </div>*/
/*         </div>*/
/*     </section>*/
/* {% endblock %}*/
