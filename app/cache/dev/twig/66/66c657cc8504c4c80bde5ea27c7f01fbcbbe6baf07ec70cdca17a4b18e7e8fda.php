<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_12243108cbd110b0956420ebda27b0725814c0de384463c8511d53de270316dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_05f3f2999f337e02d6c35d109fdc0bc30a0f1f2e0b206ae239cd76c8b394ba59 = $this->env->getExtension("native_profiler");
        $__internal_05f3f2999f337e02d6c35d109fdc0bc30a0f1f2e0b206ae239cd76c8b394ba59->enter($__internal_05f3f2999f337e02d6c35d109fdc0bc30a0f1f2e0b206ae239cd76c8b394ba59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_05f3f2999f337e02d6c35d109fdc0bc30a0f1f2e0b206ae239cd76c8b394ba59->leave($__internal_05f3f2999f337e02d6c35d109fdc0bc30a0f1f2e0b206ae239cd76c8b394ba59_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (count($errors) > 0): ?>*/
/*     <ul>*/
/*         <?php foreach ($errors as $error): ?>*/
/*             <li><?php echo $error->getMessage() ?></li>*/
/*         <?php endforeach; ?>*/
/*     </ul>*/
/* <?php endif ?>*/
/* */
