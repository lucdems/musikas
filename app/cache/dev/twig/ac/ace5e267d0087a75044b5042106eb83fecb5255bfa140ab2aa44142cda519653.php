<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_a78229226a10e292330230b327c69db4950ce44b2d164b5f1fd761d95ba483d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_59ffb7ead1bfe0a43021e5a32c3eb4fae9c4c595259c49cafe6f36e75ebf4897 = $this->env->getExtension("native_profiler");
        $__internal_59ffb7ead1bfe0a43021e5a32c3eb4fae9c4c595259c49cafe6f36e75ebf4897->enter($__internal_59ffb7ead1bfe0a43021e5a32c3eb4fae9c4c595259c49cafe6f36e75ebf4897_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_59ffb7ead1bfe0a43021e5a32c3eb4fae9c4c595259c49cafe6f36e75ebf4897->leave($__internal_59ffb7ead1bfe0a43021e5a32c3eb4fae9c4c595259c49cafe6f36e75ebf4897_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}*/
/* */
