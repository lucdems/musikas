<?php

/* @EasyAdmin/default/new.html.twig */
class __TwigTemplate_c5c50d70ad879ccbf9d0f9973cfe87024feee8911a7cc91883b4dc2f7b4fd253 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'content_title' => array($this, 'block_content_title'),
            'main' => array($this, 'block_main'),
            'entity_form' => array($this, 'block_entity_form'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 7
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "templates", array()), "layout", array()), "@EasyAdmin/default/new.html.twig", 7);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4da7df8b725ead1bcbe5142836d3ad74c44b827c474ef9dabc78ffc3186cd3da = $this->env->getExtension("native_profiler");
        $__internal_4da7df8b725ead1bcbe5142836d3ad74c44b827c474ef9dabc78ffc3186cd3da->enter($__internal_4da7df8b725ead1bcbe5142836d3ad74c44b827c474ef9dabc78ffc3186cd3da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/new.html.twig"));

        // line 1
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), $this->env->getExtension('easyadmin_extension')->getBackendConfiguration("design.form_theme"));
        // line 3
        $context["_entity_config"] = $this->env->getExtension('easyadmin_extension')->getEntityConfiguration($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "entity"), "method"));
        // line 4
        $context["__internal_7b65e151a81bd23bb428f58e1452845a8d9286496fcf562ceb55af5b5f9cdb7e"] = $this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "translation_domain", array());
        // line 5
        $context["_trans_parameters"] = array("%entity_name%" => $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "name", array()), array(),         // line 4
(isset($context["__internal_7b65e151a81bd23bb428f58e1452845a8d9286496fcf562ceb55af5b5f9cdb7e"]) ? $context["__internal_7b65e151a81bd23bb428f58e1452845a8d9286496fcf562ceb55af5b5f9cdb7e"] : $this->getContext($context, "__internal_7b65e151a81bd23bb428f58e1452845a8d9286496fcf562ceb55af5b5f9cdb7e"))), "%entity_label%" => $this->env->getExtension('translator')->trans($this->getAttribute(        // line 5
(isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "label", array()), array(),         // line 4
(isset($context["__internal_7b65e151a81bd23bb428f58e1452845a8d9286496fcf562ceb55af5b5f9cdb7e"]) ? $context["__internal_7b65e151a81bd23bb428f58e1452845a8d9286496fcf562ceb55af5b5f9cdb7e"] : $this->getContext($context, "__internal_7b65e151a81bd23bb428f58e1452845a8d9286496fcf562ceb55af5b5f9cdb7e"))));
        // line 7
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4da7df8b725ead1bcbe5142836d3ad74c44b827c474ef9dabc78ffc3186cd3da->leave($__internal_4da7df8b725ead1bcbe5142836d3ad74c44b827c474ef9dabc78ffc3186cd3da_prof);

    }

    // line 9
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_ab0674c9d2429b7138f46107e0884f169d50ced3acacd295707c0f767e30b568 = $this->env->getExtension("native_profiler");
        $__internal_ab0674c9d2429b7138f46107e0884f169d50ced3acacd295707c0f767e30b568->enter($__internal_ab0674c9d2429b7138f46107e0884f169d50ced3acacd295707c0f767e30b568_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo twig_escape_filter($this->env, ("easyadmin-new-" . $this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "name", array())), "html", null, true);
        
        $__internal_ab0674c9d2429b7138f46107e0884f169d50ced3acacd295707c0f767e30b568->leave($__internal_ab0674c9d2429b7138f46107e0884f169d50ced3acacd295707c0f767e30b568_prof);

    }

    // line 10
    public function block_body_class($context, array $blocks = array())
    {
        $__internal_61efc9dd3f1e9aea91cd0676a44654aafa9ceb3ab1d86d8916056e8354141920 = $this->env->getExtension("native_profiler");
        $__internal_61efc9dd3f1e9aea91cd0676a44654aafa9ceb3ab1d86d8916056e8354141920->enter($__internal_61efc9dd3f1e9aea91cd0676a44654aafa9ceb3ab1d86d8916056e8354141920_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        echo twig_escape_filter($this->env, ("new new-" . twig_lower_filter($this->env, $this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "name", array()))), "html", null, true);
        
        $__internal_61efc9dd3f1e9aea91cd0676a44654aafa9ceb3ab1d86d8916056e8354141920->leave($__internal_61efc9dd3f1e9aea91cd0676a44654aafa9ceb3ab1d86d8916056e8354141920_prof);

    }

    // line 12
    public function block_content_title($context, array $blocks = array())
    {
        $__internal_e3b65773481a3397d36c64a6ffa5689c025bf23cdbfb36d838302daa66afce40 = $this->env->getExtension("native_profiler");
        $__internal_e3b65773481a3397d36c64a6ffa5689c025bf23cdbfb36d838302daa66afce40->enter($__internal_e3b65773481a3397d36c64a6ffa5689c025bf23cdbfb36d838302daa66afce40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        // line 13
        ob_start();
        // line 14
        echo "    ";
        $context["_default_title"] = $this->env->getExtension('translator')->trans("new.page_title", (isset($context["_trans_parameters"]) ? $context["_trans_parameters"] : $this->getContext($context, "_trans_parameters")), "EasyAdminBundle");
        // line 15
        echo "    ";
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : null), "new", array(), "any", false, true), "title", array(), "any", true, true)) ? ($this->env->getExtension('translator')->trans($this->getAttribute($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : $this->getContext($context, "_entity_config")), "new", array()), "title", array()), (isset($context["_trans_parameters"]) ? $context["_trans_parameters"] : $this->getContext($context, "_trans_parameters")),         // line 4
(isset($context["__internal_7b65e151a81bd23bb428f58e1452845a8d9286496fcf562ceb55af5b5f9cdb7e"]) ? $context["__internal_7b65e151a81bd23bb428f58e1452845a8d9286496fcf562ceb55af5b5f9cdb7e"] : $this->getContext($context, "__internal_7b65e151a81bd23bb428f58e1452845a8d9286496fcf562ceb55af5b5f9cdb7e")))) : (        // line 15
(isset($context["_default_title"]) ? $context["_default_title"] : $this->getContext($context, "_default_title")))), "html", null, true);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_e3b65773481a3397d36c64a6ffa5689c025bf23cdbfb36d838302daa66afce40->leave($__internal_e3b65773481a3397d36c64a6ffa5689c025bf23cdbfb36d838302daa66afce40_prof);

    }

    // line 19
    public function block_main($context, array $blocks = array())
    {
        $__internal_befcc16ae943a141f37791706ee0b2c6823bf4f47013b8fcfd50b33b1f98d1cb = $this->env->getExtension("native_profiler");
        $__internal_befcc16ae943a141f37791706ee0b2c6823bf4f47013b8fcfd50b33b1f98d1cb->enter($__internal_befcc16ae943a141f37791706ee0b2c6823bf4f47013b8fcfd50b33b1f98d1cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 20
        echo "    ";
        $this->displayBlock('entity_form', $context, $blocks);
        
        $__internal_befcc16ae943a141f37791706ee0b2c6823bf4f47013b8fcfd50b33b1f98d1cb->leave($__internal_befcc16ae943a141f37791706ee0b2c6823bf4f47013b8fcfd50b33b1f98d1cb_prof);

    }

    public function block_entity_form($context, array $blocks = array())
    {
        $__internal_65bb9a0cfd5bede92d2517ee4d741536dc1ecbfcf668a45ea24f0beb62e74c8e = $this->env->getExtension("native_profiler");
        $__internal_65bb9a0cfd5bede92d2517ee4d741536dc1ecbfcf668a45ea24f0beb62e74c8e->enter($__internal_65bb9a0cfd5bede92d2517ee4d741536dc1ecbfcf668a45ea24f0beb62e74c8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        // line 21
        echo "        ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
    ";
        
        $__internal_65bb9a0cfd5bede92d2517ee4d741536dc1ecbfcf668a45ea24f0beb62e74c8e->leave($__internal_65bb9a0cfd5bede92d2517ee4d741536dc1ecbfcf668a45ea24f0beb62e74c8e_prof);

    }

    // line 25
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_4e1651c20892df317c416970bf998a4c1dc029f99fccc0d3edf0eee7ebc49e7c = $this->env->getExtension("native_profiler");
        $__internal_4e1651c20892df317c416970bf998a4c1dc029f99fccc0d3edf0eee7ebc49e7c->enter($__internal_4e1651c20892df317c416970bf998a4c1dc029f99fccc0d3edf0eee7ebc49e7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        // line 26
        echo "    ";
        $this->displayParentBlock("body_javascript", $context, $blocks);
        echo "

    <script type=\"text/javascript\">
        \$(function() {
            \$('.new-form').areYouSure({ 'message': '";
        // line 30
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("form.are_you_sure", array(), "EasyAdminBundle"), "js"), "html", null, true);
        echo "' });

            \$('.form-actions').easyAdminSticky();
        });
    </script>

    ";
        // line 36
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_select2_widget.html.twig");
        echo "
";
        
        $__internal_4e1651c20892df317c416970bf998a4c1dc029f99fccc0d3edf0eee7ebc49e7c->leave($__internal_4e1651c20892df317c416970bf998a4c1dc029f99fccc0d3edf0eee7ebc49e7c_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 36,  141 => 30,  133 => 26,  127 => 25,  117 => 21,  104 => 20,  98 => 19,  88 => 15,  87 => 4,  85 => 15,  82 => 14,  80 => 13,  74 => 12,  62 => 10,  50 => 9,  43 => 7,  41 => 4,  40 => 5,  39 => 4,  38 => 5,  36 => 4,  34 => 3,  32 => 1,  23 => 7,);
    }
}
/* {% form_theme form with easyadmin_config('design.form_theme') %}*/
/* */
/* {% set _entity_config = easyadmin_entity(app.request.query.get('entity')) %}*/
/* {% trans_default_domain _entity_config.translation_domain %}*/
/* {% set _trans_parameters = { '%entity_name%': _entity_config.name|trans, '%entity_label%': _entity_config.label|trans } %}*/
/* */
/* {% extends _entity_config.templates.layout %}*/
/* */
/* {% block body_id 'easyadmin-new-' ~ _entity_config.name %}*/
/* {% block body_class 'new new-' ~ _entity_config.name|lower %}*/
/* */
/* {% block content_title %}*/
/* {% spaceless %}*/
/*     {% set _default_title = 'new.page_title'|trans(_trans_parameters, 'EasyAdminBundle') %}*/
/*     {{ _entity_config.new.title is defined ? _entity_config.new.title|trans(_trans_parameters) : _default_title }}*/
/* {% endspaceless %}*/
/* {% endblock %}*/
/* */
/* {% block main %}*/
/*     {% block entity_form %}*/
/*         {{ form(form) }}*/
/*     {% endblock entity_form %}*/
/* {% endblock %}*/
/* */
/* {% block body_javascript %}*/
/*     {{ parent() }}*/
/* */
/*     <script type="text/javascript">*/
/*         $(function() {*/
/*             $('.new-form').areYouSure({ 'message': '{{ 'form.are_you_sure'|trans({}, 'EasyAdminBundle')|e('js') }}' });*/
/* */
/*             $('.form-actions').easyAdminSticky();*/
/*         });*/
/*     </script>*/
/* */
/*     {{ include('@EasyAdmin/default/includes/_select2_widget.html.twig') }}*/
/* {% endblock %}*/
/* */
