<?php

/* musikasespacePriveBundle:Default:espace_prive_nouvel_article.html.twig */
class __TwigTemplate_c78a2be620ac0314747ede559ad580f22926a59fb733a9c573ad4150f7c97152 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMerePrive.html.twig", "musikasespacePriveBundle:Default:espace_prive_nouvel_article.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMerePrive.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf1a6038b9ad3fd4d2c1d495bc8d0333f90ec8e7bd904a22efdfef111fd170c3 = $this->env->getExtension("native_profiler");
        $__internal_cf1a6038b9ad3fd4d2c1d495bc8d0333f90ec8e7bd904a22efdfef111fd170c3->enter($__internal_cf1a6038b9ad3fd4d2c1d495bc8d0333f90ec8e7bd904a22efdfef111fd170c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasespacePriveBundle:Default:espace_prive_nouvel_article.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cf1a6038b9ad3fd4d2c1d495bc8d0333f90ec8e7bd904a22efdfef111fd170c3->leave($__internal_cf1a6038b9ad3fd4d2c1d495bc8d0333f90ec8e7bd904a22efdfef111fd170c3_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_5735f7d92573e099bbca1a0270a430713a08eae5d4d1a454893f6d5fdc3985cc = $this->env->getExtension("native_profiler");
        $__internal_5735f7d92573e099bbca1a0270a430713a08eae5d4d1a454893f6d5fdc3985cc->enter($__internal_5735f7d92573e099bbca1a0270a430713a08eae5d4d1a454893f6d5fdc3985cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <section id=\"admin\">

<section>
    <div class=\"container\" id=\"contenu_site\">
        <div class=\"row articles\">
            ";
        // line 9
        echo twig_include($this->env, $context, "default/barre.html.twig");
        echo "
            
            <div class=\"col-md-8 item\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\"></div>
                    <div class=\"panel-body\">
                        <div class=\"row\" id=\"contact\">
                            <div class=\"col-md-12\">
                                <h1>Ecrire un article</h1></div>
                            <div class=\"col-md-12\">
                                <form>
                                    <div class=\"form-group\">
                                        <label class=\"control-label\">Titre</label>
                                        <input type=\"text\" class=\"form-control\" />
                                    </div>
                                    <div class=\"form-group\">
                                        <label class=\"control-label\">Auteur</label>
                                        <input type=\"email\" class=\"form-control\" />
                                    </div>
                                    <div class=\"form-group\">
                                        <label class=\"control-label\">Image</label>
                                        <input type=\"tel\" class=\"form-control\" />
                                    </div>
                                    <div class=\"form-group\">
                                        <label class=\"control-label\">Texte</label>
                                        <textarea class=\"form-control\"></textarea>
                                    </div>
                                </form>
                                <button class=\"btn btn-default\" type=\"button\">Valider</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


    </section>
";
        
        $__internal_5735f7d92573e099bbca1a0270a430713a08eae5d4d1a454893f6d5fdc3985cc->leave($__internal_5735f7d92573e099bbca1a0270a430713a08eae5d4d1a454893f6d5fdc3985cc_prof);

    }

    public function getTemplateName()
    {
        return "musikasespacePriveBundle:Default:espace_prive_nouvel_article.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 9,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMerePrive.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <section id="admin">*/
/* */
/* <section>*/
/*     <div class="container" id="contenu_site">*/
/*         <div class="row articles">*/
/*             {{ include('default/barre.html.twig') }}*/
/*             */
/*             <div class="col-md-8 item">*/
/*                 <div class="panel panel-default">*/
/*                     <div class="panel-heading"></div>*/
/*                     <div class="panel-body">*/
/*                         <div class="row" id="contact">*/
/*                             <div class="col-md-12">*/
/*                                 <h1>Ecrire un article</h1></div>*/
/*                             <div class="col-md-12">*/
/*                                 <form>*/
/*                                     <div class="form-group">*/
/*                                         <label class="control-label">Titre</label>*/
/*                                         <input type="text" class="form-control" />*/
/*                                     </div>*/
/*                                     <div class="form-group">*/
/*                                         <label class="control-label">Auteur</label>*/
/*                                         <input type="email" class="form-control" />*/
/*                                     </div>*/
/*                                     <div class="form-group">*/
/*                                         <label class="control-label">Image</label>*/
/*                                         <input type="tel" class="form-control" />*/
/*                                     </div>*/
/*                                     <div class="form-group">*/
/*                                         <label class="control-label">Texte</label>*/
/*                                         <textarea class="form-control"></textarea>*/
/*                                     </div>*/
/*                                 </form>*/
/*                                 <button class="btn btn-default" type="button">Valider</button>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </section>*/
/* */
/* */
/*     </section>*/
/* {% endblock %}*/
