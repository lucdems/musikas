<?php

/* EasyAdminBundle:default:exception.html.twig */
class __TwigTemplate_83ba5c74bf7c46cc097c329d44735c66d89477ac25544c1bc1cf551ee365d6e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'body_class' => array($this, 'block_body_class'),
            'page_title' => array($this, 'block_page_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return $this->loadTemplate(array(0 => (($this->getAttribute($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : null), "templates", array(), "any", false, true), "layout", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["_entity_config"]) ? $context["_entity_config"] : null), "templates", array(), "any", false, true), "layout", array()), "")) : ("")), 1 => $this->env->getExtension('easyadmin_extension')->getBackendConfiguration("design.templates.layout"), 2 => "@EasyAdmin/default/layout.html.twig"), "EasyAdminBundle:default:exception.html.twig", 3);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ff2c7c65ab6579c433f9c7bdfea98ad23ffb3706cb626e30b1d439fbf9cca599 = $this->env->getExtension("native_profiler");
        $__internal_ff2c7c65ab6579c433f9c7bdfea98ad23ffb3706cb626e30b1d439fbf9cca599->enter($__internal_ff2c7c65ab6579c433f9c7bdfea98ad23ffb3706cb626e30b1d439fbf9cca599_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:exception.html.twig"));

        // line 1
        $context["_entity_config"] = $this->env->getExtension('easyadmin_extension')->getEntityConfiguration($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "entity"), "method"));
        // line 3
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ff2c7c65ab6579c433f9c7bdfea98ad23ffb3706cb626e30b1d439fbf9cca599->leave($__internal_ff2c7c65ab6579c433f9c7bdfea98ad23ffb3706cb626e30b1d439fbf9cca599_prof);

    }

    // line 8
    public function block_body_class($context, array $blocks = array())
    {
        $__internal_2c709a0d1ea8eaffb9cfe8dc4c97a8e03105b2668b09d12d6d2e270dec82d832 = $this->env->getExtension("native_profiler");
        $__internal_2c709a0d1ea8eaffb9cfe8dc4c97a8e03105b2668b09d12d6d2e270dec82d832->enter($__internal_2c709a0d1ea8eaffb9cfe8dc4c97a8e03105b2668b09d12d6d2e270dec82d832_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        echo "error";
        
        $__internal_2c709a0d1ea8eaffb9cfe8dc4c97a8e03105b2668b09d12d6d2e270dec82d832->leave($__internal_2c709a0d1ea8eaffb9cfe8dc4c97a8e03105b2668b09d12d6d2e270dec82d832_prof);

    }

    // line 9
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_ab306a431869ec4a6e5bb354b75e0ba2bc886eaa83c7aa313a23f589dbfb722f = $this->env->getExtension("native_profiler");
        $__internal_ab306a431869ec4a6e5bb354b75e0ba2bc886eaa83c7aa313a23f589dbfb722f->enter($__internal_ab306a431869ec4a6e5bb354b75e0ba2bc886eaa83c7aa313a23f589dbfb722f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->transchoice("errors", 1, array(), "EasyAdminBundle"), "html", null, true);
        
        $__internal_ab306a431869ec4a6e5bb354b75e0ba2bc886eaa83c7aa313a23f589dbfb722f->leave($__internal_ab306a431869ec4a6e5bb354b75e0ba2bc886eaa83c7aa313a23f589dbfb722f_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_c3b88418ba822e127288020f9638ea4804f9529b55a67821ab1e1ce791f40417 = $this->env->getExtension("native_profiler");
        $__internal_c3b88418ba822e127288020f9638ea4804f9529b55a67821ab1e1ce791f40417->enter($__internal_c3b88418ba822e127288020f9638ea4804f9529b55a67821ab1e1ce791f40417_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 12
        echo "    <section id=\"main\" class=\"content\">
        <div class=\"error-description\">
            <h1><i class=\"fa fa-exclamation-circle\"></i> ";
        // line 14
        $this->displayBlock("page_title", $context, $blocks);
        echo "</h1>

            <div class=\"error-message\">
                ";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "publicMessage", array()), $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "translationParameters", array()), "EasyAdminBundle"), "html", null, true);
        echo "
            </div>
        </div>
    </section>
";
        
        $__internal_c3b88418ba822e127288020f9638ea4804f9529b55a67821ab1e1ce791f40417->leave($__internal_c3b88418ba822e127288020f9638ea4804f9529b55a67821ab1e1ce791f40417_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 17,  72 => 14,  68 => 12,  62 => 11,  50 => 9,  38 => 8,  31 => 3,  29 => 1,  20 => 3,);
    }
}
/* {% set _entity_config = easyadmin_entity(app.request.query.get('entity')) %}*/
/* {% extends [*/
/*     _entity_config.templates.layout|default(''),*/
/*     easyadmin_config('design.templates.layout'),*/
/*     '@EasyAdmin/default/layout.html.twig'*/
/* ] %}*/
/* */
/* {% block body_class 'error' %}*/
/* {% block page_title %}{{ 'errors'|transchoice(1, {}, 'EasyAdminBundle') }}{% endblock %}*/
/* */
/* {% block content %}*/
/*     <section id="main" class="content">*/
/*         <div class="error-description">*/
/*             <h1><i class="fa fa-exclamation-circle"></i> {{ block('page_title') }}</h1>*/
/* */
/*             <div class="error-message">*/
/*                 {{ exception.publicMessage|trans(exception.translationParameters, 'EasyAdminBundle') }}*/
/*             </div>*/
/*         </div>*/
/*     </section>*/
/* {% endblock %}*/
/* */
