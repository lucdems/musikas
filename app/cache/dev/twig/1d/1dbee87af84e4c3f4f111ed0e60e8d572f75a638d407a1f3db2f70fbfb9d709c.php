<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_3aad36d3d137db1847fa7255e113a4e93a7ea59e989d306c10da49863628120e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_71219bb243e5cf4cc34dd9007caa182c6d502692563220f44edcbb0d9df61955 = $this->env->getExtension("native_profiler");
        $__internal_71219bb243e5cf4cc34dd9007caa182c6d502692563220f44edcbb0d9df61955->enter($__internal_71219bb243e5cf4cc34dd9007caa182c6d502692563220f44edcbb0d9df61955_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_71219bb243e5cf4cc34dd9007caa182c6d502692563220f44edcbb0d9df61955->leave($__internal_71219bb243e5cf4cc34dd9007caa182c6d502692563220f44edcbb0d9df61955_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!isset($render_rest) || $render_rest): ?>*/
/* <?php echo $view['form']->rest($form) ?>*/
/* <?php endif ?>*/
/* </form>*/
/* */
