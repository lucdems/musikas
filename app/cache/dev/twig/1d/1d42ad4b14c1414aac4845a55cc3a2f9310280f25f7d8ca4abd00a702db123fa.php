<?php

/* EasyAdminBundle:default:label_empty.html.twig */
class __TwigTemplate_2ae105bf796653a3e632c50d961bb5d26360813eb87f4ad9f5fda31af7d8d4ac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1db7fe95600207d1802c76f233de4a1d0a221bc6c1424ccc2cd26f263f37bbfe = $this->env->getExtension("native_profiler");
        $__internal_1db7fe95600207d1802c76f233de4a1d0a221bc6c1424ccc2cd26f263f37bbfe->enter($__internal_1db7fe95600207d1802c76f233de4a1d0a221bc6c1424ccc2cd26f263f37bbfe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_empty.html.twig"));

        // line 1
        echo "<span class=\"label label-empty\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("label.empty", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
";
        
        $__internal_1db7fe95600207d1802c76f233de4a1d0a221bc6c1424ccc2cd26f263f37bbfe->leave($__internal_1db7fe95600207d1802c76f233de4a1d0a221bc6c1424ccc2cd26f263f37bbfe_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:label_empty.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <span class="label label-empty">{{ 'label.empty'|trans(domain = 'EasyAdminBundle') }}</span>*/
/* */
