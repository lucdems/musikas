<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_a0bf8468c5c311ddd63052d55f360b105310f7e4d68f124c9e75cbff5478cda7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b9efa20378c1b4b1eed28410e8e17911e0785598f9e0a24c441de82d732fccd = $this->env->getExtension("native_profiler");
        $__internal_5b9efa20378c1b4b1eed28410e8e17911e0785598f9e0a24c441de82d732fccd->enter($__internal_5b9efa20378c1b4b1eed28410e8e17911e0785598f9e0a24c441de82d732fccd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["group"]) ? $context["group"] : $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_5b9efa20378c1b4b1eed28410e8e17911e0785598f9e0a24c441de82d732fccd->leave($__internal_5b9efa20378c1b4b1eed28410e8e17911e0785598f9e0a24c441de82d732fccd_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 4,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* <div class="fos_user_group_show">*/
/*     <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>*/
/* </div>*/
/* */
