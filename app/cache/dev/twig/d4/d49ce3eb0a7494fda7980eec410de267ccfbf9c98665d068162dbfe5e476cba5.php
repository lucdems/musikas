<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_1e4fbdeef98bc744b30e41bd63b54d71c83424057dbf3c0596e9b122f7e704b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dab4f341cf5062adb890d4f7dfded46422a1435e498ca9b6c78e35cc5dc96984 = $this->env->getExtension("native_profiler");
        $__internal_dab4f341cf5062adb890d4f7dfded46422a1435e498ca9b6c78e35cc5dc96984->enter($__internal_dab4f341cf5062adb890d4f7dfded46422a1435e498ca9b6c78e35cc5dc96984_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_dab4f341cf5062adb890d4f7dfded46422a1435e498ca9b6c78e35cc5dc96984->leave($__internal_dab4f341cf5062adb890d4f7dfded46422a1435e498ca9b6c78e35cc5dc96984_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
