<?php

/* musikasespacePriveBundle:Default:admin.html.twig */
class __TwigTemplate_9da7a0234bff62b81d220ceec5d4579e10f4bc48b7aab17067688a5440a5664d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMerePrive.html.twig", "musikasespacePriveBundle:Default:admin.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMerePrive.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ce7f0a96560569bfa7a1c5252a03d222dcd0a22102fcc04ca741b38d32563787 = $this->env->getExtension("native_profiler");
        $__internal_ce7f0a96560569bfa7a1c5252a03d222dcd0a22102fcc04ca741b38d32563787->enter($__internal_ce7f0a96560569bfa7a1c5252a03d222dcd0a22102fcc04ca741b38d32563787_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasespacePriveBundle:Default:admin.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ce7f0a96560569bfa7a1c5252a03d222dcd0a22102fcc04ca741b38d32563787->leave($__internal_ce7f0a96560569bfa7a1c5252a03d222dcd0a22102fcc04ca741b38d32563787_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_7924bb908abe29b0eb6ce75e6e69743509af6b28302730ae1c87d17433510e6d = $this->env->getExtension("native_profiler");
        $__internal_7924bb908abe29b0eb6ce75e6e69743509af6b28302730ae1c87d17433510e6d->enter($__internal_7924bb908abe29b0eb6ce75e6e69743509af6b28302730ae1c87d17433510e6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <section id=\"admin\">
        Ici, bientôt la page d'administration.
    </section>
";
        
        $__internal_7924bb908abe29b0eb6ce75e6e69743509af6b28302730ae1c87d17433510e6d->leave($__internal_7924bb908abe29b0eb6ce75e6e69743509af6b28302730ae1c87d17433510e6d_prof);

    }

    public function getTemplateName()
    {
        return "musikasespacePriveBundle:Default:admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMerePrive.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <section id="admin">*/
/*         Ici, bientôt la page d'administration.*/
/*     </section>*/
/* {% endblock %}*/
