<?php

/* musikasespacePriveBundle:Default:espace_prive_utilisateurs.html.twig */
class __TwigTemplate_6d474aaf1d71dc719065375fe717991839ae6bc4e7eac7647f333817f2e8c621 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMerePrive.html.twig", "musikasespacePriveBundle:Default:espace_prive_utilisateurs.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMerePrive.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d0f121f5eae141e894bce32d6417020207fd534cd2cbf7745383ac77e8418c9 = $this->env->getExtension("native_profiler");
        $__internal_3d0f121f5eae141e894bce32d6417020207fd534cd2cbf7745383ac77e8418c9->enter($__internal_3d0f121f5eae141e894bce32d6417020207fd534cd2cbf7745383ac77e8418c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasespacePriveBundle:Default:espace_prive_utilisateurs.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3d0f121f5eae141e894bce32d6417020207fd534cd2cbf7745383ac77e8418c9->leave($__internal_3d0f121f5eae141e894bce32d6417020207fd534cd2cbf7745383ac77e8418c9_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_14acf4a5ae8cf9a5cbd295a20fb24818cbb25cffb9c8feee042c3738bf3675eb = $this->env->getExtension("native_profiler");
        $__internal_14acf4a5ae8cf9a5cbd295a20fb24818cbb25cffb9c8feee042c3738bf3675eb->enter($__internal_14acf4a5ae8cf9a5cbd295a20fb24818cbb25cffb9c8feee042c3738bf3675eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <section id=\"admin\">

    <div class=\"container\" id=\"contenu_site\">
        <div class=\"row articles\">
            
            ";
        // line 9
        echo twig_include($this->env, $context, "default/barre.html.twig");
        echo "
            
            <div class=\"col-md-8 item\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        <div class=\"row\">
                            <div class=\"col-lg-7 col-sm-6 col-xs-12\">
                                <h3 class=\"panel-title\"><span class=\"fa-stack\"><i class=\"fa fa-circle fa-stack-2x text-muted\"></i><i class=\"fa fa-users fa-stack-1x fa-inverse\"></i></span>Utilisateurs</h3></div>
                            <div class=\"col-lg-5 col-sm-6 col-xs-12 text-right\">
                                <div class=\"btn-group btn-group-justified\"><a class=\"btn btn-primary active\" href=\"#\">Fiscal Year</a>
                                    <div class=\"dropdown btn-group\">
                                        <button class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\" type=\"button\">Tri<i class=\"fa fa-chevron-down\"></i></button>
                                        <ul class=\"dropdown-menu\">
                                            <li><a href=\"#\">Nom</a></li>
                                            <li><a href=\"#\">Email</a></li>
                                            <li><a href=\"#\">ID</a></li>
                                            <li><a href=\"#\">Ecoles</a></li>
                                            <li><a href=\"#\">Age</a></li>
                                            <li><a href=\"#\">Etat</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"panel-body\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                
                                    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
                                            <p></p>
                                            <h1>Outil de gestion des utilisateurs du site</h1>
                                            <p>Vous pouvez ici administrer les comptes utilisateurs de votre site. Utiliser les butons pour ajouter, supprimer ou modifier le compte d'un utilisateur.</p>
                                            <p></p>
                                            <p></p>
                                    
                                                <div class=\"panel panel-default panel-table\">
                                                    <div class=\"panel-heading\">
                                                        <div class=\"row\">
                                                            <div class=\"col-lg-7 col-sm-6 col-xs-12\">
                                                                <h3 class=\"panel-title\">Liste des utilisateurs</h3></div>
                                                            <div class=\"col-lg-5 col-sm-6 col-xs-12 text-right\">
                                                                <button type=\"button\" class=\"btn btn-sm btn-primary btn-create\">Créer un nouveau</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class=\"panel-body\">
                                                        <table class=\"table table-striped table-bordered table-list\">
                                                            <thead>
                                                                <tr>
                                                                    <th><em class=\"fa fa-cog\"></em></th>
                                                                    <th class=\"hidden-xs\">ID</th>
                                                                    <th>Name</th>
                                                                    <th>Email</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"center\"><a class=\"btn btn-default\"><em class=\"fa fa-pencil\"></em></a><a class=\"btn btn-danger\"><em class=\"fa fa-trash\"></em></a></td>
                                                                    <td class=\"hidden-xs\">1</td>
                                                                    <td>John Doe</td>
                                                                    <td>johndoe@example.com</td>
                                                                </tr>
                                                            </tbody>
                                                            
                                                            
                                                            
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"center\"><a class=\"btn btn-default\"><em class=\"fa fa-pencil\"></em></a><a class=\"btn btn-danger\"><em class=\"fa fa-trash\"></em></a></td>
                                                                    <td class=\"hidden-xs\">1</td>
                                                                    <td>John Doe</td>
                                                                    <td>johndoe@example.com</td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"center\"><a class=\"btn btn-default\"><em class=\"fa fa-pencil\"></em></a><a class=\"btn btn-danger\"><em class=\"fa fa-trash\"></em></a></td>
                                                                    <td class=\"hidden-xs\">1</td>
                                                                    <td>Lucas Doe</td>
                                                                    <td>johndoe@example.com</td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"center\"><a class=\"btn btn-default\"><em class=\"fa fa-pencil\"></em></a><a class=\"btn btn-danger\"><em class=\"fa fa-trash\"></em></a></td>
                                                                    <td class=\"hidden-xs\">1</td>
                                                                    <td>Justin Doe</td>
                                                                    <td>johndoe@example.com</td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"center\"><a class=\"btn btn-default\"><em class=\"fa fa-pencil\"></em></a><a class=\"btn btn-danger\"><em class=\"fa fa-trash\"></em></a></td>
                                                                    <td class=\"hidden-xs\">1</td>
                                                                    <td>Sylvie Doe</td>
                                                                    <td>johndoe@example.com</td>
                                                                </tr>
                                                            </tbody>
                                                            
                                                            
                                                        </table>
                                                    </div>
                                                    <div class=\"panel-footer\">
                                                        <div class=\"row\">
                                                            <div class=\"col col-xs-4\">Page 1 of 5</div>
                                                            <div class=\"col col-xs-8\">
                                                                <ul class=\"pagination hidden-xs pull-right\">
                                                                    <li><a href=\"#\">1</a></li>
                                                                    <li><a href=\"#\">2</a></li>
                                                                    <li><a href=\"#\">3</a></li>
                                                                    <li><a href=\"#\">4</a></li>
                                                                    <li><a href=\"#\">5</a></li>
                                                                </ul>
                                                                <ul class=\"pagination visible-xs pull-right\">
                                                                    <li><a href=\"#\">«</a></li>
                                                                    <li><a href=\"#\">»</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    </section>
";
        
        $__internal_14acf4a5ae8cf9a5cbd295a20fb24818cbb25cffb9c8feee042c3738bf3675eb->leave($__internal_14acf4a5ae8cf9a5cbd295a20fb24818cbb25cffb9c8feee042c3738bf3675eb_prof);

    }

    public function getTemplateName()
    {
        return "musikasespacePriveBundle:Default:espace_prive_utilisateurs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 9,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMerePrive.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <section id="admin">*/
/* */
/*     <div class="container" id="contenu_site">*/
/*         <div class="row articles">*/
/*             */
/*             {{ include('default/barre.html.twig') }}*/
/*             */
/*             <div class="col-md-8 item">*/
/*                 <div class="panel panel-default">*/
/*                     <div class="panel-heading">*/
/*                         <div class="row">*/
/*                             <div class="col-lg-7 col-sm-6 col-xs-12">*/
/*                                 <h3 class="panel-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-2x text-muted"></i><i class="fa fa-users fa-stack-1x fa-inverse"></i></span>Utilisateurs</h3></div>*/
/*                             <div class="col-lg-5 col-sm-6 col-xs-12 text-right">*/
/*                                 <div class="btn-group btn-group-justified"><a class="btn btn-primary active" href="#">Fiscal Year</a>*/
/*                                     <div class="dropdown btn-group">*/
/*                                         <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" type="button">Tri<i class="fa fa-chevron-down"></i></button>*/
/*                                         <ul class="dropdown-menu">*/
/*                                             <li><a href="#">Nom</a></li>*/
/*                                             <li><a href="#">Email</a></li>*/
/*                                             <li><a href="#">ID</a></li>*/
/*                                             <li><a href="#">Ecoles</a></li>*/
/*                                             <li><a href="#">Age</a></li>*/
/*                                             <li><a href="#">Etat</a></li>*/
/*                                         </ul>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="panel-body">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 */
/*                                     <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />*/
/*                                             <p></p>*/
/*                                             <h1>Outil de gestion des utilisateurs du site</h1>*/
/*                                             <p>Vous pouvez ici administrer les comptes utilisateurs de votre site. Utiliser les butons pour ajouter, supprimer ou modifier le compte d'un utilisateur.</p>*/
/*                                             <p></p>*/
/*                                             <p></p>*/
/*                                     */
/*                                                 <div class="panel panel-default panel-table">*/
/*                                                     <div class="panel-heading">*/
/*                                                         <div class="row">*/
/*                                                             <div class="col-lg-7 col-sm-6 col-xs-12">*/
/*                                                                 <h3 class="panel-title">Liste des utilisateurs</h3></div>*/
/*                                                             <div class="col-lg-5 col-sm-6 col-xs-12 text-right">*/
/*                                                                 <button type="button" class="btn btn-sm btn-primary btn-create">Créer un nouveau</button>*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                     <div class="panel-body">*/
/*                                                         <table class="table table-striped table-bordered table-list">*/
/*                                                             <thead>*/
/*                                                                 <tr>*/
/*                                                                     <th><em class="fa fa-cog"></em></th>*/
/*                                                                     <th class="hidden-xs">ID</th>*/
/*                                                                     <th>Name</th>*/
/*                                                                     <th>Email</th>*/
/*                                                                 </tr>*/
/*                                                             </thead>*/
/*                                                             <tbody>*/
/*                                                                 <tr>*/
/*                                                                     <td align="center"><a class="btn btn-default"><em class="fa fa-pencil"></em></a><a class="btn btn-danger"><em class="fa fa-trash"></em></a></td>*/
/*                                                                     <td class="hidden-xs">1</td>*/
/*                                                                     <td>John Doe</td>*/
/*                                                                     <td>johndoe@example.com</td>*/
/*                                                                 </tr>*/
/*                                                             </tbody>*/
/*                                                             */
/*                                                             */
/*                                                             */
/*                                                             <tbody>*/
/*                                                                 <tr>*/
/*                                                                     <td align="center"><a class="btn btn-default"><em class="fa fa-pencil"></em></a><a class="btn btn-danger"><em class="fa fa-trash"></em></a></td>*/
/*                                                                     <td class="hidden-xs">1</td>*/
/*                                                                     <td>John Doe</td>*/
/*                                                                     <td>johndoe@example.com</td>*/
/*                                                                 </tr>*/
/*                                                             </tbody>*/
/*                                                             <tbody>*/
/*                                                                 <tr>*/
/*                                                                     <td align="center"><a class="btn btn-default"><em class="fa fa-pencil"></em></a><a class="btn btn-danger"><em class="fa fa-trash"></em></a></td>*/
/*                                                                     <td class="hidden-xs">1</td>*/
/*                                                                     <td>Lucas Doe</td>*/
/*                                                                     <td>johndoe@example.com</td>*/
/*                                                                 </tr>*/
/*                                                             </tbody>*/
/*                                                             <tbody>*/
/*                                                                 <tr>*/
/*                                                                     <td align="center"><a class="btn btn-default"><em class="fa fa-pencil"></em></a><a class="btn btn-danger"><em class="fa fa-trash"></em></a></td>*/
/*                                                                     <td class="hidden-xs">1</td>*/
/*                                                                     <td>Justin Doe</td>*/
/*                                                                     <td>johndoe@example.com</td>*/
/*                                                                 </tr>*/
/*                                                             </tbody>*/
/*                                                             <tbody>*/
/*                                                                 <tr>*/
/*                                                                     <td align="center"><a class="btn btn-default"><em class="fa fa-pencil"></em></a><a class="btn btn-danger"><em class="fa fa-trash"></em></a></td>*/
/*                                                                     <td class="hidden-xs">1</td>*/
/*                                                                     <td>Sylvie Doe</td>*/
/*                                                                     <td>johndoe@example.com</td>*/
/*                                                                 </tr>*/
/*                                                             </tbody>*/
/*                                                             */
/*                                                             */
/*                                                         </table>*/
/*                                                     </div>*/
/*                                                     <div class="panel-footer">*/
/*                                                         <div class="row">*/
/*                                                             <div class="col col-xs-4">Page 1 of 5</div>*/
/*                                                             <div class="col col-xs-8">*/
/*                                                                 <ul class="pagination hidden-xs pull-right">*/
/*                                                                     <li><a href="#">1</a></li>*/
/*                                                                     <li><a href="#">2</a></li>*/
/*                                                                     <li><a href="#">3</a></li>*/
/*                                                                     <li><a href="#">4</a></li>*/
/*                                                                     <li><a href="#">5</a></li>*/
/*                                                                 </ul>*/
/*                                                                 <ul class="pagination visible-xs pull-right">*/
/*                                                                     <li><a href="#">«</a></li>*/
/*                                                                     <li><a href="#">»</a></li>*/
/*                                                                 </ul>*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </section>*/
/*     </section>*/
/* {% endblock %}*/
