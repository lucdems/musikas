<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_efc3a3bf21ac8f8b17994250b04d0dfe30ba953ae78e803b3fb12a4a293d0447 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_417898c1821272fa140700cf9649485030d641ee106f1cab30fe4698d7c56950 = $this->env->getExtension("native_profiler");
        $__internal_417898c1821272fa140700cf9649485030d641ee106f1cab30fe4698d7c56950->enter($__internal_417898c1821272fa140700cf9649485030d641ee106f1cab30fe4698d7c56950_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_417898c1821272fa140700cf9649485030d641ee106f1cab30fe4698d7c56950->leave($__internal_417898c1821272fa140700cf9649485030d641ee106f1cab30fe4698d7c56950_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?>*/
/* */
