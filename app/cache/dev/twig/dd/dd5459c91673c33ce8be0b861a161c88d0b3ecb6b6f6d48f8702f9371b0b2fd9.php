<?php

/* EasyAdminBundle:default:field_integer.html.twig */
class __TwigTemplate_6b55dcf65ed3104c578205fca14406b98883b2b21ec95d8f6ed1b883d8f1049f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_59193b026386cfdef2fac558ec69ae6699e60f6a11c76b5ebf286d012c934769 = $this->env->getExtension("native_profiler");
        $__internal_59193b026386cfdef2fac558ec69ae6699e60f6a11c76b5ebf286d012c934769->enter($__internal_59193b026386cfdef2fac558ec69ae6699e60f6a11c76b5ebf286d012c934769_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_integer.html.twig"));

        // line 1
        if ($this->getAttribute((isset($context["field_options"]) ? $context["field_options"] : $this->getContext($context, "field_options")), "format", array())) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, sprintf($this->getAttribute((isset($context["field_options"]) ? $context["field_options"] : $this->getContext($context, "field_options")), "format", array()), (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        }
        
        $__internal_59193b026386cfdef2fac558ec69ae6699e60f6a11c76b5ebf286d012c934769->leave($__internal_59193b026386cfdef2fac558ec69ae6699e60f6a11c76b5ebf286d012c934769_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_integer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  24 => 2,  22 => 1,);
    }
}
/* {% if field_options.format %}*/
/*     {{ field_options.format|format(value) }}*/
/* {% else %}*/
/*     {{ value|number_format }}*/
/* {% endif %}*/
/* */
