<?php

/* @EasyAdmin/default/label_null.html.twig */
class __TwigTemplate_f7a44d470de3ef304f53c368a81bc70ea109dcb0e2eef4d49acd8e5061839672 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa567753b263e43c6c1b5dc8f9e587bd0f6f49a750ba80c302455bee67e87cb7 = $this->env->getExtension("native_profiler");
        $__internal_fa567753b263e43c6c1b5dc8f9e587bd0f6f49a750ba80c302455bee67e87cb7->enter($__internal_fa567753b263e43c6c1b5dc8f9e587bd0f6f49a750ba80c302455bee67e87cb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/label_null.html.twig"));

        // line 1
        echo "<span class=\"label\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("label.null", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
";
        
        $__internal_fa567753b263e43c6c1b5dc8f9e587bd0f6f49a750ba80c302455bee67e87cb7->leave($__internal_fa567753b263e43c6c1b5dc8f9e587bd0f6f49a750ba80c302455bee67e87cb7_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/label_null.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <span class="label">{{ 'label.null'|trans(domain = 'EasyAdminBundle') }}</span>*/
/* */
