<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_dfea2f5e6603fe19b56e5c3dca32fcddc4a690bec45ac11ec6dc8d040742eb13 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_183cce66c7f6d33440506060fbccb112a2cb2839fd77e35d51a5c72e5eed87e7 = $this->env->getExtension("native_profiler");
        $__internal_183cce66c7f6d33440506060fbccb112a2cb2839fd77e35d51a5c72e5eed87e7->enter($__internal_183cce66c7f6d33440506060fbccb112a2cb2839fd77e35d51a5c72e5eed87e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_183cce66c7f6d33440506060fbccb112a2cb2839fd77e35d51a5c72e5eed87e7->leave($__internal_183cce66c7f6d33440506060fbccb112a2cb2839fd77e35d51a5c72e5eed87e7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!$label) { $label = isset($label_format)*/
/*     ? strtr($label_format, array('%name%' => $name, '%id%' => $id))*/
/*     : $view['form']->humanize($name); } ?>*/
/* <button type="<?php echo isset($type) ? $view->escape($type) : 'button' ?>" <?php echo $view['form']->block($form, 'button_attributes') ?>><?php echo $view->escape(false !== $translation_domain ? $view['translator']->trans($label, array(), $translation_domain) : $label) ?></button>*/
/* */
