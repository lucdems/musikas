<?php

/* EasyAdminBundle:default:field_text.html.twig */
class __TwigTemplate_e9a878481f51532d63a2e7f870b929f63ed71cbf2b02954352c57c5810b6053f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_513b9b9628fb50e8ec1f6cc0600985e4c1903d29222088cf0aeab80c7cc281d9 = $this->env->getExtension("native_profiler");
        $__internal_513b9b9628fb50e8ec1f6cc0600985e4c1903d29222088cf0aeab80c7cc281d9->enter($__internal_513b9b9628fb50e8ec1f6cc0600985e4c1903d29222088cf0aeab80c7cc281d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_text.html.twig"));

        // line 1
        if (((isset($context["view"]) ? $context["view"] : $this->getContext($context, "view")) == "show")) {
            // line 2
            echo "    ";
            echo nl2br(twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true));
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('easyadmin_extension')->truncateText($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        }
        
        $__internal_513b9b9628fb50e8ec1f6cc0600985e4c1903d29222088cf0aeab80c7cc281d9->leave($__internal_513b9b9628fb50e8ec1f6cc0600985e4c1903d29222088cf0aeab80c7cc281d9_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_text.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  24 => 2,  22 => 1,);
    }
}
/* {% if view == 'show' %}*/
/*     {{ value|nl2br }}*/
/* {% else %}*/
/*     {{ value|easyadmin_truncate }}*/
/* {% endif %}*/
/* */
