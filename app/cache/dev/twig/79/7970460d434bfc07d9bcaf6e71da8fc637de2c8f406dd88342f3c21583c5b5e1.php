<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_c4f7a8ec4cc5f400aaa144aa9a4f329663d7f0ff1f7f0e1775020d4c4584cd59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf32407ccd7c734788735b34ec0b9a6c2dc0c491f543f8bbff196a286ec53111 = $this->env->getExtension("native_profiler");
        $__internal_cf32407ccd7c734788735b34ec0b9a6c2dc0c491f543f8bbff196a286ec53111->enter($__internal_cf32407ccd7c734788735b34ec0b9a6c2dc0c491f543f8bbff196a286ec53111_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_cf32407ccd7c734788735b34ec0b9a6c2dc0c491f543f8bbff196a286ec53111->leave($__internal_cf32407ccd7c734788735b34ec0b9a6c2dc0c491f543f8bbff196a286ec53111_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/* <?php foreach ($form as $child): ?>*/
/*     <?php echo $view['form']->widget($child) ?>*/
/*     <?php echo $view['form']->label($child, null, array('translation_domain' => $choice_translation_domain)) ?>*/
/* <?php endforeach ?>*/
/* </div>*/
/* */
