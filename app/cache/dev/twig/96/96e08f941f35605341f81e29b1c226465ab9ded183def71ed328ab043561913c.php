<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_a82bb64d46bf96fba5ed1a1cf25f036cd32fd986d172cbfd5e83191d95b25dc8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9b16e5a8a1f07a20c4ecf89ae362a33f8721996144c21324058a47ef9674052f = $this->env->getExtension("native_profiler");
        $__internal_9b16e5a8a1f07a20c4ecf89ae362a33f8721996144c21324058a47ef9674052f->enter($__internal_9b16e5a8a1f07a20c4ecf89ae362a33f8721996144c21324058a47ef9674052f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9b16e5a8a1f07a20c4ecf89ae362a33f8721996144c21324058a47ef9674052f->leave($__internal_9b16e5a8a1f07a20c4ecf89ae362a33f8721996144c21324058a47ef9674052f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_53c3d5681bc3f43488750ab5fc07503f2beee47e9bef2c46a79befd06a0a0acf = $this->env->getExtension("native_profiler");
        $__internal_53c3d5681bc3f43488750ab5fc07503f2beee47e9bef2c46a79befd06a0a0acf->enter($__internal_53c3d5681bc3f43488750ab5fc07503f2beee47e9bef2c46a79befd06a0a0acf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_53c3d5681bc3f43488750ab5fc07503f2beee47e9bef2c46a79befd06a0a0acf->leave($__internal_53c3d5681bc3f43488750ab5fc07503f2beee47e9bef2c46a79befd06a0a0acf_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "@FOSUser/layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "@FOSUser/Group/list_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
