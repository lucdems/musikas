<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_acaea58832419017fcd8cdf8d4fba3f13508a9e7a407fbb91836fd32c7897fa8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_af4c98d5af140aec8af8d8b4a3d6cc99748e80650c1c91a93fdb21b8cdfd126e = $this->env->getExtension("native_profiler");
        $__internal_af4c98d5af140aec8af8d8b4a3d6cc99748e80650c1c91a93fdb21b8cdfd126e->enter($__internal_af4c98d5af140aec8af8d8b4a3d6cc99748e80650c1c91a93fdb21b8cdfd126e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_af4c98d5af140aec8af8d8b4a3d6cc99748e80650c1c91a93fdb21b8cdfd126e->leave($__internal_af4c98d5af140aec8af8d8b4a3d6cc99748e80650c1c91a93fdb21b8cdfd126e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </div>*/
/* */
