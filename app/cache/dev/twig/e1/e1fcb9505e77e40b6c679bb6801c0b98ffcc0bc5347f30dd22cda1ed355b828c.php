<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_e94b12b191edffcf78d5dd9212d6db03faa6b410b03ee8b777490b0e1afb76ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b7a9e0169466ba463ff7c9ec249c14c93fbf5a74167ce178582a24f34208df7 = $this->env->getExtension("native_profiler");
        $__internal_4b7a9e0169466ba463ff7c9ec249c14c93fbf5a74167ce178582a24f34208df7->enter($__internal_4b7a9e0169466ba463ff7c9ec249c14c93fbf5a74167ce178582a24f34208df7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_4b7a9e0169466ba463ff7c9ec249c14c93fbf5a74167ce178582a24f34208df7->leave($__internal_4b7a9e0169466ba463ff7c9ec249c14c93fbf5a74167ce178582a24f34208df7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child): ?>*/
/*     <?php if (!$child->isRendered()): ?>*/
/*         <?php echo $view['form']->row($child) ?>*/
/*     <?php endif; ?>*/
/* <?php endforeach; ?>*/
/* */
