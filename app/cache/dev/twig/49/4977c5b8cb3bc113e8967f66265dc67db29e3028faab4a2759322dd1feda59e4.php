<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_e6edd83f77b5cba6eeaf49f234f24e98f77fd0db110dcf8629a14dcce77ef2f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d4535c25585918ed6f503def22bfcb50610a539327a44f65cd6cc1d0be4ccd86 = $this->env->getExtension("native_profiler");
        $__internal_d4535c25585918ed6f503def22bfcb50610a539327a44f65cd6cc1d0be4ccd86->enter($__internal_d4535c25585918ed6f503def22bfcb50610a539327a44f65cd6cc1d0be4ccd86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_d4535c25585918ed6f503def22bfcb50610a539327a44f65cd6cc1d0be4ccd86->leave($__internal_d4535c25585918ed6f503def22bfcb50610a539327a44f65cd6cc1d0be4ccd86_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_ef367b7b61da81442c46cc32ccf02c4f521a28e75cfc42f7a5242ba2bd81335d = $this->env->getExtension("native_profiler");
        $__internal_ef367b7b61da81442c46cc32ccf02c4f521a28e75cfc42f7a5242ba2bd81335d->enter($__internal_ef367b7b61da81442c46cc32ccf02c4f521a28e75cfc42f7a5242ba2bd81335d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_ef367b7b61da81442c46cc32ccf02c4f521a28e75cfc42f7a5242ba2bd81335d->leave($__internal_ef367b7b61da81442c46cc32ccf02c4f521a28e75cfc42f7a5242ba2bd81335d_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_34badfb41ab3b66962c0eba86cf12b88c47f3a6f76eeaa0cdca6571b521288ef = $this->env->getExtension("native_profiler");
        $__internal_34badfb41ab3b66962c0eba86cf12b88c47f3a6f76eeaa0cdca6571b521288ef->enter($__internal_34badfb41ab3b66962c0eba86cf12b88c47f3a6f76eeaa0cdca6571b521288ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('translator')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_34badfb41ab3b66962c0eba86cf12b88c47f3a6f76eeaa0cdca6571b521288ef->leave($__internal_34badfb41ab3b66962c0eba86cf12b88c47f3a6f76eeaa0cdca6571b521288ef_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_f539ff963654aca89757447fe5904fb95a30e7611095dc3f47be95ceae81285a = $this->env->getExtension("native_profiler");
        $__internal_f539ff963654aca89757447fe5904fb95a30e7611095dc3f47be95ceae81285a->enter($__internal_f539ff963654aca89757447fe5904fb95a30e7611095dc3f47be95ceae81285a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_f539ff963654aca89757447fe5904fb95a30e7611095dc3f47be95ceae81285a->leave($__internal_f539ff963654aca89757447fe5904fb95a30e7611095dc3f47be95ceae81285a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  58 => 10,  52 => 8,  45 => 4,  39 => 2,  32 => 13,  30 => 8,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {%- autoescape false -%}*/
/* {{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {%- endautoescape -%}*/
/* {% endblock %}*/
/* */
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
