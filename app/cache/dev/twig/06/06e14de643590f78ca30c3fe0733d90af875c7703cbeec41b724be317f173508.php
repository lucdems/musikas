<?php

/* musikasvitrineBundle:Default:accueil.html.twig */
class __TwigTemplate_5c4918e9f6150779bc8250ab92cabeb2b1bb2b641d5fac84f88fe0b19a3b64dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMere.html.twig", "musikasvitrineBundle:Default:accueil.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMere.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_261a55b11ffcaa0140bf2baa738d7fc41bce09d4752921f55086bc63c451ff4a = $this->env->getExtension("native_profiler");
        $__internal_261a55b11ffcaa0140bf2baa738d7fc41bce09d4752921f55086bc63c451ff4a->enter($__internal_261a55b11ffcaa0140bf2baa738d7fc41bce09d4752921f55086bc63c451ff4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasvitrineBundle:Default:accueil.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_261a55b11ffcaa0140bf2baa738d7fc41bce09d4752921f55086bc63c451ff4a->leave($__internal_261a55b11ffcaa0140bf2baa738d7fc41bce09d4752921f55086bc63c451ff4a_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_54db354d17fb8b8c53c6234b236d52bba7f2ad2cd91ab76c8a481435299dd88d = $this->env->getExtension("native_profiler");
        $__internal_54db354d17fb8b8c53c6234b236d52bba7f2ad2cd91ab76c8a481435299dd88d->enter($__internal_54db354d17fb8b8c53c6234b236d52bba7f2ad2cd91ab76c8a481435299dd88d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <section id=\"accueil\">
    
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h1>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Musikas: Ecole de Musique Intercommunale"), "html", null, true);
        echo "</h1>
        </div>
        <div class=\"col-lg-12 col-md-12 col-sm-12\">
            <div class=\"carousel slide\" data-ride=\"carousel\" id=\"carousel-1\">
                <div class=\"carousel-inner\" role=\"listbox\" id=\"slideaccueil\">
                    <div class=\"item active\"><img class=\"img-responsive\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/notes-de-musique-partition.jpg"), "html", null, true);
        echo "\" alt=\"Slide Image\" />
                        <div class=\"carousel-caption\">
                            <h3>Apprendre la musique, un avantage</h3>
                            <p>Le nombre d&#39;études sur les bienfaits de la musique ne cesse d&#39;augmenter... Lire la suite.</p>
                        </div>
                    </div>
                    <div class=\"item\"><img class=\"img-responsive\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/bare_image_3.jpg"), "html", null, true);
        echo "\" alt=\"Slide Image\" />
                        <div class=\"carousel-caption\">
                            <h3>Slide Title</h3>
                            <p>Slide Caption</p>
                        </div>
                    </div>
                    <div class=\"item\"><img class=\"img-responsive\" src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/bare_image_2.jpg"), "html", null, true);
        echo "\" alt=\"Slide Image\" />
                        <div class=\"carousel-caption\">
                            <h3>Slide Title</h3>
                            <p>Slide Caption</p>
                        </div>
                    </div>
                </div>
                <div><a class=\"left carousel-control\" href=\"#carousel-1\" role=\"button\" data-slide=\"prev\"><i class=\"glyphicon glyphicon-chevron-left\"></i><span class=\"sr-only\">Previous</span></a><a class=\"right carousel-control\" href=\"#carousel-1\" role=\"button\"
                    data-slide=\"next\"><i class=\"glyphicon glyphicon-chevron-right\"></i><span class=\"sr-only\">Next</span></a></div>
                <ol class=\"carousel-indicators\">
                    <li data-target=\"#carousel-1\" data-slide-to=\"0\" class=\"active\"></li>
                    <li data-target=\"#carousel-1\" data-slide-to=\"1\"></li>
                    <li data-target=\"#carousel-1\" data-slide-to=\"2\"></li>
                </ol>
            </div>
        </div>
    </div>
</section>
    </section>
";
        
        $__internal_54db354d17fb8b8c53c6234b236d52bba7f2ad2cd91ab76c8a481435299dd88d->leave($__internal_54db354d17fb8b8c53c6234b236d52bba7f2ad2cd91ab76c8a481435299dd88d_prof);

    }

    public function getTemplateName()
    {
        return "musikasvitrineBundle:Default:accueil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 25,  63 => 19,  54 => 13,  46 => 8,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMere.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <section id="accueil">*/
/*     */
/*     <div class="row">*/
/*         <div class="col-md-12">*/
/*             <h1>{{ 'Musikas: Ecole de Musique Intercommunale' | trans }}</h1>*/
/*         </div>*/
/*         <div class="col-lg-12 col-md-12 col-sm-12">*/
/*             <div class="carousel slide" data-ride="carousel" id="carousel-1">*/
/*                 <div class="carousel-inner" role="listbox" id="slideaccueil">*/
/*                     <div class="item active"><img class="img-responsive" src="{{ asset('bundles/assets/img/notes-de-musique-partition.jpg') }}" alt="Slide Image" />*/
/*                         <div class="carousel-caption">*/
/*                             <h3>Apprendre la musique, un avantage</h3>*/
/*                             <p>Le nombre d&#39;études sur les bienfaits de la musique ne cesse d&#39;augmenter... Lire la suite.</p>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="item"><img class="img-responsive" src="{{ asset('bundles/assets/img/bare_image_3.jpg') }}" alt="Slide Image" />*/
/*                         <div class="carousel-caption">*/
/*                             <h3>Slide Title</h3>*/
/*                             <p>Slide Caption</p>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="item"><img class="img-responsive" src="{{ asset('bundles/assets/img/bare_image_2.jpg') }}" alt="Slide Image" />*/
/*                         <div class="carousel-caption">*/
/*                             <h3>Slide Title</h3>*/
/*                             <p>Slide Caption</p>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div><a class="left carousel-control" href="#carousel-1" role="button" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i><span class="sr-only">Previous</span></a><a class="right carousel-control" href="#carousel-1" role="button"*/
/*                     data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i><span class="sr-only">Next</span></a></div>*/
/*                 <ol class="carousel-indicators">*/
/*                     <li data-target="#carousel-1" data-slide-to="0" class="active"></li>*/
/*                     <li data-target="#carousel-1" data-slide-to="1"></li>*/
/*                     <li data-target="#carousel-1" data-slide-to="2"></li>*/
/*                 </ol>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </section>*/
/*     </section>*/
/* {% endblock %}*/
