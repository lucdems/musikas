<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_ef64f413770e45d88b5091bbddc20e986722327f1f9ec84760e40a07c62bbe00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e09b7f241c4897d692d47b3e82ad783403ba792a4a430e4708c572c302a7e3e0 = $this->env->getExtension("native_profiler");
        $__internal_e09b7f241c4897d692d47b3e82ad783403ba792a4a430e4708c572c302a7e3e0->enter($__internal_e09b7f241c4897d692d47b3e82ad783403ba792a4a430e4708c572c302a7e3e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e09b7f241c4897d692d47b3e82ad783403ba792a4a430e4708c572c302a7e3e0->leave($__internal_e09b7f241c4897d692d47b3e82ad783403ba792a4a430e4708c572c302a7e3e0_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_e55dd226a9abe898803cb9a2552fd61031001312d8bd0b9ce52414d0b9453236 = $this->env->getExtension("native_profiler");
        $__internal_e55dd226a9abe898803cb9a2552fd61031001312d8bd0b9ce52414d0b9453236->enter($__internal_e55dd226a9abe898803cb9a2552fd61031001312d8bd0b9ce52414d0b9453236_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.check_email", array("%tokenLifetime%" => (isset($context["tokenLifetime"]) ? $context["tokenLifetime"] : $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_e55dd226a9abe898803cb9a2552fd61031001312d8bd0b9ce52414d0b9453236->leave($__internal_e55dd226a9abe898803cb9a2552fd61031001312d8bd0b9ce52414d0b9453236_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "@FOSUser/layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* <p>*/
/* {{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}*/
/* </p>*/
/* {% endblock %}*/
/* */
