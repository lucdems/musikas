<?php

/* ::default/vueMere.html.twig */
class __TwigTemplate_6ee9226170b6a924f694022aeb234f0adad607ceba83a4ffb29fe7839abbd61c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'header' => array($this, 'block_header'),
            'contenu' => array($this, 'block_contenu'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c8c93e9e4511b78da26e6b40c66a08fc6643bdb681420bd756a4803d8f2d0642 = $this->env->getExtension("native_profiler");
        $__internal_c8c93e9e4511b78da26e6b40c66a08fc6643bdb681420bd756a4803d8f2d0642->enter($__internal_c8c93e9e4511b78da26e6b40c66a08fc6643bdb681420bd756a4803d8f2d0642_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::default/vueMere.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<meta content=\"width=decive-width, initial-scale=1, maximum-scale=1\" name=\"viewport\">
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 91
        echo "        
        ";
        // line 92
        $this->displayBlock('javascripts', $context, $blocks);
        // line 99
        echo "    </body>
    
    
    
";
        // line 103
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 146
        echo "</html>
";
        
        $__internal_c8c93e9e4511b78da26e6b40c66a08fc6643bdb681420bd756a4803d8f2d0642->leave($__internal_c8c93e9e4511b78da26e6b40c66a08fc6643bdb681420bd756a4803d8f2d0642_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_48d0fa29223f9f0938a1c85ae2768b30f13152d88b2526771dfdb9fba1409415 = $this->env->getExtension("native_profiler");
        $__internal_48d0fa29223f9f0938a1c85ae2768b30f13152d88b2526771dfdb9fba1409415->enter($__internal_48d0fa29223f9f0938a1c85ae2768b30f13152d88b2526771dfdb9fba1409415_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Musikas";
        
        $__internal_48d0fa29223f9f0938a1c85ae2768b30f13152d88b2526771dfdb9fba1409415->leave($__internal_48d0fa29223f9f0938a1c85ae2768b30f13152d88b2526771dfdb9fba1409415_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_f50c7744942d6cbc48abc35c4600ea0d08013ebbcd4bfb81b2af4f95002a1e5f = $this->env->getExtension("native_profiler");
        $__internal_f50c7744942d6cbc48abc35c4600ea0d08013ebbcd4bfb81b2af4f95002a1e5f->enter($__internal_f50c7744942d6cbc48abc35c4600ea0d08013ebbcd4bfb81b2af4f95002a1e5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 11
        echo "        
            ";
        // line 12
        $this->displayBlock('header', $context, $blocks);
        // line 44
        echo "            
            <div id=\"contenu_site\" class=\"container\">
            ";
        // line 46
        $this->displayBlock('contenu', $context, $blocks);
        // line 48
        echo "            </div>
            
            ";
        // line 50
        $this->displayBlock('footer', $context, $blocks);
        // line 89
        echo "            
        ";
        
        $__internal_f50c7744942d6cbc48abc35c4600ea0d08013ebbcd4bfb81b2af4f95002a1e5f->leave($__internal_f50c7744942d6cbc48abc35c4600ea0d08013ebbcd4bfb81b2af4f95002a1e5f_prof);

    }

    // line 12
    public function block_header($context, array $blocks = array())
    {
        $__internal_a9d6cd93eaf0303a64f5f228f54586fb85500837592d292636f465f2d7dd2dc4 = $this->env->getExtension("native_profiler");
        $__internal_a9d6cd93eaf0303a64f5f228f54586fb85500837592d292636f465f2d7dd2dc4->enter($__internal_a9d6cd93eaf0303a64f5f228f54586fb85500837592d292636f465f2d7dd2dc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 13
        echo "            <header>
                <section>
                    <nav class=\"navbar navbar-default\">
                        <div class=\"container\">
                            <div class=\"navbar-header\">
                                <a class=\"navbar-brand navbar-link\" href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_accueil");
        echo "\"><img class=\"img-responsive\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/musikaslogo2.png"), "html", null, true);
        echo "\" id=\"icon\"></a>
                                <button class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navcol-1\" id=\"menuxs\"><span class=\"sr-only\">Toggle navigation</span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span></button>
                            </div>
                            <div class=\"collapse navbar-collapse\" id=\"navcol-1\">
                                <ul class=\"nav navbar-nav navbar-right\">
                                    <li role=\"presentation\"><a href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_changerLangue", array("langue" => "fr"));
        echo "\" id=\"boutondrapeaufrancais\"> </a></li>
                                    <li role=\"presentation\" id=\"test\"><a href=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_changerLangue", array("langue" => "eu"));
        echo "\" id=\"boutondrapeaubasque\"> </a></li>
                                    <li class=\"active\" role=\"presentation\"><a href=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_accueil");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accueil"), "html", null, true);
        echo "</a></li>
                                    <li role=\"presentation\"><a href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("musikasespace_prive_homepage");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Espace Privé"), "html", null, true);
        echo "</a></li>
                                    <li role=\"presentation\"><a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
        echo "#\" id=\"btnformulaire\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pré-Inscription"), "html", null, true);
        echo "</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </section>
            </header>
            <div id=\"navigationmenu\" class=\"container\">
                <ul class=\"nav nav-pills categories\">
                    <li ";
        // line 36
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "musikasvitrine_evenements")) {
            echo "class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_evenements");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Actualités"), "html", null, true);
        echo "</a></li>
                    <li ";
        // line 37
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "musikasvitrine_enseignements")) {
            echo "class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_enseignements");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nos enseignements"), "html", null, true);
        echo "</a></li>
                    <li ";
        // line 38
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "musikasvitrine_equipe")) {
            echo "class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_equipe");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Notre équipe"), "html", null, true);
        echo "</a></li>
                    <li ";
        // line 39
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "musikasvitrine_ecoles")) {
            echo "class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_ecoles");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nos écoles"), "html", null, true);
        echo "</a></li>
                    <li ";
        // line 40
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "musikasvitrine_contact")) {
            echo "class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_contact");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contact"), "html", null, true);
        echo "</a></li>
                </ul>
            </div>
            ";
        
        $__internal_a9d6cd93eaf0303a64f5f228f54586fb85500837592d292636f465f2d7dd2dc4->leave($__internal_a9d6cd93eaf0303a64f5f228f54586fb85500837592d292636f465f2d7dd2dc4_prof);

    }

    // line 46
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_cdd0b36f07126f796e0576a0d2bcf47b795cb3e00d33418a29402d207a86476c = $this->env->getExtension("native_profiler");
        $__internal_cdd0b36f07126f796e0576a0d2bcf47b795cb3e00d33418a29402d207a86476c->enter($__internal_cdd0b36f07126f796e0576a0d2bcf47b795cb3e00d33418a29402d207a86476c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 47
        echo "            ";
        
        $__internal_cdd0b36f07126f796e0576a0d2bcf47b795cb3e00d33418a29402d207a86476c->leave($__internal_cdd0b36f07126f796e0576a0d2bcf47b795cb3e00d33418a29402d207a86476c_prof);

    }

    // line 50
    public function block_footer($context, array $blocks = array())
    {
        $__internal_9379d808e3f179fc4da83e956861b6c269442d25f6d38f1887de2c3c670f0708 = $this->env->getExtension("native_profiler");
        $__internal_9379d808e3f179fc4da83e956861b6c269442d25f6d38f1887de2c3c670f0708->enter($__internal_9379d808e3f179fc4da83e956861b6c269442d25f6d38f1887de2c3c670f0708_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 51
        echo "                
                <footer>
                    <div class=\"footer-clean\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-md-3 col-sm-4 item\">
                                <h3>";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contact"), "html", null, true);
        echo "</h3>
                                <ul>
                                    <li><a href=\"#\">Se rendre à l'école</a></li>
                                    <li><a href=\"#\">Nous joindre</a></li>
                                    <li><a href=\"#\">autre contact...</a></li>
                                </ul>
                            </div>
                            <div class=\"col-md-3 col-sm-4 item\">
                                <h3>";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Liens"), "html", null, true);
        echo " </h3>
                                <ul>
                                    <li><a href=\"#\">Financeur </a></li>
                                    <li><a href=\"#\">.... </a></li>
                                    <li><a href=\"#\">autres écoles...</a></li>
                                </ul>
                            </div>
                            <div class=\"col-md-3 col-sm-4 item\">
                                <h3>";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mentions légales"), "html", null, true);
        echo "</h3>
                                <ul>
                                    <li><a href=\"#\">Statuts </a></li>
                                    <li><a href=\"#\">Compte rendu CA</a></li>
                                    <li><a href=\"#\">Documents </a></li>
                                </ul>
                            </div>
                            <div class=\"col-lg-3 col-md-3 item social\"><a href=\"#\"><i class=\"icon ion-social-facebook\"></i></a>
                            <p class=\"copyright\">Musikas © 2017</p>
                            </div>
                        </div>
                    </div>
                </div>
                </footer>
                
            ";
        
        $__internal_9379d808e3f179fc4da83e956861b6c269442d25f6d38f1887de2c3c670f0708->leave($__internal_9379d808e3f179fc4da83e956861b6c269442d25f6d38f1887de2c3c670f0708_prof);

    }

    // line 92
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_30db3974b4751cc5303983d90b0392a4634b310818cb2145f661b15ff4f4b0f9 = $this->env->getExtension("native_profiler");
        $__internal_30db3974b4751cc5303983d90b0392a4634b310818cb2145f661b15ff4f4b0f9->enter($__internal_30db3974b4751cc5303983d90b0392a4634b310818cb2145f661b15ff4f4b0f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 93
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/js/menuresponsif.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/js/Pricing-Tables.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/js/Pricing-Tables1.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_30db3974b4751cc5303983d90b0392a4634b310818cb2145f661b15ff4f4b0f9->leave($__internal_30db3974b4751cc5303983d90b0392a4634b310818cb2145f661b15ff4f4b0f9_prof);

    }

    // line 103
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6b89ae8629eb0a9269a3672543021c89741a4b5d031db9d4d58528dfedaf515a = $this->env->getExtension("native_profiler");
        $__internal_6b89ae8629eb0a9269a3672543021c89741a4b5d031db9d4d58528dfedaf515a->enter($__internal_6b89ae8629eb0a9269a3672543021c89741a4b5d031db9d4d58528dfedaf515a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 104
        echo "<style>
/*    body { background: #F5F5F5; font: 18px/1.5 sans-serif; }
    h1, h2 { line-height: 1.2; margin: 0 0 .5em; }
    h1 { font-size: 36px; }
    h2 { font-size: 21px; margin-bottom: 1em; }
    p { margin: 0 0 1em 0; }
    a { color: #0000F0; }
    a:hover { text-decoration: none; }
    code { background: #F5F5F5; max-width: 100px; padding: 2px 6px; word-wrap: break-word; }
    #wrapper { background: #FFF; margin: 1em auto; max-width: 800px; width: 95%; }
    #container { padding: 2em; }
    #welcome, #status { margin-bottom: 2em; }
    #welcome h1 span { display: block; font-size: 75%; }
    #icon-status, #icon-book { float: left; height: 64px; margin-right: 1em; margin-top: -4px; width: 64px; }
    #icon-book { display: none; }

    @media (min-width: 768px) {
        #wrapper { width: 80%; margin: 2em auto; }
        #icon-book { display: inline-block; }
        #status a, #next a { display: block; }

        @-webkit-keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        @keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        .sf-toolbar { opacity: 0; -webkit-animation: fade-in 1s .2s forwards; animation: fade-in 1s .2s forwards;}
    }*/
</style>
    <link rel=\"stylesheet\" href=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("https://fonts.googleapis.com/css?family=Roboto:300,400,500,700"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("https://fonts.googleapis.com/css?family=Lora"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/fonts/ionicons.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Article-Clean.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Article-List-1.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Article-List.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Footer-Clean.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("https://daneden.github.io/animate.css/animate.min.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Login-Form-Dark.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Pricing-Tables.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/Pricing-Tables1.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/user.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/css/fa-icon.css"), "html", null, true);
        echo "\"/>

";
        
        $__internal_6b89ae8629eb0a9269a3672543021c89741a4b5d031db9d4d58528dfedaf515a->leave($__internal_6b89ae8629eb0a9269a3672543021c89741a4b5d031db9d4d58528dfedaf515a_prof);

    }

    public function getTemplateName()
    {
        return "::default/vueMere.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  416 => 143,  412 => 142,  408 => 141,  404 => 140,  400 => 139,  396 => 138,  392 => 137,  388 => 136,  384 => 135,  380 => 134,  376 => 133,  372 => 132,  368 => 131,  364 => 130,  336 => 104,  330 => 103,  321 => 97,  317 => 96,  313 => 95,  309 => 94,  304 => 93,  298 => 92,  275 => 73,  264 => 65,  253 => 57,  245 => 51,  239 => 50,  232 => 47,  226 => 46,  209 => 40,  199 => 39,  189 => 38,  179 => 37,  169 => 36,  155 => 27,  149 => 26,  143 => 25,  139 => 24,  135 => 23,  125 => 18,  118 => 13,  112 => 12,  104 => 89,  102 => 50,  98 => 48,  96 => 46,  92 => 44,  90 => 12,  87 => 11,  81 => 10,  69 => 6,  61 => 146,  59 => 103,  53 => 99,  51 => 92,  48 => 91,  46 => 10,  40 => 7,  36 => 6,  29 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <meta content="width=decive-width, initial-scale=1, maximum-scale=1" name="viewport">*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Musikas{% endblock %}</title>*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}*/
/*         */
/*             {% block header %}*/
/*             <header>*/
/*                 <section>*/
/*                     <nav class="navbar navbar-default">*/
/*                         <div class="container">*/
/*                             <div class="navbar-header">*/
/*                                 <a class="navbar-brand navbar-link" href="{{ path('musikasvitrine_accueil') }}"><img class="img-responsive" src="{{ asset('bundles/assets/img/musikaslogo2.png') }}" id="icon"></a>*/
/*                                 <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1" id="menuxs"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>*/
/*                             </div>*/
/*                             <div class="collapse navbar-collapse" id="navcol-1">*/
/*                                 <ul class="nav navbar-nav navbar-right">*/
/*                                     <li role="presentation"><a href="{{ path('musikasvitrine_changerLangue', {'langue': 'fr'}) }}" id="boutondrapeaufrancais"> </a></li>*/
/*                                     <li role="presentation" id="test"><a href="{{ path('musikasvitrine_changerLangue', {'langue': 'eu'}) }}" id="boutondrapeaubasque"> </a></li>*/
/*                                     <li class="active" role="presentation"><a href="{{ path('musikasvitrine_accueil') }}">{{ 'Accueil' | trans }}</a></li>*/
/*                                     <li role="presentation"><a href="{{ path('musikasespace_prive_homepage') }}">{{ 'Espace Privé' | trans }}</a></li>*/
/*                                     <li role="presentation"><a href="{{ path('fos_user_registration_register') }}#" id="btnformulaire">{{ 'Pré-Inscription' | trans }}</a></li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                         </div>*/
/*                     </nav>*/
/*                 </section>*/
/*             </header>*/
/*             <div id="navigationmenu" class="container">*/
/*                 <ul class="nav nav-pills categories">*/
/*                     <li {% if app .request.attributes.get('_route') == 'musikasvitrine_evenements' %}class="active"{% endif %}><a href="{{ path('musikasvitrine_evenements') }}">{{ 'Actualités'|trans }}</a></li>*/
/*                     <li {% if app .request.attributes.get('_route') == 'musikasvitrine_enseignements' %}class="active"{% endif %}><a href="{{ path('musikasvitrine_enseignements') }}">{{ 'Nos enseignements' | trans }}</a></li>*/
/*                     <li {% if app .request.attributes.get('_route') == 'musikasvitrine_equipe' %}class="active"{% endif %}><a href="{{ path('musikasvitrine_equipe') }}">{{ 'Notre équipe' | trans }}</a></li>*/
/*                     <li {% if app .request.attributes.get('_route') == 'musikasvitrine_ecoles' %}class="active"{% endif %}><a href="{{ path('musikasvitrine_ecoles') }}">{{ 'Nos écoles' | trans }}</a></li>*/
/*                     <li {% if app .request.attributes.get('_route') == 'musikasvitrine_contact' %}class="active"{% endif %}><a href="{{ path('musikasvitrine_contact') }}">{{ 'Contact' | trans }}</a></li>*/
/*                 </ul>*/
/*             </div>*/
/*             {% endblock %}*/
/*             */
/*             <div id="contenu_site" class="container">*/
/*             {% block contenu %}*/
/*             {% endblock %}*/
/*             </div>*/
/*             */
/*             {% block footer %}*/
/*                 */
/*                 <footer>*/
/*                     <div class="footer-clean">*/
/*                     <div class="container">*/
/*                         <div class="row">*/
/*                             <div class="col-md-3 col-sm-4 item">*/
/*                                 <h3>{{ 'Contact' | trans }}</h3>*/
/*                                 <ul>*/
/*                                     <li><a href="#">Se rendre à l'école</a></li>*/
/*                                     <li><a href="#">Nous joindre</a></li>*/
/*                                     <li><a href="#">autre contact...</a></li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                             <div class="col-md-3 col-sm-4 item">*/
/*                                 <h3>{{ 'Liens' | trans }} </h3>*/
/*                                 <ul>*/
/*                                     <li><a href="#">Financeur </a></li>*/
/*                                     <li><a href="#">.... </a></li>*/
/*                                     <li><a href="#">autres écoles...</a></li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                             <div class="col-md-3 col-sm-4 item">*/
/*                                 <h3>{{ 'Mentions légales' | trans}}</h3>*/
/*                                 <ul>*/
/*                                     <li><a href="#">Statuts </a></li>*/
/*                                     <li><a href="#">Compte rendu CA</a></li>*/
/*                                     <li><a href="#">Documents </a></li>*/
/*                                 </ul>*/
/*                             </div>*/
/*                             <div class="col-lg-3 col-md-3 item social"><a href="#"><i class="icon ion-social-facebook"></i></a>*/
/*                             <p class="copyright">Musikas © 2017</p>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 </footer>*/
/*                 */
/*             {% endblock %}*/
/*             */
/*         {% endblock %}*/
/*         */
/*         {% block javascripts %}*/
/*             <script src="{{ asset('bundles/assets/js/jquery.min.js') }}"></script>*/
/*             <script src="{{ asset('bundles/assets/bootstrap/js/bootstrap.min.js') }}"></script>*/
/*             <script src="{{ asset('bundles/assets/js/menuresponsif.js') }}"></script>*/
/*             <script src="{{ asset('bundles/assets/js/Pricing-Tables.js') }}"></script>*/
/*             <script src="{{ asset('bundles/assets/js/Pricing-Tables1.js') }}"></script>*/
/*         {% endblock %}*/
/*     </body>*/
/*     */
/*     */
/*     */
/* {% block stylesheets %}*/
/* <style>*/
/* /*    body { background: #F5F5F5; font: 18px/1.5 sans-serif; }*/
/*     h1, h2 { line-height: 1.2; margin: 0 0 .5em; }*/
/*     h1 { font-size: 36px; }*/
/*     h2 { font-size: 21px; margin-bottom: 1em; }*/
/*     p { margin: 0 0 1em 0; }*/
/*     a { color: #0000F0; }*/
/*     a:hover { text-decoration: none; }*/
/*     code { background: #F5F5F5; max-width: 100px; padding: 2px 6px; word-wrap: break-word; }*/
/*     #wrapper { background: #FFF; margin: 1em auto; max-width: 800px; width: 95%; }*/
/*     #container { padding: 2em; }*/
/*     #welcome, #status { margin-bottom: 2em; }*/
/*     #welcome h1 span { display: block; font-size: 75%; }*/
/*     #icon-status, #icon-book { float: left; height: 64px; margin-right: 1em; margin-top: -4px; width: 64px; }*/
/*     #icon-book { display: none; }*/
/* */
/*     @media (min-width: 768px) {*/
/*         #wrapper { width: 80%; margin: 2em auto; }*/
/*         #icon-book { display: inline-block; }*/
/*         #status a, #next a { display: block; }*/
/* */
/*         @-webkit-keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }*/
/*         @keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }*/
/*         .sf-toolbar { opacity: 0; -webkit-animation: fade-in 1s .2s forwards; animation: fade-in 1s .2s forwards;}*/
/*     }*//* */
/* </style>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/bootstrap/css/bootstrap.min.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('https://fonts.googleapis.com/css?family=Lora') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/fonts/ionicons.min.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Article-Clean.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Article-List-1.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Article-List.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Footer-Clean.css') }}">*/
/*     <link rel="stylesheet" href="{{ asset('https://daneden.github.io/animate.css/animate.min.css') }}">*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Login-Form-Dark.css') }}">*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Pricing-Tables.css') }}">*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/Pricing-Tables1.css') }}">*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/user.css') }}"/>*/
/*     <link rel="stylesheet" href="{{ asset('bundles/assets/css/fa-icon.css') }}"/>*/
/* */
/* {% endblock %}*/
/* </html>*/
/* */
