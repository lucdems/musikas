<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_6116f3374ac681f30e6b8a07ea0c65a343ef3393019085c08910d9764cae2931 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b431b5e7c4dada165d0cfbac1a3bd1277869f0916af5b4057111f1502fc8d51b = $this->env->getExtension("native_profiler");
        $__internal_b431b5e7c4dada165d0cfbac1a3bd1277869f0916af5b4057111f1502fc8d51b->enter($__internal_b431b5e7c4dada165d0cfbac1a3bd1277869f0916af5b4057111f1502fc8d51b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b431b5e7c4dada165d0cfbac1a3bd1277869f0916af5b4057111f1502fc8d51b->leave($__internal_b431b5e7c4dada165d0cfbac1a3bd1277869f0916af5b4057111f1502fc8d51b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a07e50b81988b953e0fea1c0fc46ffaef5430f595d8455765a1eb2b51cdabf40 = $this->env->getExtension("native_profiler");
        $__internal_a07e50b81988b953e0fea1c0fc46ffaef5430f595d8455765a1eb2b51cdabf40->enter($__internal_a07e50b81988b953e0fea1c0fc46ffaef5430f595d8455765a1eb2b51cdabf40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_a07e50b81988b953e0fea1c0fc46ffaef5430f595d8455765a1eb2b51cdabf40->leave($__internal_a07e50b81988b953e0fea1c0fc46ffaef5430f595d8455765a1eb2b51cdabf40_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "@FOSUser/layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "@FOSUser/Group/edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
