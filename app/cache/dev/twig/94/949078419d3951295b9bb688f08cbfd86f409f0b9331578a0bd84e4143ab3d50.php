<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_3b6b6928a1beae79008f73603d9159e14540fd4b268ee6bec108cc5f18c9f1bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec78464bbc469baa7da292476c9994c9e7581be159f79f9fd7c82b19fe986d5a = $this->env->getExtension("native_profiler");
        $__internal_ec78464bbc469baa7da292476c9994c9e7581be159f79f9fd7c82b19fe986d5a->enter($__internal_ec78464bbc469baa7da292476c9994c9e7581be159f79f9fd7c82b19fe986d5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_ec78464bbc469baa7da292476c9994c9e7581be159f79f9fd7c82b19fe986d5a->leave($__internal_ec78464bbc469baa7da292476c9994c9e7581be159f79f9fd7c82b19fe986d5a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
