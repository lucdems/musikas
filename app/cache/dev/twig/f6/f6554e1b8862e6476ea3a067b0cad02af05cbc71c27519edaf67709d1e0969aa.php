<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_2e85af32cbee4a91a2acaeedf1215755d0acaee0d630598e2932e5696162475f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_42991f7650c80534baf6ca8e9438e0ed11a49480836a58dcb37809af2cad09c7 = $this->env->getExtension("native_profiler");
        $__internal_42991f7650c80534baf6ca8e9438e0ed11a49480836a58dcb37809af2cad09c7->enter($__internal_42991f7650c80534baf6ca8e9438e0ed11a49480836a58dcb37809af2cad09c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_42991f7650c80534baf6ca8e9438e0ed11a49480836a58dcb37809af2cad09c7->leave($__internal_42991f7650c80534baf6ca8e9438e0ed11a49480836a58dcb37809af2cad09c7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td>*/
/*         <?php echo $view['form']->label($form) ?>*/
/*     </td>*/
/*     <td>*/
/*         <?php echo $view['form']->errors($form) ?>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
