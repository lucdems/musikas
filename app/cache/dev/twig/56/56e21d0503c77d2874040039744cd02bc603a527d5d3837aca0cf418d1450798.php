<?php

/* EasyAdminBundle:default:label_inaccessible.html.twig */
class __TwigTemplate_aa22ff59a8ce54e90d2bc5d94cd86b465c774399b688b9a9cd7466bef6ec9860 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dcc7576cb1e6a70e18192d4a888e60d24f0996d3ab782d8f3c436743d7288749 = $this->env->getExtension("native_profiler");
        $__internal_dcc7576cb1e6a70e18192d4a888e60d24f0996d3ab782d8f3c436743d7288749->enter($__internal_dcc7576cb1e6a70e18192d4a888e60d24f0996d3ab782d8f3c436743d7288749_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_inaccessible.html.twig"));

        // line 2
        echo "
<span class=\"label label-danger\" title=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("label.inaccessible.explanation", array(), "EasyAdminBundle"), "html", null, true);
        echo "\">
    ";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("label.inaccessible", array(), "EasyAdminBundle"), "html", null, true);
        echo "
</span>
";
        
        $__internal_dcc7576cb1e6a70e18192d4a888e60d24f0996d3ab782d8f3c436743d7288749->leave($__internal_dcc7576cb1e6a70e18192d4a888e60d24f0996d3ab782d8f3c436743d7288749_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:label_inaccessible.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 3,  22 => 2,);
    }
}
/* {% trans_default_domain 'EasyAdminBundle' %}*/
/* */
/* <span class="label label-danger" title="{{ 'label.inaccessible.explanation'|trans }}">*/
/*     {{ 'label.inaccessible'|trans }}*/
/* </span>*/
/* */
