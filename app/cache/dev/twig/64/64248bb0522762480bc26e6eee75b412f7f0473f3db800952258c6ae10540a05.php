<?php

/* musikasvitrineBundle:Default:enseignement.html.twig */
class __TwigTemplate_2da1fe4f48cd64839e978bcc63f2f6cb4e37d82279b5153b4a2952bdf6e3665b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMere.html.twig", "musikasvitrineBundle:Default:enseignement.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMere.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c230bbb0992e3dfefbe4d7bd27d2c613bf904aefa1a286c94f1785545f82b69c = $this->env->getExtension("native_profiler");
        $__internal_c230bbb0992e3dfefbe4d7bd27d2c613bf904aefa1a286c94f1785545f82b69c->enter($__internal_c230bbb0992e3dfefbe4d7bd27d2c613bf904aefa1a286c94f1785545f82b69c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasvitrineBundle:Default:enseignement.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c230bbb0992e3dfefbe4d7bd27d2c613bf904aefa1a286c94f1785545f82b69c->leave($__internal_c230bbb0992e3dfefbe4d7bd27d2c613bf904aefa1a286c94f1785545f82b69c_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_e75b72ecb3e03e7553061c796caf7079d71e97b81db6c048d8611c6070156124 = $this->env->getExtension("native_profiler");
        $__internal_e75b72ecb3e03e7553061c796caf7079d71e97b81db6c048d8611c6070156124->enter($__internal_e75b72ecb3e03e7553061c796caf7079d71e97b81db6c048d8611c6070156124_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <div class=\"article-list\">
        <div class=\"container\" id=\"contenuenseignement\">
            <div class=\"intro\">
                <h1 class=\"text-center\">";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["enseignement"]) ? $context["enseignement"] : $this->getContext($context, "enseignement")), "nom", array()), "html", null, true);
        echo " </h1>
                <p class=\"text-justify\">";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["enseignement"]) ? $context["enseignement"] : $this->getContext($context, "enseignement")), "description", array()), "html", null, true);
        echo " </p>
                <a href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["enseignement"]) ? $context["enseignement"] : $this->getContext($context, "enseignement")), "image", array())), "html", null, true);
        echo "\"></a>

            </div>

            <div class=\"row\">
                <div class=\"col-md-12\">
                    <a class=\"btn btn-default\" type=\"button\" id=\"btn_retour\" href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_equipe");
        echo "\">Retour aux enseignements</a>
                </div>
            </div>

            <div class=\"container\" id=\"contenuenseignement\">
                <h1 class=\"text-center\"> Nos lieux d'enseignement : </h1>
            </div>
            <div class=\"row articles\" id=\"imagesenseignements\">
                ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tabEcole"]) ? $context["tabEcole"] : $this->getContext($context, "tabEcole")));
        foreach ($context['_seq'] as $context["_key"] => $context["ecole"]) {
            // line 24
            echo "                
                <div class=\"col-md-4 col-sm-6 item\">
                    <a href=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("musikasvitrine_ecole", array("id" => $this->getAttribute($context["ecole"], "id", array()))), "html", null, true);
            echo "\"><img class=\"img-responsive\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["ecole"], "image", array())), "html", null, true);
            echo "\"></a>
                    <h3 class=\"name\">";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["ecole"], "nom", array()), "html", null, true);
            echo " </h3>
                    <p class=\"text-justify description\">";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["ecole"], "description", array()), "html", null, true);
            echo "</p><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("musikasvitrine_ecole", array("id" => $this->getAttribute($context["ecole"], "id", array()))), "html", null, true);
            echo "\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a>
                </div>
                
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ecole'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "            </div>
        </div>
    </div>
";
        
        $__internal_e75b72ecb3e03e7553061c796caf7079d71e97b81db6c048d8611c6070156124->leave($__internal_e75b72ecb3e03e7553061c796caf7079d71e97b81db6c048d8611c6070156124_prof);

    }

    public function getTemplateName()
    {
        return "musikasvitrineBundle:Default:enseignement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 32,  91 => 28,  87 => 27,  81 => 26,  77 => 24,  73 => 23,  62 => 15,  53 => 9,  49 => 8,  45 => 7,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMere.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <div class="article-list">*/
/*         <div class="container" id="contenuenseignement">*/
/*             <div class="intro">*/
/*                 <h1 class="text-center">{{ enseignement.nom }} </h1>*/
/*                 <p class="text-justify">{{ enseignement.description }} </p>*/
/*                 <a href="#"><img class="img-responsive" src="{{ asset(enseignement.image) }}"></a>*/
/* */
/*             </div>*/
/* */
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <a class="btn btn-default" type="button" id="btn_retour" href="{{ path('musikasvitrine_equipe') }}">Retour aux enseignements</a>*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="container" id="contenuenseignement">*/
/*                 <h1 class="text-center"> Nos lieux d'enseignement : </h1>*/
/*             </div>*/
/*             <div class="row articles" id="imagesenseignements">*/
/*                 {% for ecole in tabEcole %}*/
/*                 */
/*                 <div class="col-md-4 col-sm-6 item">*/
/*                     <a href="{{ path('musikasvitrine_ecole', { 'id':ecole.id }) }}"><img class="img-responsive" src="{{ asset(ecole.image) }}"></a>*/
/*                     <h3 class="name">{{ecole.nom}} </h3>*/
/*                     <p class="text-justify description">{{ ecole.description }}</p><a href="{{ path('musikasvitrine_ecole', { 'id':ecole.id }) }}" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a>*/
/*                 </div>*/
/*                 */
/*                 {% endfor %}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
