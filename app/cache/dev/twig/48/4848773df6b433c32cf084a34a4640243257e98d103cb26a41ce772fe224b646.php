<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_515eac7ea1a5fa553633835679cc742d779bbe05ef186ab41b158450bb729b82 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b5118ba375ed032c1358bc750eabae22793fb1944473fbc7c7baac9188e61ddb = $this->env->getExtension("native_profiler");
        $__internal_b5118ba375ed032c1358bc750eabae22793fb1944473fbc7c7baac9188e61ddb->enter($__internal_b5118ba375ed032c1358bc750eabae22793fb1944473fbc7c7baac9188e61ddb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_b5118ba375ed032c1358bc750eabae22793fb1944473fbc7c7baac9188e61ddb->leave($__internal_b5118ba375ed032c1358bc750eabae22793fb1944473fbc7c7baac9188e61ddb_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_2145839fd27a767f53b4d6879a8be3885bf76217116eb13f0e26a65540956610 = $this->env->getExtension("native_profiler");
        $__internal_2145839fd27a767f53b4d6879a8be3885bf76217116eb13f0e26a65540956610->enter($__internal_2145839fd27a767f53b4d6879a8be3885bf76217116eb13f0e26a65540956610_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_2145839fd27a767f53b4d6879a8be3885bf76217116eb13f0e26a65540956610->leave($__internal_2145839fd27a767f53b4d6879a8be3885bf76217116eb13f0e26a65540956610_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_c108f0c089b571d53940a79bf46d1e3e4d88b429e78b308d947867d1b6337c00 = $this->env->getExtension("native_profiler");
        $__internal_c108f0c089b571d53940a79bf46d1e3e4d88b429e78b308d947867d1b6337c00->enter($__internal_c108f0c089b571d53940a79bf46d1e3e4d88b429e78b308d947867d1b6337c00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('translator')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_c108f0c089b571d53940a79bf46d1e3e4d88b429e78b308d947867d1b6337c00->leave($__internal_c108f0c089b571d53940a79bf46d1e3e4d88b429e78b308d947867d1b6337c00_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_2af42b7cf5afd5217399311ec144a77c5749ca77c7f3a2a560020460b7be9a7a = $this->env->getExtension("native_profiler");
        $__internal_2af42b7cf5afd5217399311ec144a77c5749ca77c7f3a2a560020460b7be9a7a->enter($__internal_2af42b7cf5afd5217399311ec144a77c5749ca77c7f3a2a560020460b7be9a7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_2af42b7cf5afd5217399311ec144a77c5749ca77c7f3a2a560020460b7be9a7a->leave($__internal_2af42b7cf5afd5217399311ec144a77c5749ca77c7f3a2a560020460b7be9a7a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  58 => 10,  52 => 8,  45 => 4,  39 => 2,  32 => 13,  30 => 8,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {%- autoescape false -%}*/
/* {{ 'resetting.email.subject'|trans({'%username%': user.username}) }}*/
/* {%- endautoescape -%}*/
/* {% endblock %}*/
/* */
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
