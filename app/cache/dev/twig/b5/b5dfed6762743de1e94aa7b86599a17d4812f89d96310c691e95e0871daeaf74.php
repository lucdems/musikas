<?php

/* musikasespacePriveBundle:Default:espace_prive_documents.html.twig */
class __TwigTemplate_c44b7202acc1f683babd04f9f58cce184009f416d1d4f8d2b62d906a08584fae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMerePrive.html.twig", "musikasespacePriveBundle:Default:espace_prive_documents.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMerePrive.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_93810cec88a7442c181d41edfb213619fcbc06d0dc8074cccfe2e2c1152f0c28 = $this->env->getExtension("native_profiler");
        $__internal_93810cec88a7442c181d41edfb213619fcbc06d0dc8074cccfe2e2c1152f0c28->enter($__internal_93810cec88a7442c181d41edfb213619fcbc06d0dc8074cccfe2e2c1152f0c28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasespacePriveBundle:Default:espace_prive_documents.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_93810cec88a7442c181d41edfb213619fcbc06d0dc8074cccfe2e2c1152f0c28->leave($__internal_93810cec88a7442c181d41edfb213619fcbc06d0dc8074cccfe2e2c1152f0c28_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_0c16559266db75c818b1ec8356c309615e53f9c37cd88fb9921984a6d1c92c55 = $this->env->getExtension("native_profiler");
        $__internal_0c16559266db75c818b1ec8356c309615e53f9c37cd88fb9921984a6d1c92c55->enter($__internal_0c16559266db75c818b1ec8356c309615e53f9c37cd88fb9921984a6d1c92c55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "<section>
    
<div class=\"container\" id=\"contenu_site\">
    <div class=\"row articles\">
        ";
        // line 8
        echo twig_include($this->env, $context, "default/barre.html.twig");
        echo "
                
        <div class=\"col-md-8 item\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <div class=\"row\">
                        <div class=\"col-lg-7 col-sm-6 col-xs-12\">
                            <h3 class=\"panel-title\"><span class=\"fa-stack\"><i class=\"fa fa-circle fa-stack-2x text-muted\"></i><i class=\"fa fa-users fa-stack-1x fa-inverse\"></i></span>Liste des documents</h3></div>
                        <div class=\"col-lg-5 col-sm-6 col-xs-12 text-right\">
                            <div class=\"btn-group btn-group-justified\">
                                <div class=\"dropdown btn-group\">
                                    <button class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\" type=\"button\">Tri<i class=\"fa fa-chevron-down\"></i></button>
                                    <ul class=\"dropdown-menu\">
                                        <li><a href=\"#\">Day</a></li>
                                        <li><a href=\"#\">Week</a></li>
                                        <li><a href=\"#\">Month</a></li>
                                        <li><a href=\"#\">Quarter</a></li>
                                        <li><a href=\"#\">Year</a></li>
                                        <li><a href=\"#\">All Time</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                    <div class=\"panel-body\">
                        <div class=\"row\">
                            <div class=\"col-md-12\">
                                
                                    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
                                            <p></p>
                                            <h1>Documents partagés avec vous</h1>
                                            <p>Vous pouvez ici consulter les documents mis à disposition par vos professeurs. Utiliser les butons pour télécharger, ou consulter un document.</p>
                                            <p></p>
                                            <p></p>
                                    
                                                <div class=\"panel panel-default panel-table\">
                                                    <div class=\"panel-heading\">
                                                        <div class=\"row\">
                                                            <div class=\"col-lg-7 col-sm-6 col-xs-12\">
                                                                <h3 class=\"panel-title\">Documents</h3></div>
                                                            <div class=\"col-lg-5 col-sm-6 col-xs-12 text-right\">
                                                                <button type=\"button\" class=\"btn btn-sm btn-primary btn-create\">Créer un nouveau</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class=\"panel-body\">
                                                        <table class=\"table table-striped table-bordered table-list\">
                                                            <thead>
                                                                <tr>
                                                                    <th><em class=\"fa fa-cog\"></em></th>
                                                                    <th class=\"hidden-xs\">Nom</th>
                                                                    <th>Type</th>
                                                                    <th>Auteur</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"center\"><a class=\"btn btn-default\"><em class=\"fa fa-pencil\"></em></a><a class=\"btn btn-primary\"><em class=\"fa fa-eye\"></em></a></td>
                                                                    <td class=\"hidden-xs\">1</td>
                                                                    <td>John Doe</td>
                                                                    <td>johndoe@example.com</td>
                                                                </tr>
                                                            </tbody>
                                                            
                                                            
                                                            
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"center\"><a class=\"btn btn-default\"><em class=\"fa fa-download\"></em></a><a class=\"btn btn-primary\"><em class=\"fa fa-eye\"></em></a></td>
                                                                    <td class=\"hidden-xs\">Partition Chant</td>
                                                                    <td>Partition</td>
                                                                    <td>John Doe</td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"center\"><a class=\"btn btn-default\"><em class=\"fa fa-download\"></em></a><a class=\"btn btn-primary\"><em class=\"fa fa-eye\"></em></a></td>
                                                                    <td class=\"hidden-xs\">Guitare</td>
                                                                    <td>Tutoriel vidéo</td>
                                                                    <td>Lucas DOe</td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"center\"><a class=\"btn btn-default\"><em class=\"fa fa-download\"></em></a><a class=\"btn btn-primary\"><em class=\"fa fa-eye\"></em></a></td>
                                                                    <td class=\"hidden-xs\">1</td>
                                                                    <td>Justin Doe</td>
                                                                    <td>johndoe@example.com</td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody>
                                                                <tr>
                                                                    <td align=\"center\"><a class=\"btn btn-default\"><em class=\"fa fa-download\"></em></a><a class=\"btn btn-primary\"><em class=\"fa fa-eye\"></em></a></td>
                                                                    <td class=\"hidden-xs\">1</td>
                                                                    <td>Sylvie Doe</td>
                                                                    <td>johndoe@example.com</td>
                                                                </tr>
                                                            </tbody>
                                                            
                                                            
                                                        </table>
                                                    </div>
                                                    <div class=\"panel-footer\">
                                                        <div class=\"row\">
                                                            <div class=\"col col-xs-4\">Page 1 sur 5</div>
                                                            <div class=\"col col-xs-8\">
                                                                <ul class=\"pagination hidden-xs pull-right\">
                                                                    <li><a href=\"#\">1</a></li>
                                                                    <li><a href=\"#\">2</a></li>
                                                                    <li><a href=\"#\">3</a></li>
                                                                    <li><a href=\"#\">4</a></li>
                                                                    <li><a href=\"#\">5</a></li>
                                                                </ul>
                                                                <ul class=\"pagination visible-xs pull-right\">
                                                                    <li><a href=\"#\">«</a></li>
                                                                    <li><a href=\"#\">»</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                        </div>
                    </div>
                    
            </div>
        </div>
    </div>
</div>

</section>
    </section>
";
        
        $__internal_0c16559266db75c818b1ec8356c309615e53f9c37cd88fb9921984a6d1c92c55->leave($__internal_0c16559266db75c818b1ec8356c309615e53f9c37cd88fb9921984a6d1c92c55_prof);

    }

    public function getTemplateName()
    {
        return "musikasespacePriveBundle:Default:espace_prive_documents.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 8,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMerePrive.html.twig" %}*/
/* */
/* {% block contenu %}*/
/* <section>*/
/*     */
/* <div class="container" id="contenu_site">*/
/*     <div class="row articles">*/
/*         {{ include('default/barre.html.twig') }}*/
/*                 */
/*         <div class="col-md-8 item">*/
/*             <div class="panel panel-default">*/
/*                 <div class="panel-heading">*/
/*                     <div class="row">*/
/*                         <div class="col-lg-7 col-sm-6 col-xs-12">*/
/*                             <h3 class="panel-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-2x text-muted"></i><i class="fa fa-users fa-stack-1x fa-inverse"></i></span>Liste des documents</h3></div>*/
/*                         <div class="col-lg-5 col-sm-6 col-xs-12 text-right">*/
/*                             <div class="btn-group btn-group-justified">*/
/*                                 <div class="dropdown btn-group">*/
/*                                     <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" type="button">Tri<i class="fa fa-chevron-down"></i></button>*/
/*                                     <ul class="dropdown-menu">*/
/*                                         <li><a href="#">Day</a></li>*/
/*                                         <li><a href="#">Week</a></li>*/
/*                                         <li><a href="#">Month</a></li>*/
/*                                         <li><a href="#">Quarter</a></li>*/
/*                                         <li><a href="#">Year</a></li>*/
/*                                         <li><a href="#">All Time</a></li>*/
/*                                     </ul>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 */
/*                 */
/*                     <div class="panel-body">*/
/*                         <div class="row">*/
/*                             <div class="col-md-12">*/
/*                                 */
/*                                     <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />*/
/*                                             <p></p>*/
/*                                             <h1>Documents partagés avec vous</h1>*/
/*                                             <p>Vous pouvez ici consulter les documents mis à disposition par vos professeurs. Utiliser les butons pour télécharger, ou consulter un document.</p>*/
/*                                             <p></p>*/
/*                                             <p></p>*/
/*                                     */
/*                                                 <div class="panel panel-default panel-table">*/
/*                                                     <div class="panel-heading">*/
/*                                                         <div class="row">*/
/*                                                             <div class="col-lg-7 col-sm-6 col-xs-12">*/
/*                                                                 <h3 class="panel-title">Documents</h3></div>*/
/*                                                             <div class="col-lg-5 col-sm-6 col-xs-12 text-right">*/
/*                                                                 <button type="button" class="btn btn-sm btn-primary btn-create">Créer un nouveau</button>*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                     <div class="panel-body">*/
/*                                                         <table class="table table-striped table-bordered table-list">*/
/*                                                             <thead>*/
/*                                                                 <tr>*/
/*                                                                     <th><em class="fa fa-cog"></em></th>*/
/*                                                                     <th class="hidden-xs">Nom</th>*/
/*                                                                     <th>Type</th>*/
/*                                                                     <th>Auteur</th>*/
/*                                                                 </tr>*/
/*                                                             </thead>*/
/*                                                             <tbody>*/
/*                                                                 <tr>*/
/*                                                                     <td align="center"><a class="btn btn-default"><em class="fa fa-pencil"></em></a><a class="btn btn-primary"><em class="fa fa-eye"></em></a></td>*/
/*                                                                     <td class="hidden-xs">1</td>*/
/*                                                                     <td>John Doe</td>*/
/*                                                                     <td>johndoe@example.com</td>*/
/*                                                                 </tr>*/
/*                                                             </tbody>*/
/*                                                             */
/*                                                             */
/*                                                             */
/*                                                             <tbody>*/
/*                                                                 <tr>*/
/*                                                                     <td align="center"><a class="btn btn-default"><em class="fa fa-download"></em></a><a class="btn btn-primary"><em class="fa fa-eye"></em></a></td>*/
/*                                                                     <td class="hidden-xs">Partition Chant</td>*/
/*                                                                     <td>Partition</td>*/
/*                                                                     <td>John Doe</td>*/
/*                                                                 </tr>*/
/*                                                             </tbody>*/
/*                                                             <tbody>*/
/*                                                                 <tr>*/
/*                                                                     <td align="center"><a class="btn btn-default"><em class="fa fa-download"></em></a><a class="btn btn-primary"><em class="fa fa-eye"></em></a></td>*/
/*                                                                     <td class="hidden-xs">Guitare</td>*/
/*                                                                     <td>Tutoriel vidéo</td>*/
/*                                                                     <td>Lucas DOe</td>*/
/*                                                                 </tr>*/
/*                                                             </tbody>*/
/*                                                             <tbody>*/
/*                                                                 <tr>*/
/*                                                                     <td align="center"><a class="btn btn-default"><em class="fa fa-download"></em></a><a class="btn btn-primary"><em class="fa fa-eye"></em></a></td>*/
/*                                                                     <td class="hidden-xs">1</td>*/
/*                                                                     <td>Justin Doe</td>*/
/*                                                                     <td>johndoe@example.com</td>*/
/*                                                                 </tr>*/
/*                                                             </tbody>*/
/*                                                             <tbody>*/
/*                                                                 <tr>*/
/*                                                                     <td align="center"><a class="btn btn-default"><em class="fa fa-download"></em></a><a class="btn btn-primary"><em class="fa fa-eye"></em></a></td>*/
/*                                                                     <td class="hidden-xs">1</td>*/
/*                                                                     <td>Sylvie Doe</td>*/
/*                                                                     <td>johndoe@example.com</td>*/
/*                                                                 </tr>*/
/*                                                             </tbody>*/
/*                                                             */
/*                                                             */
/*                                                         </table>*/
/*                                                     </div>*/
/*                                                     <div class="panel-footer">*/
/*                                                         <div class="row">*/
/*                                                             <div class="col col-xs-4">Page 1 sur 5</div>*/
/*                                                             <div class="col col-xs-8">*/
/*                                                                 <ul class="pagination hidden-xs pull-right">*/
/*                                                                     <li><a href="#">1</a></li>*/
/*                                                                     <li><a href="#">2</a></li>*/
/*                                                                     <li><a href="#">3</a></li>*/
/*                                                                     <li><a href="#">4</a></li>*/
/*                                                                     <li><a href="#">5</a></li>*/
/*                                                                 </ul>*/
/*                                                                 <ul class="pagination visible-xs pull-right">*/
/*                                                                     <li><a href="#">«</a></li>*/
/*                                                                     <li><a href="#">»</a></li>*/
/*                                                                 </ul>*/
/*                                                             </div>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     */
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* </section>*/
/*     </section>*/
/* {% endblock %}*/
