<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_4a5979fb6ce2e8aff1aeb5235aeb3a5c8cc48120c4544c490ed1ebb559cfe354 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c3b011b4dee23da455e9d90903ec7cd5e8ca77fa4c1be9a978ef89e08ce0b92d = $this->env->getExtension("native_profiler");
        $__internal_c3b011b4dee23da455e9d90903ec7cd5e8ca77fa4c1be9a978ef89e08ce0b92d->enter($__internal_c3b011b4dee23da455e9d90903ec7cd5e8ca77fa4c1be9a978ef89e08ce0b92d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_c3b011b4dee23da455e9d90903ec7cd5e8ca77fa4c1be9a978ef89e08ce0b92d->leave($__internal_c3b011b4dee23da455e9d90903ec7cd5e8ca77fa4c1be9a978ef89e08ce0b92d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
