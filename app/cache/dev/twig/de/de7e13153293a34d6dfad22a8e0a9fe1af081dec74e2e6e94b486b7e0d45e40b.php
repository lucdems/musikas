<?php

/* musikasvitrineBundle:Default:evenements.html.twig */
class __TwigTemplate_bff782eec5800bf5b292d2027b13f0d8831cff6feff5f9b7edf6491771a00578 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMere.html.twig", "musikasvitrineBundle:Default:evenements.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMere.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1fe0550af0bd04539cda8e32dfbb2c24464d07fa4f31931fe8e3d105ca7c8931 = $this->env->getExtension("native_profiler");
        $__internal_1fe0550af0bd04539cda8e32dfbb2c24464d07fa4f31931fe8e3d105ca7c8931->enter($__internal_1fe0550af0bd04539cda8e32dfbb2c24464d07fa4f31931fe8e3d105ca7c8931_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasvitrineBundle:Default:evenements.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1fe0550af0bd04539cda8e32dfbb2c24464d07fa4f31931fe8e3d105ca7c8931->leave($__internal_1fe0550af0bd04539cda8e32dfbb2c24464d07fa4f31931fe8e3d105ca7c8931_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_88d2e10d87e4525778022afe7ccbd5c0c198ec8932e94e6422315fd47b76afc3 = $this->env->getExtension("native_profiler");
        $__internal_88d2e10d87e4525778022afe7ccbd5c0c198ec8932e94e6422315fd47b76afc3->enter($__internal_88d2e10d87e4525778022afe7ccbd5c0c198ec8932e94e6422315fd47b76afc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <section>
        <div id=\"contenu\" class=\"article-list\">
        <div class=\"container\">
            <div class=\"intro\">
                <h1 class=\"text-center\">";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nos évènements"), "html", null, true);
        echo " </h1>
                <p class=\"text-center\">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p>
            </div>
            <div class=\"row articles\">
                <div class=\"col-md-12 col-sm-6 item\">
                    <a href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/imageaccueil1.jpg"), "html", null, true);
        echo "\"></a>
                    <h3 class=\"name\">Article Title</h3>
                    <p class=\"description\">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href=\"#\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a></div>
                
                ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tabArticle"]) ? $context["tabArticle"] : $this->getContext($context, "tabArticle")));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 18
            echo "                
                <div class=\"col-md-3 col-sm-6 item\">
                    <a href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("musikasvitrine_evenement", array("id" => $this->getAttribute($context["article"], "id", array()))), "html", null, true);
            echo "\"><img class=\"img-responsive\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["article"], "image", array())), "html", null, true);
            echo "\"></a>
                    <h3 class=\"name\">";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["article"], "titre", array()), "html", null, true);
            echo "</h3>
                    <p class=\"description\">";
            // line 22
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["article"], "contenuFr", array()), 0, 350), "html", null, true);
            echo "... </p><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("musikasvitrine_evenement", array("id" => $this->getAttribute($context["article"], "id", array()))), "html", null, true);
            echo "\" class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a>
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "                
                </div>
                </div>
                </div>
    </section>
";
        
        $__internal_88d2e10d87e4525778022afe7ccbd5c0c198ec8932e94e6422315fd47b76afc3->leave($__internal_88d2e10d87e4525778022afe7ccbd5c0c198ec8932e94e6422315fd47b76afc3_prof);

    }

    public function getTemplateName()
    {
        return "musikasvitrineBundle:Default:evenements.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 25,  79 => 22,  75 => 21,  69 => 20,  65 => 18,  61 => 17,  54 => 13,  46 => 8,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMere.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <section>*/
/*         <div id="contenu" class="article-list">*/
/*         <div class="container">*/
/*             <div class="intro">*/
/*                 <h1 class="text-center">{{ 'Nos évènements' | trans }} </h1>*/
/*                 <p class="text-center">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p>*/
/*             </div>*/
/*             <div class="row articles">*/
/*                 <div class="col-md-12 col-sm-6 item">*/
/*                     <a href="#"><img class="img-responsive" src="{{ asset('bundles/assets/img/imageaccueil1.jpg') }}"></a>*/
/*                     <h3 class="name">Article Title</h3>*/
/*                     <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a href="#" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a></div>*/
/*                 */
/*                 {% for article in tabArticle %}*/
/*                 */
/*                 <div class="col-md-3 col-sm-6 item">*/
/*                     <a href="{{ path('musikasvitrine_evenement', { 'id': article.id }) }}"><img class="img-responsive" src="{{ asset(article.image) }}"></a>*/
/*                     <h3 class="name">{{ article.titre }}</h3>*/
/*                     <p class="description">{{ article.contenuFr[:350] }}... </p><a href="{{ path('musikasvitrine_evenement', { 'id': article.id }) }}" class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a>*/
/*                 </div>*/
/*                 {% endfor %}*/
/*                 */
/*                 </div>*/
/*                 </div>*/
/*                 </div>*/
/*     </section>*/
/* {% endblock %}*/
