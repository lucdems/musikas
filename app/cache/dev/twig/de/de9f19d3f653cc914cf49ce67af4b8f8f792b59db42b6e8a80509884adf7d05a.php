<?php

/* musikasvitrineBundle:Default:contact.html.twig */
class __TwigTemplate_e92a70b295f7d66e32dba961b249427dc170658f5f918a54cd5c96210e93dea7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMere.html.twig", "musikasvitrineBundle:Default:contact.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMere.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cece1ee60eb7c0e967a9559bd3b2c3a78aaa4e819fb3d39be417b23abe8930e7 = $this->env->getExtension("native_profiler");
        $__internal_cece1ee60eb7c0e967a9559bd3b2c3a78aaa4e819fb3d39be417b23abe8930e7->enter($__internal_cece1ee60eb7c0e967a9559bd3b2c3a78aaa4e819fb3d39be417b23abe8930e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasvitrineBundle:Default:contact.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cece1ee60eb7c0e967a9559bd3b2c3a78aaa4e819fb3d39be417b23abe8930e7->leave($__internal_cece1ee60eb7c0e967a9559bd3b2c3a78aaa4e819fb3d39be417b23abe8930e7_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_740b0fdd7e20598727c0404fa5159ba2d3bf84e93882bca71b710cd14b37d129 = $this->env->getExtension("native_profiler");
        $__internal_740b0fdd7e20598727c0404fa5159ba2d3bf84e93882bca71b710cd14b37d129->enter($__internal_740b0fdd7e20598727c0404fa5159ba2d3bf84e93882bca71b710cd14b37d129_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <section>
        <div id=\"contenu\" class=\"article-list\">
        <div class=\"container\">
            <div class=\"intro\">
                <h1 class=\"text-center\"> Nous contacter </h1>
                <!--<p class=\"text-center\"></p>-->
            </div>
            <div class=\"row articles\">
                <div class=\"col-md-12 col-sm-6 item\">
                    <a href=\"#\"><img class=\"img-responsive\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "image", array())), "html", null, true);
        echo "\"></a>
                    <!--<h3 class=\"name\">Article Title</h3>-->
                    <p >";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "contenuFr", array()), "html", null, true);
        echo " </p>
                        
                </div>
            </div>
            
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <a class=\"btn btn-default\" type=\"button\" id=\"btn_retour\" href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("musikasvitrine_equipe");
        echo "\">Retour aux actualités</a>
                </div>
            </div>
            
        </div>
        </div>
    </section>
";
        
        $__internal_740b0fdd7e20598727c0404fa5159ba2d3bf84e93882bca71b710cd14b37d129->leave($__internal_740b0fdd7e20598727c0404fa5159ba2d3bf84e93882bca71b710cd14b37d129_prof);

    }

    public function getTemplateName()
    {
        return "musikasvitrineBundle:Default:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 22,  56 => 15,  51 => 13,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMere.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <section>*/
/*         <div id="contenu" class="article-list">*/
/*         <div class="container">*/
/*             <div class="intro">*/
/*                 <h1 class="text-center"> Nous contacter </h1>*/
/*                 <!--<p class="text-center"></p>-->*/
/*             </div>*/
/*             <div class="row articles">*/
/*                 <div class="col-md-12 col-sm-6 item">*/
/*                     <a href="#"><img class="img-responsive" src="{{ asset(article.image) }}"></a>*/
/*                     <!--<h3 class="name">Article Title</h3>-->*/
/*                     <p >{{ article.contenuFr }} </p>*/
/*                         */
/*                 </div>*/
/*             </div>*/
/*             */
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <a class="btn btn-default" type="button" id="btn_retour" href="{{ path('musikasvitrine_equipe') }}">Retour aux actualités</a>*/
/*                 </div>*/
/*             </div>*/
/*             */
/*         </div>*/
/*         </div>*/
/*     </section>*/
/* {% endblock %}*/
