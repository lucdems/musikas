<?php

/* EasyAdminBundle:default:field_object.html.twig */
class __TwigTemplate_c934d95d7314cfb0adb1fc9651f132dfa8c4ebd8d0bee0739a632614436047f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9486603ab4a3d0686aa6b760887e269a95984d4bd37f50a002764e629e6d9fdf = $this->env->getExtension("native_profiler");
        $__internal_9486603ab4a3d0686aa6b760887e269a95984d4bd37f50a002764e629e6d9fdf->enter($__internal_9486603ab4a3d0686aa6b760887e269a95984d4bd37f50a002764e629e6d9fdf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_object.html.twig"));

        // line 1
        echo "<span class=\"label\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("label.object", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
";
        
        $__internal_9486603ab4a3d0686aa6b760887e269a95984d4bd37f50a002764e629e6d9fdf->leave($__internal_9486603ab4a3d0686aa6b760887e269a95984d4bd37f50a002764e629e6d9fdf_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_object.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <span class="label">{{ 'label.object'|trans(domain = 'EasyAdminBundle') }}</span>*/
/* */
