<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_1a47fdf7143c9d297cc6fde0de42d5edd3730095f47ca1fa8d3cfb1ad3d07453 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8c918c032256fc8ba6f7dcb19fa023c1667a1d51083843d6cc7c33c0063ab00 = $this->env->getExtension("native_profiler");
        $__internal_b8c918c032256fc8ba6f7dcb19fa023c1667a1d51083843d6cc7c33c0063ab00->enter($__internal_b8c918c032256fc8ba6f7dcb19fa023c1667a1d51083843d6cc7c33c0063ab00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_b8c918c032256fc8ba6f7dcb19fa023c1667a1d51083843d6cc7c33c0063ab00->leave($__internal_b8c918c032256fc8ba6f7dcb19fa023c1667a1d51083843d6cc7c33c0063ab00_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'submit')) ?>*/
/* */
