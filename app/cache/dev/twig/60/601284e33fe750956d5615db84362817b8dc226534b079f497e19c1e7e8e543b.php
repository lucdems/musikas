<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_a81fdc131f9e87d159bb56ac48d3d53dd412f0ee10a3e1a940c8a0765ccec5f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d94662c06b70bf56e92051b364594325994f200d431cecba7ee52a5cf0700be = $this->env->getExtension("native_profiler");
        $__internal_5d94662c06b70bf56e92051b364594325994f200d431cecba7ee52a5cf0700be->enter($__internal_5d94662c06b70bf56e92051b364594325994f200d431cecba7ee52a5cf0700be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_5d94662c06b70bf56e92051b364594325994f200d431cecba7ee52a5cf0700be->leave($__internal_5d94662c06b70bf56e92051b364594325994f200d431cecba7ee52a5cf0700be_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'email')) ?>*/
/* */
