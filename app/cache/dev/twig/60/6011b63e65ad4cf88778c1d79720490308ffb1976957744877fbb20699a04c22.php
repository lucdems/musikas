<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_371b6f5e2128c8e58829949dd739a690142348f93665117a778fe09c584d8f4c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e91ccd4c198decb3d7b97618ed377039e8488a01bfeff657566f5dd5260c4975 = $this->env->getExtension("native_profiler");
        $__internal_e91ccd4c198decb3d7b97618ed377039e8488a01bfeff657566f5dd5260c4975->enter($__internal_e91ccd4c198decb3d7b97618ed377039e8488a01bfeff657566f5dd5260c4975_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_e91ccd4c198decb3d7b97618ed377039e8488a01bfeff657566f5dd5260c4975->leave($__internal_e91ccd4c198decb3d7b97618ed377039e8488a01bfeff657566f5dd5260c4975_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="<?php echo isset($type) ? $view->escape($type) : 'text' ?>" <?php echo $view['form']->block($form, 'widget_attributes') ?><?php if (!empty($value) || is_numeric($value)): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?> />*/
/* */
