<?php

/* musikasvitrineBundle:Default:equipe.html.twig */
class __TwigTemplate_a5a10908ee58d462df1772632e5fae017654c27a2af1d872c1c4854a592f0b67 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMere.html.twig", "musikasvitrineBundle:Default:equipe.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMere.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6de8b47e1adcd827357fda3dab7637a83105d7c67c70d5089675b9230dd38053 = $this->env->getExtension("native_profiler");
        $__internal_6de8b47e1adcd827357fda3dab7637a83105d7c67c70d5089675b9230dd38053->enter($__internal_6de8b47e1adcd827357fda3dab7637a83105d7c67c70d5089675b9230dd38053_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasvitrineBundle:Default:equipe.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6de8b47e1adcd827357fda3dab7637a83105d7c67c70d5089675b9230dd38053->leave($__internal_6de8b47e1adcd827357fda3dab7637a83105d7c67c70d5089675b9230dd38053_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_415da75af2716a3e88917c006240c1e29eb40abfef5baeb5b48f01d487e1425e = $this->env->getExtension("native_profiler");
        $__internal_415da75af2716a3e88917c006240c1e29eb40abfef5baeb5b48f01d487e1425e->enter($__internal_415da75af2716a3e88917c006240c1e29eb40abfef5baeb5b48f01d487e1425e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <section>
        <div class=\"article-list\">
        <div class=\"container\" id=\"contenuenseignement\">
            <div class=\"intro\">
                <h1 class=\"text-center\">";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Notre équipe"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("pédagogique"), "html", null, true);
        echo "</h1>
                <p class=\"text-justify\">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p>
            </div>
            <div class=\"row articles\" id=\"imagesequipe\">
                ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tabProf"]) ? $context["tabProf"] : $this->getContext($context, "tabProf")));
        foreach ($context['_seq'] as $context["_key"] => $context["prof"]) {
            // line 13
            echo "                <div class=\"col-md-4 col-sm-6 item\">
                    <a href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("musikasvitrine_prof", array("id" => $this->getAttribute($context["prof"], "id", array()))), "html", null, true);
            echo "\"><img class=\"img-responsive\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["prof"], "image", array())), "html", null, true);
            echo "\"></a>
                    <h3 class=\"name\">";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["prof"], "username", array()), "html", null, true);
            echo " ~ ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["prof"], "nom", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["prof"], "prenom", array()), "html", null, true);
            echo "</h3>
                    <p class=\"text-justify description\">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a class=\"action\"><i class=\"glyphicon glyphicon-circle-arrow-right\"></i></a>
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prof'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "                
            </div>
        </div>
        </div>
    </section>
";
        
        $__internal_415da75af2716a3e88917c006240c1e29eb40abfef5baeb5b48f01d487e1425e->leave($__internal_415da75af2716a3e88917c006240c1e29eb40abfef5baeb5b48f01d487e1425e_prof);

    }

    public function getTemplateName()
    {
        return "musikasvitrineBundle:Default:equipe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 19,  68 => 15,  62 => 14,  59 => 13,  55 => 12,  46 => 8,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMere.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <section>*/
/*         <div class="article-list">*/
/*         <div class="container" id="contenuenseignement">*/
/*             <div class="intro">*/
/*                 <h1 class="text-center">{{ 'Notre équipe' | trans }} {{ 'pédagogique' | trans }}</h1>*/
/*                 <p class="text-justify">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p>*/
/*             </div>*/
/*             <div class="row articles" id="imagesequipe">*/
/*                 {% for prof in tabProf %}*/
/*                 <div class="col-md-4 col-sm-6 item">*/
/*                     <a href="{{ path('musikasvitrine_prof', { 'id': prof.id }) }}"><img class="img-responsive" src="{{ asset(prof.image) }}"></a>*/
/*                     <h3 class="name">{{prof.username}} ~ {{prof.nom}} {{prof.prenom}}</h3>*/
/*                     <p class="text-justify description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p><a class="action"><i class="glyphicon glyphicon-circle-arrow-right"></i></a>*/
/*                 </div>*/
/*                 {% endfor %}*/
/*                 */
/*             </div>*/
/*         </div>*/
/*         </div>*/
/*     </section>*/
/* {% endblock %}*/
