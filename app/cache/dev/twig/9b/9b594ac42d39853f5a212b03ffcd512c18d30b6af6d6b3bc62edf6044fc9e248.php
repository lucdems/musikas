<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_f300115fb6f9c5ba84c24af47c44e78264be473aa010c8d872b5e06c3d4e3811 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6d60e3b9ceae4eccbc888c63d62f34df0a3ebbab296c8007881497aa8d01100a = $this->env->getExtension("native_profiler");
        $__internal_6d60e3b9ceae4eccbc888c63d62f34df0a3ebbab296c8007881497aa8d01100a->enter($__internal_6d60e3b9ceae4eccbc888c63d62f34df0a3ebbab296c8007881497aa8d01100a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_6d60e3b9ceae4eccbc888c63d62f34df0a3ebbab296c8007881497aa8d01100a->leave($__internal_6d60e3b9ceae4eccbc888c63d62f34df0a3ebbab296c8007881497aa8d01100a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'hidden')) ?>*/
/* */
