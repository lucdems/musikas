<?php

/* musikasvitrineBundle:Default:connexion.html.twig */
class __TwigTemplate_2275b9c84445477b1d86a60ff2fc8f58c06b5a51eefc6c349473705cd54cbc28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::default/vueMere.html.twig", "musikasvitrineBundle:Default:connexion.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::default/vueMere.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_de18a1de81d9f13c3f3c0007e21633483173a1f09f7687479e4256a93002e4a7 = $this->env->getExtension("native_profiler");
        $__internal_de18a1de81d9f13c3f3c0007e21633483173a1f09f7687479e4256a93002e4a7->enter($__internal_de18a1de81d9f13c3f3c0007e21633483173a1f09f7687479e4256a93002e4a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "musikasvitrineBundle:Default:connexion.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_de18a1de81d9f13c3f3c0007e21633483173a1f09f7687479e4256a93002e4a7->leave($__internal_de18a1de81d9f13c3f3c0007e21633483173a1f09f7687479e4256a93002e4a7_prof);

    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        $__internal_c53d20bf57249ce06b84ac43a0fe4832cc2dbcdaf0c72c7f54dd381c4f4a854b = $this->env->getExtension("native_profiler");
        $__internal_c53d20bf57249ce06b84ac43a0fe4832cc2dbcdaf0c72c7f54dd381c4f4a854b->enter($__internal_c53d20bf57249ce06b84ac43a0fe4832cc2dbcdaf0c72c7f54dd381c4f4a854b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "contenu"));

        // line 4
        echo "    <section>
        <div class=\"register-photo\">
    <div class=\"container\">
        <div class=\"form-container\">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"row\">
                        <div class=\"col-md-6\">
                            <form method=\"post\">
                                <h1 class=\"text-center\">Créer un compte.</h1>
                                <div class=\"form-group\">
                                    <input class=\"form-control\" type=\"email\" name=\"email\" placeholder=\"Email\" />
                                </div>
                                <div class=\"form-group\">
                                    <input class=\"form-control\" type=\"text\" name=\"prenom\" placeholder=\"Prénom\" />
                                </div>
                                <div class=\"form-group\">
                                    <input class=\"form-control\" type=\"text\" name=\"nom\" placeholder=\"Nom\" />
                                </div>
                                <div class=\"form-group\">
                                    <input class=\"form-control\" type=\"password\" name=\"password\" placeholder=\"Mot de passe\" />
                                </div>
                                <div class=\"form-group\">
                                    <input class=\"form-control\" type=\"password\" name=\"password-repeat\" placeholder=\"Répéter le mot de passe\" />
                                </div>
                                <div class=\"form-group\">
                                    <button class=\"btn btn-primary btn-block\" type=\"submit\">S&#39;inscrire </button>
                                </div><a href=\"#\" class=\"already\">Vous avez déjà un compte ? Connectez vous-ici.</a></form>
                        </div>
                        <div class=\"col-md-6\">
                            <div class=\"visible-md-block visible-lg-block image-holder\"><img class=\"img-responsive\" src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/assets/img/jazz_in_his_soul.jpg"), "html", null, true);
        echo "\" id=\"img_connexion\" /></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    </section>
";
        
        $__internal_c53d20bf57249ce06b84ac43a0fe4832cc2dbcdaf0c72c7f54dd381c4f4a854b->leave($__internal_c53d20bf57249ce06b84ac43a0fe4832cc2dbcdaf0c72c7f54dd381c4f4a854b_prof);

    }

    public function getTemplateName()
    {
        return "musikasvitrineBundle:Default:connexion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 34,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "::default/vueMere.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <section>*/
/*         <div class="register-photo">*/
/*     <div class="container">*/
/*         <div class="form-container">*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <div class="row">*/
/*                         <div class="col-md-6">*/
/*                             <form method="post">*/
/*                                 <h1 class="text-center">Créer un compte.</h1>*/
/*                                 <div class="form-group">*/
/*                                     <input class="form-control" type="email" name="email" placeholder="Email" />*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <input class="form-control" type="text" name="prenom" placeholder="Prénom" />*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <input class="form-control" type="text" name="nom" placeholder="Nom" />*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <input class="form-control" type="password" name="password" placeholder="Mot de passe" />*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <input class="form-control" type="password" name="password-repeat" placeholder="Répéter le mot de passe" />*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <button class="btn btn-primary btn-block" type="submit">S&#39;inscrire </button>*/
/*                                 </div><a href="#" class="already">Vous avez déjà un compte ? Connectez vous-ici.</a></form>*/
/*                         </div>*/
/*                         <div class="col-md-6">*/
/*                             <div class="visible-md-block visible-lg-block image-holder"><img class="img-responsive" src="{{ asset('bundles/assets/img/jazz_in_his_soul.jpg') }}" id="img_connexion" /></div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/*     </section>*/
/* {% endblock %}*/
