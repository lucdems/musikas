<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_76cc1be33900aa00afb78024ff50856c2d9efd68f5b9a2e0e81b5a87276dcb0f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64f31c8097221086416dbad2c296326d08772b315f3143b1d3b69ee6d9f5bb1c = $this->env->getExtension("native_profiler");
        $__internal_64f31c8097221086416dbad2c296326d08772b315f3143b1d3b69ee6d9f5bb1c->enter($__internal_64f31c8097221086416dbad2c296326d08772b315f3143b1d3b69ee6d9f5bb1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_64f31c8097221086416dbad2c296326d08772b315f3143b1d3b69ee6d9f5bb1c->leave($__internal_64f31c8097221086416dbad2c296326d08772b315f3143b1d3b69ee6d9f5bb1c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->start($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* <?php echo $view['form']->end($form) ?>*/
/* */
