(function($){

    /* Quand je clique sur l'icône hamburger je rajoute une classe au body */
    $('#menuxs').click(function(e){
        e.preventDefault();
        $('#contenu_site').toggleClass('with--sidebar');
        $('#navigationmenu').toggleClass('with--sidebar');
    });

    /* Je veux pouvoir masquer le menu si on clique sur le cache */
    $('#site-cache').click(function(e){
        $('body').removeClass('with--sidebar');
        $('#contenu_site').removeClass('with--sidebar');
        $('#navigationmenu').removeClass('with--sidebar');
    })

})(jQuery);